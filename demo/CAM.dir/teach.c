// EyeBot Demo Program: Color Detection (red), T. Bräunl, Nov 2015
#include "eyebot.h"
#define SIZE QVGA_SIZE
#define PIX  (QVGA_SIZE/3)
#define XS 160
#define YS 120
#define X   80
#define Y   60
#define D    5
#define MID (3*(Y*XS + X))

// calc. absolute difference to given hue, then threshold
void match(BYTE himg [PIX], BYTE hue, BYTE mat[PIX], BYTE thres)
{ int diff;
  for (int i=0; i<PIX; i++)
  { diff = abs(himg[i] - hue);
    if (diff>=128) diff = 255 - diff;   // wrap around 0;
    if (himg[i]==0 || hue==0) diff=255; // NO hue
    mat[i] = (diff<thres);
  }  
}

// find position of max element in array
int maxpos(int f[], int size)
{ int i, pos=0, m=0;
  for (i=0; i<size; i++)
    if (f[i]>m)
    { m = f[i]; pos = i;
    }
  return pos;
}


//returns Hue from user input
int teach(void)
{ BYTE img[SIZE];
  BYTE r, g, b, hue;
  
  LCDSetPrintf(0,40, "TEACH Color");
  LCDMenu("TEACH", " ", " ", " ");
  do
  { CAMGet(img);
    LCDImage(img);
    LCDLine(X-D, Y,   X+D, Y,   RED); //Crosshair
    LCDLine(X,   Y-D, X,   Y+D, RED); //Crosshair
    // Use a sample points LEFT + MIDDLE + RIGHT for Hue
    r = (img[MID]   + img[MID+3] + img[MID-3]) / 3;
    g = (img[MID+1] + img[MID+4] + img[MID-2]) / 3;
    b = (img[MID+2] + img[MID+5] + img[MID-1]) / 3;
    
    hue = IPPRGB2Hue(r, g, b);  
    LCDSetPrintf(1,40, "RGB %3d %3d %3d", r, g, b);
    LCDSetPrintf(2,40, "Hue %3d", hue);
  } while (KEYRead() != KEY1);

  return hue;
}


int main()
{ BYTE image[SIZE];
  int  row[YS], col[XS];
  BYTE h[PIX], s[PIX], i[PIX], m[PIX], hue;
  int a, b;

  if (CAMInit(QQVGA) != CAM_SUCCESS)
  { LCDPrintf("ERROR cam init"); return 1; }

  hue = teach();
  LCDMenu(" ", " ", " ", "END");

  do
  { CAMGet(image);
    LCDImage(image);
    IPCol2HSI(image, h, s, i);
    match(h, hue, m, 10);

    // Histogram summation over each row and col
    for (int i=0; i<YS; i++) row[i]=0;
    for (int j=0; j<XS; j++) col[j]=0;

    for (int i=0; i<YS; i++)
      for (int j=0; j<XS; j++)
      { row[i] += m[i*XS+j];
        col[j] += m[i*XS+j];
      }

    a = maxpos(row, YS);
    b = maxpos(col, XS);
    
 printf("circle %d %d\n, a, b");
   LCDCircle(a,b, 10, GREEN, 1);
  } while (KEYRead() != KEY4);
    
  CAMRelease();
  return 0;
}
