// EyeBot Demo Program: Camera Display, T. Bräunl, June 2015
#include "eyebot.h"

int main()
{ BYTE img[QVGA_SIZE];

  CAMInit(QVGA); // automatically sets IPSize and LCDImageSize
  
  for (int i=0; i<200; i++)
  { CAMGet(img);
    LCDImage(img);
  }
}
