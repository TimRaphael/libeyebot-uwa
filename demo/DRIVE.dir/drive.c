// EyeBot Demo Program: Drive, T. Bräunl, Nov. 2015
#include "eyebot.h"

int main()
{ for (int i=0; i<2; i++)  // run twice
  { VWStraight(500, 100);  // 0.5m in ca. 5s
    while (!VWDriveDone())
      if (PSDGet(2) < 100) VWSetSpeed(0,0);  // STOP if obstacle in front
    VWTurn(314, 100);      // half turn (Pi) in ca. 3s
    VWDriveWait();         // wait until completed
  }
}
