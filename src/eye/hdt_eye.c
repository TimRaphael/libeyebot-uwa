/**
 * \file hdt.c
 * \brief the code to run hardware sub-windowthe GUI for the EyeBot
 * \author Marcus Pham
 */

int show_hdt(void);

#include <unistd.h> /* access()? */
#include <stdlib.h> /* malloc() */
#include <stdio.h> /* sprintf() */
#include <stdarg.h> /* va_list */
#include <string.h> /* memset()  */

#include <eyebot.h>
#include "header.h"

#define MSG_WAITTIME 2 /* sec */

#define MSG_POS_Y 2
#define MSG_POS_X 2

/**
 * \brief Structure defining a all the HDT devices
 */
typedef struct HDT_ALL
{
	HDT_TABLE* ptabmotor;
	HDT_TABLE* ptabpsd;
    HDT_TABLE* ptabservo;
    
	HDT_MOTOR* pmotor;
	HDT_ENCODER* pencoder;
	HDT_PSD* ppsd;
	HDT_IRTV* pirtv;
	HDT_SERVO* pservo;
    
	int countMOTOR;
	int countENCODER;
	int countPSD;
	int countIRTV;
	int countSERVO;
}
HDT_ALL;

static FBINFO fb;

char* IRTVTypeString(int type)
{
	switch(type)
	{
	case 0: return "RAW_CODE";
	case 1: return "SPACE_CODE";
	case 2: return "PULSE_CODE";
	case 3: return "MANCHESTER_CODE";
	}
	return "";
}

char* IRTVModeString(int mode)
{
	switch(mode)
	{
	case 0: return "DEFAULT_MODE";
	case 1: return "SLOPPY_MODE";
	case 2: return "REPCODE_MODE";
	}
	return "";
}

/* ------------------------------------------------------------------------- */
int load_devs(HDT_DEVICE* pdevices, int type, int count, LIST_MENU *plist)
{
	int loop;
	char *title;
	HDT_DEVICE* pdev;

	/* are you sure? */
	if(count<0)
		return -1;

	/* check device type */
	switch(type)
	{
	case HDT_IDX_PSD:
		title = "PSD List";
		break;
	case HDT_IDX_SERVO:
		title = "Servo List";
		break;
	case HDT_IDX_MOTOR:
		title = "Motor List";
		break;
	case HDT_IDX_ENCODER:
		title = "Encoder List";
		break;
	case HDT_IDX_DRIVE:
		title = "Drive Module List";
		break;
	case HDT_IDX_COMPASS:
		title = "Compass List";
		break;
	case HDT_IDX_IRTV:
		title = "IRTV List";
		break;
	case HDT_IDX_CAM:
		title = "Camera List";
		break;
	/*case HDT_IDX_ADC:
		title = "ADC List";
		break;*/
	default:
		title = 0x0;
		break;
	}

	/* unsupported device type */
	if(!title)
		return -2;

	/* setup filemenu list */
	LMInit(plist,title);
	for(loop=0, pdev=pdevices; loop<count && pdev; loop++, pdev=pdev->pnext)
	{
		LMAddItem(plist,pdev->name,(void *)pdev);
	}
	LCDList(plist);

	return loop;
}

int test_motor(HDT_DEVICE* pdev)
{
    EYEFONT eyeFontDefaultYellow;
    
    strcpy(eyeFontDefaultYellow.fontName,FONTDEFAULT);
    eyeFontDefaultYellow.fontColour = LCD_YELLOW;
    
    EYEFONT eyeFontDefault;
    strcpy(eyeFontDefault.fontName, FONTDEFAULT);
    eyeFontDefault.fontColour = LCD_WHITE;
    
	int row = 5, speed = 0, step = 10, dir = 0, setspeed, changed = 0;
	HDT_MOTOR* pmotor = (HDT_MOTOR*) pdev;

	LCDClear();
	LCDMenu("Dir","Inc Spd","Dec Spd","Done");
	LCDPrintfFont(eyeFontDefaultYellow, "Testing Motor %d: \n\n", pmotor->regaddr);
	LCDPrintfFont(eyeFontDefault, " Number: %d     \n",pmotor->regaddr);
	LCDPrintfFont(eyeFontDefault, " Table: \"%s\"     \n",pmotor->ptable?
			pmotor->ptable->name:"NOT FOUND!");

    LCDSetPrintf(row,0,"Raw Speed = %3d/100       ",speed);
    LCDSetPrintf(row+2,0,"Direction = %1d          ",dir);
	
	keycode = 0x0;
	LCDRefresh();
	int exit = 0;
	while(exit==0)
	{
		LCDRefresh2();
		switch(keycode)
		{
			case KEY1:
			{
				dir ^= 0x01;
				changed = 1;
				keycode = 0x0;
				break;
			}
			case KEY2:
			{
				if(speed<=(100-step))
					speed += step;
				changed = 1;
				keycode = 0x0;
				break;
			}
			case KEY3:
			{
				if(speed>=step)
					speed -= step;
				changed = 1;
				keycode = 0x0;
				break;
			}
			case KEY4:
			{
                MOTORDriveRaw(pmotor->regaddr,0);
				exit = 1;
				break;
			}
		}
		if(changed)
		{
			setspeed = speed;
			if(dir) setspeed = -setspeed;
			MOTORDriveRaw(pmotor->regaddr,setspeed);
			LCDSetPrintf(row,0,"Raw Speed = %3d/100      ",speed);
			LCDSetPrintf(row+2,0,"Direction = %d      ",dir);
			LCDRefresh();
			changed = 0;
		}
	}
	
	return 0;
}

/* ------------------------------------------------------------------------- */
int test_encoder(HDT_DEVICE* pdev)
{
    EYEFONT eyeFontDefaultYellow;
    
    strcpy(eyeFontDefaultYellow.fontName,FONTDEFAULT);
    eyeFontDefaultYellow.fontColour = LCD_YELLOW;
    
    EYEFONT eyeFontDefault;
    strcpy(eyeFontDefault.fontName, FONTDEFAULT);
    eyeFontDefault.fontColour = LCD_WHITE;
    
	int row = 7, value = 0, speed = 0, step = 10, dir = 0, setspeed, changed = 0;
	HDT_ENCODER* penc = (HDT_ENCODER*) pdev;

	LCDClear();
	LCDMenu("Dir","Inc Spd","Dec Spd","Done");

	LCDPrintfFont(eyeFontDefaultYellow,"Testing Encoder %d: \n\n", penc->regaddr);
	LCDPrintfFont(eyeFontDefault, " Number: %x       \n",penc->regaddr);
	LCDPrintfFont(eyeFontDefault, " Clickspm: %d       \n\n",penc->clickspm);
    
    value = ENCODERRead(penc->regaddr);
		LCDSetPrintf(row,0,"Decoder = %4d (%04x)         ",value,value);
		LCDSetPrintf(row+2,0,"Raw Speed = %3d/100         ",speed);
		LCDSetPrintf(row+4,0,"Direction = %1d         ",dir);
    
	LCDRefresh();
	keycode = 0x0;
	int exit = 0;
    
	while(exit==0)
	{
		switch(keycode)
		{
			case KEY1:
			{
				dir ^= 0x01;
				changed = 1;
				keycode = 0x0;
				break;
			}
			case KEY2:
			{
				if(speed<=(100-step))
					speed += step;
				changed = 1;
				keycode = 0x0;
				break;
			}
			case KEY3:
			{
				if(speed>=step)
					speed -= step;
				changed = 1;
				keycode = 0x0;
				break;
			}
			case KEY4:
			{
                MOTORDrive(penc->regaddr,0);
				exit = 1;
				break;
			}
		}
    
        
		if(changed)
		{
			setspeed = speed;
			if(dir) setspeed = -setspeed;
			MOTORDriveRaw(penc->regaddr,setspeed);
			LCDSetPrintf(row+2,0,"Raw Speed = %3d/100          ",speed);
			LCDSetPrintf(row+4,0,"Direction = %1d         ",dir);
			changed = 0;
		}
        usleep(1000);
		value = ENCODERRead(penc->regaddr);
		LCDSetPrintf(row,0,"Decoder = %4d (%04x)          ",value,value);
		LCDRefresh3();
	}
	
	return 0;
}
/* ------------------------------------------------------------------------- */
int test_psd(HDT_DEVICE* pdev)
{
    
    EYEFONT eyeFontDefaultYellow;
    
    strcpy(eyeFontDefaultYellow.fontName,FONTDEFAULT);
    eyeFontDefaultYellow.fontColour = LCD_YELLOW;
    
    EYEFONT eyeFontDefault;
    strcpy(eyeFontDefault.fontName, FONTDEFAULT);
    eyeFontDefault.fontColour = LCD_WHITE;
    
	int row = 5;
	HDT_PSD* ppsd = (HDT_PSD*) pdev;

	LCDClear();
	LCDMenu("","","","Done");

	LCDPrintfFont(eyeFontDefaultYellow, "Testing PSD %d: \n\n", ppsd->regaddr);
	LCDPrintfFont(eyeFontDefault, " Number: %1d   \n",ppsd->regaddr);
	LCDPrintfFont(eyeFontDefault, " Table: \"%s\"     \n",ppsd->ptable?
			ppsd->ptable->name:"NOT FOUND!");
	LCDRefresh();
    
	while(keycode!=KEY4)
	{
        int temp;
        temp = PSDGetRaw(ppsd->regaddr);
		LCDSetPrintf(row,0,"Raw Value = %4d      ", temp);
		LCDSetPrintf(row+1,0,"Table Value = %4d cm     ", PSDGet(ppsd->regaddr));
		LCDRefresh3();
	}

	return 0;
}
/* ------------------------------------------------------------------------- */
int test_servo(HDT_DEVICE* pdev)
{
    EYEFONT eyeFontDefaultYellow;
    strcpy(eyeFontDefaultYellow.fontName,FONTDEFAULT);
    eyeFontDefaultYellow.fontColour = LCD_YELLOW;
    
    EYEFONT eyeFontDefault;
    strcpy(eyeFontDefault.fontName, FONTDEFAULT);
    eyeFontDefault.fontColour = LCD_WHITE;
    
	int row = 7, angle = 128, step = 10, changed = 0, posPerc = 0;
	HDT_SERVO* pservo = (HDT_SERVO*) pdev;
    
    int maxAngle = 255;
	LCDClear();
	LCDMenu("Min","Inc","Dec","Done");
	LCDPrintfFont(eyeFontDefaultYellow, "Testing Servo %d: \n\n", pservo->regaddr);
	LCDPrintfFont(eyeFontDefault, " Number: %1d      \n",pservo->regaddr);
	LCDPrintfFont(eyeFontDefault, " PWM low: %4d us      \n",pservo->low);
	LCDPrintfFont(eyeFontDefault, " PWM high: %4d us     \n",pservo->high);

    SERVOSetRaw(pservo->regaddr,angle);
    posPerc = (angle*100)/maxAngle;
    
    LCDSetPrintf(row,0,"Servo position (Value, percent):\n%3d, %3d%%",angle,posPerc);
	
	LCDRefresh();
	keycode = 0x0;
	int exit = 0;
	while(exit == 0)
	{
		switch(keycode)
		{
			case KEY1:
			{
				if (angle <= maxAngle && angle > 0)
				{
					angle = 0;
					changed = 1;
				}
				else
				{
					angle = maxAngle;
					changed = 1;
				}
				keycode = 0x0;
				break;
			}
			case KEY2:
			{
				angle += step;
				if(angle>maxAngle) angle = maxAngle;
				changed = 1;
				keycode = 0x0;
				break;
			}
			case KEY3:
			{
				angle -= step;
				if(angle<0) angle = 0;
				changed = 1;
				keycode = 0x0;
				break;
			}
			case KEY4:
			{
				exit = 1;
				break;
			}
		}
		if(changed)
		{
			SERVOSetRaw(pservo->regaddr,angle);
			posPerc = (angle*100)/maxAngle;
			LCDSetPrintf(row,0,"Servo position (Value, percent):\n%3d, %3d%%      ",angle,posPerc);
			LCDRefresh();
			changed = 0;
			if (angle <= maxAngle && angle > 0)
			{
				LCDMenu("Min","Inc","Dec","Done");
			}
			else if (angle == 0)
			{
				LCDMenu("Min","Inc","Dec","Done");
			}
		}
		LCDRefresh3();
	}
	return 0;
}
/* ------------------------------------------------------------------------- */
int test_irtv(HDT_DEVICE* pdev)
{
#define ROW1 4
#define ROW2 6
    
    EYEFONT eyeFontDefault;
    strcpy(eyeFontDefault.fontName, FONTDEFAULT);
    eyeFontDefault.fontColour = LCD_WHITE;
    
	int mask = 0, loop = 0;
	HDT_IRTV* pirtv = (HDT_IRTV*) pdev;

	while(loop<pirtv->length)
	{
		mask |= (0x01 << loop);
		loop++;
	}

	LCDClear();
	LCDMenu("","","","Done");
	LCDPrintfFont(eyeFontDefault, "IRTV \"%s\" Found!\n",pirtv->name);
	LCDPrintfFont(eyeFontDefault, "=> Type: %s\n",IRTVTypeString(pirtv->type));
	LCDPrintfFont(eyeFontDefault, "=> Length: %d\n",pirtv->length);
	LCDPrintfFont(eyeFontDefault, "=> Toggle Mask: 0x%04x\n",pirtv->togmask);
	LCDPrintfFont(eyeFontDefault, "=> Invert Mask: 0x%04x\n",pirtv->invmask);
	LCDPrintfFont(eyeFontDefault, "=> Mode: %s\n",IRTVModeString(pirtv->mode));
	LCDPrintfFont(eyeFontDefault, "=> Buffer Size: %d\n",pirtv->buffsize);
	LCDPrintfFont(eyeFontDefault, "=> Delay: %d\n",pirtv->delay);
	LCDPrintfFont(eyeFontDefault, "\nTesting IRTV \"%s\" (Press any key on remote)\n\n",pirtv->name);
	LCDPrintfFont(eyeFontDefault, "Code Mask : 0x%08x (Length = %d bit)", mask, pirtv->length);
	LCDRefresh();
	while(KEYGet()!=KEY4)
	{
	/*	code = IRTVGet();
		if(code)
		{
			LCDSetPrintf(row,col,"Raw Code = 0x%08x", code&mask);
			row = (row==ROW1?ROW2:ROW1);
			LCDSetPrintf(row,col,"                     ");
			LCDRefresh();
		}
        code = 0;
     */
        usleep(5000);
    }
	return 0;
}
/* ------------------------------------------------------------------------- */
int test_dio(void)
{
    EYEFONT eyeFontYellow;
    strcpy(eyeFontYellow.fontName,FONTDEFAULT);
    eyeFontYellow.fontColour = LCD_YELLOW;
    
    //storing the list
    LIST_MENU *lcdlist;
    lcdlist = LCDGetList();
    LCDSetList(0x0);
    
	LCDClear();
	LCDMenu("","","","DONE");
	LCDPrintfFont(eyeFontYellow, "\nStatus of digital inputs:\n");
	
	LCDSetPrintf(4, 0, "PIN : 0  1  2  3  4  5  6  7");


    LCDSetPrintf(8, 0, "PIN : 8  9 10 11 12 13 14 15");
    LCDRefresh();
    
    while(KEYGet()!=KEY4)
    {
        unsigned int all = 100;
        all = DIGITALReadAll();
        
        LCDSetPrintf(5, 0, "MODE: ");
        LCDSetPrintf(9, 0, "MODE: ");
        
        for(int i = 0; i<8; i++) {
            char value[1];
            sprintf(value, "%d",((all & ( 1 << (15-i) )) >> (15-i)));
            LCDSetPrintf(5,6+i+2*i,value);
        }
        for(int i = 8; i<16; i++) {
            char value[1];
            sprintf(value, "%d",((all & ( 1 << (15-i) )) >> (15-i)));
            LCDSetPrintf(9,6+(i-8)+2*(i-8),value);
        }
        LCDRefresh3();
    }

    LCDSetList(lcdlist);
	return 0;
}

/* ------------------------------------------------------------------------- */
int test_adc(void)
{
    /*
	int adcsample,exit;
	char volt[20];
	ADCHandle adc1_handle, adc2_handle, adc3_handle, adc0_handle;
	exit = 0;

	LCDClear();
	LCDMenu("","",""," DONE");
	printf("Reading from analog inputs:\n");
	LCDRefresh();

	adc0_handle = OSInitADC("ADC0");
	adc1_handle = OSInitADC("ADC1");
	adc2_handle = OSInitADC("ADC2");
	adc3_handle = OSInitADC("ADC3");

	while(exit == 0)
	{
		//Read and print the sample value and voltage level at each adc channel
		//ADC0
		adcsample = OSGetADC(adc0_handle);
		ConvADCSampleToVoltage(adc0_handle, volt, adcsample);
		LCDSetPrintf(2, 0, "ADC0: reading=%d    ", adcsample);
		LCDSetPrintf(2, 30, "voltage=%s v ", volt);

		//ADC1
		adcsample = OSGetADC(adc1_handle);
		ConvADCSampleToVoltage(adc1_handle, volt, adcsample);
		LCDSetPrintf(4, 0, "ADC1: reading=%d    ", adcsample);
		LCDSetPrintf(4, 30, "voltage=%s v ", volt);

		//ADC2
		adcsample = OSGetADC(adc2_handle);
		ConvADCSampleToVoltage(adc2_handle, volt, adcsample);
		LCDSetPrintf(6, 0, "ADC2: reading=%d    ", adcsample);
		LCDSetPrintf(6, 30, "voltage=%s v ", volt);

		//ADC3
		adcsample = OSGetADC(adc3_handle);
		ConvADCSampleToVoltage(adc3_handle, volt, adcsample);
		LCDSetPrintf(8, 0, "ADC3: reading=%d    ", adcsample);
		LCDSetPrintf(8, 30, "voltage=%s v ", volt);

		LCDRefresh3();
		
		if (keycode == KEY4)
		{
			exit = 1;
		}
		
		//sleep(1);
	}

	OSADCRelease(adc0_handle);
	OSADCRelease(adc1_handle);
	OSADCRelease(adc2_handle);
	OSADCRelease(adc3_handle);
*/
	return 0;
}


/* ------------------------------------------------------------------------- */
int test_device(HDT_DEVICE* pdev, int type)
{
	LCDClear();
	LCDSetMode(LCD_CLASSICMENU);
	KEYSetTM(KEYTM_CLASSIC);

	switch(type)
	{
	case HDT_IDX_PSD:
		test_psd(pdev);
		break;
	case HDT_IDX_SERVO:
		test_servo(pdev);
		break;
	case HDT_IDX_MOTOR:
		test_motor(pdev);
		break;
	case HDT_IDX_ENCODER:
		test_encoder(pdev);
		break;
	case HDT_IDX_DRIVE:
		break;
	case HDT_IDX_COMPASS:
		break;
	case HDT_IDX_IRTV:
		test_irtv(pdev);
		break;
	case HDT_IDX_CAM:
		break;
	/*case HDT_IDX_ADC:
		test_adc();
		break;*/
	default:
		type = -1;
		break;
	}

	LCDSetMode(LCD_LISTMENU);
	KEYSetTM(KEYTM_LISTMENU);
	return type;
}
/* ------------------------------------------------------------------------- */
int pick_dev(HDT_DEVICE* pdevices, int type, int count)
{
	int notdone = 1, runindex = -1, retval = 0, loop;
	LIST_MENU listmenu, *listtemp;
	HDT_DEVICE* pdev;

	/* load items & create list menu */
	if (count == 1)
	{
		pdev = pdevices;
		test_device(pdev,type);
		return 0;
	}
	
	listtemp = LCDGetList();
	listmenu.pitems = 0x0;
	LCDClear();
	
	if(load_devs(pdevices, type, count, &listmenu)<=0)
	{
		notdone = 0;
		retval = -1;
	}
	keycode = 0x0;
	LCDMenu("Select", "", " ", "Back");
	/* main loop */
	while(notdone)
	{
		LCDRefresh3();
		switch(keycode)
		{
			case IRTV_OK:
			{
				keycode = KEY1;
				break;
			}
			case KEY4:
				notdone = 0;
				retval = 0;
				keycode = 0x0;
				LCDClear();
				LCDRefresh();
				break;
			case KEY3:
				keycode = 0x0;
				break;
			case KEY1:
				if(listmenu.index<0)
					break;
				runindex = listmenu.index+listmenu.start;
				loop = 0;
				pdev = pdevices;
				while(loop<runindex&&pdev)
				{
					pdev = pdev->pnext;
					loop++;
				}
				if(loop==runindex&&pdev)
				{
					test_device(pdev, type);
				}
				notdone = 1;
				retval = 1;
				keycode = 0x0;
				LCDClear();
				LCDRefresh();
				LCDMenu("Select", "", " ", "Back");
				break;
			case KEY_LISTUP:
				LCDListScrollUp();
				keycode = 0x0;
				LCDClear();
				LCDRefresh();
				break;
			case KEY_LISTDN:
				LCDListScrollDown();
				keycode = 0x0;
				LCDClear();
				LCDRefresh();
				break;
			case KEY_LISTTL:
				keycode = 0x0;
				break;
			case KEY_INVALID:
				/* ignore these */
				keycode = 0x0;
				break;
			case IRTV_UP:
			{
				IRScrollUp();
				keycode = 0x0;
				LCDRefresh();
				break;
			}
			case IRTV_DOWN:
			{
				IRScrollDown();
				keycode = 0x0;
				LCDRefresh();
				break;
			}
			default:
			{
				if (keycode == 0)
				{
					break;
				}
				int test = 0;
				while(keycode&&!(keycode&KEY_LIST1))
				{
					keycode >>= 1; test++;
				}
				LCDListIndex(test);
				keycode = 0x0;
			}
			LCDClear();
			LCDRefresh();
			break;
		}
		usleep(LOOPSLEEPTIME);
	}

	if(listmenu.pitems) free(listmenu.pitems);
	LCDSetList(listtemp);

	return retval;
}
/* ------------------------------------------------------------------------- */
int clean_up(HDT_ALL *pdevs)
{
	if(pdevs->ptabpsd)
	{
		HDTClearTable(pdevs->ptabpsd);
		pdevs->ptabpsd = 0x0;
	}
	if(pdevs->ppsd)
	{
		HDTClearPSD(pdevs->ppsd);
		pdevs->ppsd = 0x0;
		pdevs->countPSD = 0;
	}
	if(pdevs->ptabmotor)
	{
		HDTClearTable(pdevs->ptabmotor);
		pdevs->ptabmotor = 0x0;
	}
	if(pdevs->pmotor)
	{
		HDTClearMOTOR(pdevs->pmotor);
		pdevs->pmotor = 0x0;
		pdevs->countMOTOR = 0;
	}
	if(pdevs->pencoder)
	{
		HDTClearENCODER(pdevs->pencoder);
		pdevs->pencoder = 0x0;
		pdevs->countENCODER = 0;
	}
	if(pdevs->pirtv)
	{
		HDTClearIRTV(pdevs->pirtv);
		pdevs->pirtv = 0x0;
		pdevs->countIRTV = 0;
	}
    if(pdevs->ptabservo)
    {
        HDTClearTable(pdevs->ptabservo);
        pdevs->ptabservo = 0x0;
    }
	if(pdevs->pservo)
	{
		HDTClearSERVO(pdevs->pservo);
		pdevs->pservo = 0x0;
		pdevs->countSERVO = 0;
	}
	/*if(pdevs->padc)
	{
		HDTClearADC(pdevs->padc);
		pdevs->padc = 0x0;
		pdevs->countADC = 0;
	}*/

	return 0;
}
/* ------------------------------------------------------------------------- */
int load_hdt(char* hdtfile, HDT_ALL *pdevs, LIST_MENU *plist)
{
	HDT_MOTOR* pmotor;
	HDT_ENCODER* pencoder;
	HDT_PSD* ppsd;
	HDT_IRTV* pirtv;
	HDT_SERVO* pservo;
#define FILE_STRLEN 6
#define M6MAIN_HDTCOUNT 6
#define M6MAIN_MOTINDEX 0
#define M6MAIN_ENCINDEX 1
#define M6MAIN_PSDINDEX 2
#define M6MAIN_ITVINDEX 3
#define M6MAIN_SRVINDEX 4
//#define M6MAIN_ADCINDEX 5
	char device_name[LCD_LIST_STRLENGTH];

	memset((void*) pdevs, 0x0, sizeof(HDT_ALL));
    
	/* try to load all psds */
	pdevs->ppsd = HDTLoadPSD(hdtfile, 0x0);
	if(pdevs->ppsd)
		pdevs->ptabpsd = HDTLoadTable(hdtfile,(HDT_DEVICE*)pdevs->ppsd);
	/* count psd */
	pdevs->countPSD = 0;
	ppsd = pdevs->ppsd;
	while(ppsd)
	{
		pdevs->countPSD++;
		ppsd = ppsd->pnext;
	}
    
	/* try to load all motors */
	pdevs->pmotor = HDTLoadMOTOR(hdtfile, 0x0);
    if(pdevs->pmotor) {
		pdevs->ptabmotor = HDTLoadTable(hdtfile,(HDT_DEVICE*)pdevs->pmotor);
    }
	/* count motor */
	pdevs->countMOTOR = 0;
	pmotor = pdevs->pmotor;
	while(pmotor)
	{
		pdevs->countMOTOR++;
		pmotor = pmotor->pnext;
	}
    
	/* try to load all encoders */
	pdevs->pencoder = HDTLoadENCODER(hdtfile, 0x0);
	pdevs->countENCODER = 0;
	pencoder = pdevs->pencoder;
	while(pencoder)
	{
		pdevs->countENCODER++;
		pencoder = pencoder->pnext;
	}
    
	/* try to load all irtv */
	pdevs->pirtv = HDTLoadIRTV(hdtfile, 0x0);
	/* count irtvs */
	pdevs->countIRTV = 0;
	pirtv = pdevs->pirtv;
	while(pirtv)
	{
		pdevs->countIRTV++;
		pirtv = pirtv->pnext;
	}
    
	/* try to load all servo */
	pdevs->pservo = HDTLoadSERVO(hdtfile, 0x0);
	/* count servos */
    if(pdevs->pservo) {
        pdevs->ptabservo = HDTLoadTable(hdtfile,(HDT_DEVICE*)pdevs->pservo);
    }
    
	pdevs->countSERVO = 0;
    pservo = pdevs->pservo;
	while(pservo)
	{
		pdevs->countSERVO++;
		pservo = pservo->pnext;
	}
	/* setup filemenu list */
	char *name = malloc(LCD_LIST_STRLENGTH);
	sprintf(name,"File: %s",hdtfile);
    LMInit(plist,name);
	
	/* MOTOR ENTRY */
	sprintf(device_name,"%d MOTOR(s)",pdevs->countMOTOR);
	LMAddItem(plist,device_name,pdevs->pmotor);
	/* ENCODER ENTRY */
	sprintf(device_name,"%d ENCODER(s)",pdevs->countENCODER);
	LMAddItem(plist,device_name,pdevs->pencoder);
	/* PSD ENTRY */
	sprintf(device_name,"%d PSD(s)",pdevs->countPSD);
	LMAddItem(plist,device_name,pdevs->ppsd);
	/* IRTV ENTRY */
	sprintf(device_name,"%d IRTV(s)",pdevs->countIRTV);
	LMAddItem(plist,device_name,pdevs->pirtv);
	/* SERVO ENTRY */
	sprintf(device_name,"%d SERVO(s)",pdevs->countSERVO);
	LMAddItem(plist,device_name,pdevs->pservo);

	/* assign the list! */
	LCDList(plist);

	free(name);
	return 0;
}
/* ------------------------------------------------------------------------- */
int show_hdt_int(char* hdtfile)
{
	keycode = 0x0;
	int exit = 0, runindex = -1, /*retval = 0,*/ type = -1, count = 0;
	LIST_MENU listmenu, *listtemp;
	HDT_ALL m6hdt;
	HDT_DEVICE *pdev = 0x0;

	/* prep list menu */
	listtemp = LCDGetList();
	listmenu.pitems = 0x0;

	/* load hdt */
	load_hdt(hdtfile, &m6hdt, &listmenu);
	LCDClear();
	LCDMenu("Select", "Reload", "Digital IO", "Back");
	LCDRefresh();
	/* main loop */
	while(exit == 0)
	{
		LCDRefresh3();
		switch(keycode)
		{
			case KEY4:
			{
				exit = 1;
				keycode = 0x0;
				LCDClear();
				LCDRefresh();
				break;
			}
			case KEY2:
			{
				keycode = 0x0;
				LCDClear();
				LCDRefresh();
				break;
			}
			case KEY3:
			{
				test_dio();
				keycode = 0x0;
				LCDClear();
                LCDMenu("Select", "Reload", "Digital IO", "Back");
				LCDRefresh();
				break;
			}
			case KEY1:
			{
				if(listmenu.index<0)
					break;
				runindex = listmenu.index+listmenu.start;
				switch(runindex)
				{
					case M6MAIN_MOTINDEX:
					{
						pdev = (HDT_DEVICE*) m6hdt.pmotor;
						type = HDT_IDX_MOTOR;
						count = m6hdt.countMOTOR;
						break;
					}
					case M6MAIN_ENCINDEX:
					{
						pdev = (HDT_DEVICE*) m6hdt.pencoder;
						type = HDT_IDX_ENCODER;
						count = m6hdt.countENCODER;
						break;
					}
					case M6MAIN_PSDINDEX:
					{
						pdev = (HDT_DEVICE*) m6hdt.ppsd;
						type = HDT_IDX_PSD;
						count = m6hdt.countPSD;
						break;
					}
					case M6MAIN_ITVINDEX:
					{
						pdev = (HDT_DEVICE*) m6hdt.pirtv;
						type = HDT_IDX_IRTV;
						count = m6hdt.countIRTV;
						break;
					}
					case M6MAIN_SRVINDEX:
					{
						pdev = (HDT_DEVICE*) m6hdt.pservo;
						type = HDT_IDX_SERVO;
						count = m6hdt.countSERVO;
					break;
					}

				}
				if(pdev)
				{
					//while(pick_dev(pdev, type, count)>0);
					pick_dev(pdev, type, count);
				}
				keycode = 0x0;
				LCDClear();
				LCDRefresh();
				LCDMenu("Select", "Reload", "Digital IO", "Back");
				break;
			}
			case KEY_LISTUP:
			{
				LCDListScrollUp();
				keycode = 0x0;
				LCDRefresh();
				break;
			}
			case KEY_LISTDN:
			{
				LCDListScrollDown();
				keycode = 0x0;
				LCDRefresh();
				break;
			}
			case KEY_LISTTL:
			case KEY_INVALID:
			/* ignore these */
			keycode = 0x0;
			break;
			case IRTV_UP:
			{
				IRScrollUp();
				keycode = 0x0;
				LCDRefresh();
				break;
			}
			case IRTV_DOWN:
			{
				IRScrollDown();
				keycode = 0x0;
				LCDRefresh();
				break;
			}
			case IRTV_OK:
			{
				keycode = KEY1;
				break;
			}
			default:
			{
				if (keycode == 0x0)
				{
					break;
				}
				int test = 0;
				while(keycode&&!(keycode&KEY_LIST1))
				{
					keycode >>= 1; test++;
				}
				LCDListIndex(test);
				keycode = 0x0;
				LCDRefresh();
			}
		}
		usleep(LOOPSLEEPTIME);
	}

	/* clean-up & restore list */
	if(listmenu.pitems)
		LMFreeItems(&listmenu);
	LCDSetList(listtemp);

	/* clean-up hdt memory */
	clean_up(&m6hdt);

	//return retval;
	return 0;
}
/* ------------------------------------------------------------------------- */
int show_hdt(void)
{
	int tempmode;
	int tlcdmode;
	LIST_MENU *listtemp;

	/* save current keymode & lcdmode */
	tempmode = KEYGetTM(0x0);
	tlcdmode = LCDGetMode();
	listtemp = LCDGetList();

	LCDSetMode(LCD_LISTMENU);
	KEYSetTM(KEYTM_LISTMENU);

	LCDGetFBInfo(&fb);
	/* execute low-level function */
	show_hdt_int(HDT_FILE);

	LCDSetList(listtemp);
	LCDResetMode(tlcdmode);
	KEYSetTM(tempmode);

	return 0;
}
/* ------------------------------------------------------------------------- */
