/**
 * \file header.h
 * \brief The header containing all definitions for the GUI for the Eyebot 7
 * \author Marcus Pham
 */

#include "camera.h"
#include "servos_and_motors.h"
#include "psd_sensors.h"
#include "timer.h"
#include "multitasking.h"
#include "key.h"
#include "system.h"
#include "improc.h"
#include "irtv.h"
#include "vomega.h"
#include "lcd.h"
#include "serial.h"
#include "inout.h"
#include "audio.h"
#include "hdt.h"
#include "types.h"
#include "listmenu.h"

#define MAX_FILECHAR 400
#define MAX_USERPROG 32
#define MAX_FULLPATH 320
#define MAX_MSGLEN 128

#define MAX_COOLTIME 300000 /* usec */
#define MAX_WAITTIME 250000 /* usec */
#define MAX_WAITKILL 100000 /* usec */
#define MSG_WAITTIME 2 /* sec */
#define TOUCH_IDLETIME_USEC 250000
#define LOOPSLEEPTIME 250000


// shared functions
int LCDRefresh (void);
int LCDRefresh2(void);
int LCDRefresh3(void);
void processMenu();
int show_msg(const char *format, ...);

/**
 * \brief Structure defining the params for the GUI
 */
typedef struct
{
	int irEnable;
	int irDelay;
	int irCode;
    int runProg;
    int runROBIOS;
    int wifiType;
    char* slaveSsid;
    char* slavePassword;
    char* hotspotSsid;
    char* hotspotPassword;
	char* progName;
    int fontSize;
    int rPi;
    int dispRotate;
    int lcdMode;
    int lcdRot;
    int lcdSize;
} ROBIOS_PARAMS;

//system screen functions
int page_system(void);
int system_exit();
int system_network();
int system_info();

//demo screen functions
int demoPage(void);

//software screen functions
int show_exec(void);

//hardware screen functions
int show_hdt(void);

//common functions
void IRScrollUp(void);
void IRScrollDown(void);
char *keyboardInput(void);
void trimline(char *src);

//global variables
extern int keycode;
extern int screenRefresh;
extern ROBIOS_PARAMS robiosParams;

//thread variables
extern pthread_t threadKeyInput;
extern pthread_t threadIRInput;
extern pthread_t threadScreenRefresh;

