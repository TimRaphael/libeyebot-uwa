/**
 * \file demo.c
 * \brief the code to run the demos sub-window on the GUI for the EyeBot
 * \author Marcus Pham
 */

#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> /* gethostname() */
#include <arpa/inet.h> /**/
#include <signal.h>

// for open()
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "header.h"
#include "lcd.h"
#include "eyebot.h"

#define MAX_STACK_SIZE 10

int demoPage(void);
char demoPath[MAX_FILECHAR];

/**
 * \brief Structure defining a simple stack for use storing iterations down folders
 */
typedef struct
{
    char paths[MAX_STACK_SIZE][MAX_FILECHAR];
    int top;
} STACK;

void setupDemoMenu(LIST_MENU *listtemp)
{
    //first setting up path to demos
    FILE *fp;
    fp = fopen("/home/pi/eyebot/bin/hdt.txt","r");
    if (fp == NULL) {
        fprintf(stdout, "No HDT file present");
        exit(EXIT_FAILURE);
    }
    else {
        char* line = malloc(300*sizeof(char));
        char* buffer = malloc(100*sizeof(char));
        ssize_t read;
        size_t len = 0;
        
        while((read = getline(&line, &len, fp)) != -1) {
            if(strstr(line,"DEMOPATH")==line) {
                buffer = strtok(line," \n");
                
                //checking for the Pathname
                buffer = strtok(NULL, " \n");
                strcpy(demoPath, buffer);
            }
            
        }
        free(line);
    }
    if (fp != NULL) {
        fclose(fp);
    }
    
	DIR *dir;
	struct dirent *ent;
	FBINFO fb;
	LCDGetFBInfo(&fb);
    char menuTitle[MAX_FILECHAR];
    
    strcpy(menuTitle, "Demos: ");
    strcat(menuTitle ,demoPath);
    
	//Set up the list
	LMInit(listtemp,menuTitle);
	/*
	 * populate the demo list menu
	 * first open the directory where the demos are located
	 */
	if ((dir = opendir(demoPath)) != NULL)
	{
		//Next while there are still demo programs to read add them to the item list
		while ((ent = readdir(dir)) != NULL)
		{ // Use only programs ending in ".demo"
            if ((strstr(ent->d_name, ".demo"))||(strstr(ent->d_name, ".dir"))) {
                LMAddItem(listtemp,ent->d_name,NULL);
            }
		}
        closedir(dir);
	}
	LCDList(listtemp);
}

void setupDeeperMenu(LIST_MENU *listtemp, char *path) {
    DIR *dir;
    struct dirent *ent;
    
    FBINFO fb;
    LCDGetFBInfo(&fb);
    
    char menuTitle[MAX_FILECHAR];
    
    strcpy(menuTitle, "Demos: ");
    strcat(menuTitle, path);
    
    //Set up the list
    LMInit(listtemp, menuTitle);
    /*
     * populate the demo list menu
     * first open the directory where the demos are located
     */
    if ((dir = opendir(path)) != NULL)
    {
        //Next while there are still demo programs to read add them to the item list
        while ((ent = readdir(dir)) != NULL)
        { // Use only programs ending in ".demo"
            if ((strstr(ent->d_name, ".demo"))||(strstr(ent->d_name, ".dir")))
                LMAddItem(listtemp,ent->d_name,NULL);
        }
        closedir(dir);
    }
    
    LCDList(listtemp);
}

int demoPage(){
    char run[MAX_FILECHAR];
    keycode = 0x0;
    int exit = 0;
    LIST_MENU listmenu, *listtemp, *demoList;
    
    LCDSetMode(LCD_LISTMENU);
    KEYSetTM(KEYTM_LISTMENU);
    
    listtemp = LCDGetList();
    listmenu.pitems = 0x0;
    LCDClear();
    setupDemoMenu(&listmenu);
    
    STACK stack;
    stack.top = 0;
    sprintf(stack.paths[stack.top], "%s", demoPath);
    
    LCDMenu("Run","Up","Down","Back");
    LCDRefresh();
    
    while (exit==0) {
        LCDRefresh3();
        
        if (LMSize(listmenu) == 0){
            show_msg("Demo folder is empty\n\n  Folder Path: %s\n\n  Returning...", stack.paths[stack.top]);
            if(stack.top == 0) {
                exit = 1;
            }
            else {
                LCDClear();
                stack.top--;
                setupDeeperMenu(&listmenu, stack.paths[stack.top]);
                LCDMenu("Run","Up","Down","Back");
            }
        }
        
        else {
        switch(keycode){
            
            case KEY1:
                if(listmenu.index<0)
                    break;
                
                demoList = LCDGetList();
                
                //running program
                if(strstr(listmenu.pitems[listmenu.index+listmenu.start].label, ".demo")) {
                    sprintf(run,"cd %s; ./%s", stack.paths[stack.top], listmenu.pitems[listmenu.index+listmenu.start].label);
                    system(run);
                    LCDClear();
                    LCDList(demoList);
                }
                
                //iterating deeper
                else if (strstr(listmenu.pitems[listmenu.index+listmenu.start].label, ".dir")) {
                    if(stack.top == 9) {
                        fprintf(stderr, "Cannot iterate any deeper\n");
                    }
                    LCDClear();
                    stack.top++;
                    sprintf(stack.paths[stack.top], "%s/%s", stack.paths[stack.top-1], listmenu.pitems[listmenu.index+listmenu.start].label);
                    setupDeeperMenu(&listmenu, stack.paths[stack.top]);
                }
                
                keycode = 0x0;
                
                LCDMenu("Run","Up","Down","Back");
                break;
		
            case KEY4:
                if(stack.top == 0) {
                    exit = 1;
                }
                else {
                    LCDClear();
                    stack.top--;
                    setupDeeperMenu(&listmenu, stack.paths[stack.top]);
                    LCDMenu("Run","Up","Down","Back");
                }
                keycode = 0x0;
                break;
		
            case KEY_LISTUP:
                LCDListScrollUp();
                keycode = 0x0;
                LCDRefresh();
                break;
		
            case KEY_LISTDN:
                LCDListScrollDown();
                keycode = 0x0;
                LCDRefresh();
                break;
		
            case KEY2: case IRTV_UP:
                IRScrollUp();
                keycode = 0x0;
                LCDRefresh();
                break;
            
            case KEY3: case IRTV_DOWN:
                IRScrollDown();
                keycode = 0x0;
                LCDRefresh();
                break;
                
            case IRTV_OK:
                keycode = KEY1;
                break;

            default:
            {
                if (keycode == 0x0) break;
                int index = 0;
                while(keycode&&!(keycode&KEY_LIST1)){
                keycode >>= 1; index++;
                }
                LCDListIndex(index);
                keycode = 0x0;
                LCDRefresh();
                break;
            }
        } // end Switch
        }
	usleep(LOOPSLEEPTIME);
    } // end while
    
    LCDClear();
    LCDSetList(listtemp);
    return 0;
}
