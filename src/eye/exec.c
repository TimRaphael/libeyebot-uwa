/** 20080717 - azman@ee.uwa.edu.au */
/** based on codes by Thomas Sommer */
/** heavily modified by Stuart Howard */
/** finalised by Marcus Pham */
/** Thomas Braunl, JUne 2015 */

#include <dirent.h> /* readdir()? */
#include <unistd.h> /* access()? */
#include <stdlib.h> /* malloc() */
#include <stdio.h> /* sprintf() */
#include <stdarg.h> /* va_list */
#include <wait.h> /* wait() */
#include <string.h> /* strcmp()  */
#include <signal.h> /* sigaction */
#include <netdb.h> /* struct hostent */
#include <unistd.h> /* gethostname() */

#include <sys/stat.h> /* stat() */
#include <sys/types.h> /* pid_t */
#include <fcntl.h>

#include <eyebot.h>
#include "header.h"

#define MSG_POS_Y 2
#define MSG_POS_X 2

#define MAX_STACK_SIZE 10

static char usermsg[MAX_MSGLEN];
char softwarePath[MAX_FILECHAR];

/**
 * \brief Structure defining a simple stack for use storing iterations down folders
 */
typedef struct
{
    char paths[MAX_STACK_SIZE][MAX_FILECHAR];
    int top;
} STACK;

/* ------------------------------------------------------------------------- */
int show_msg(const char *format, ...)
{
	char formatted_text[MAX_MSGLEN+MAX_FULLPATH];
	va_list var_arg_list;
	int tlcdmode = LCDGetMode(); //save lcd mode!

	//reformat message buffer 
	va_start(var_arg_list, format);
	vsprintf(formatted_text, format, var_arg_list);
	va_end(var_arg_list);

	//copy to actual message buffer 
	OSStrcpy_n(usermsg,formatted_text,MAX_MSGLEN);
    
	//do that thingy! 
	LCDClear();
	LCDSetPrintf(MSG_POS_Y,MSG_POS_X,usermsg);
	LCDRefresh();
	sleep(MSG_WAITTIME);

	//restore lcd mode, but not what was on it!
	LCDResetMode(tlcdmode);
	LCDClear();
    
	return 0;
}

void setupSoftwareMenu(LIST_MENU *listtemp)
{
    //first setting up path to demos
    FILE *fp;
    fp = fopen("/home/pi/eyebot/bin/hdt.txt","r");
    if (fp == NULL) {
        fprintf(stdout, "No HDT file present");
        exit(EXIT_FAILURE);
    }
    else {
        char* line = malloc(300*sizeof(char));
        char* buffer = malloc(100*sizeof(char));
        ssize_t read;
        size_t len = 0;
        
        while((read = getline(&line, &len, fp)) != -1) {
            if(strstr(line,"SOFTWAREPATH")==line) {
                buffer = strtok(line," \n");
                
                //checking for the Pathname
                buffer = strtok(NULL, " \n");
                strcpy(softwarePath, buffer);
            }
            
        }
        free(line);
    }
    if (fp != NULL) {
        fclose(fp);
    }

    
	DIR *dir;
	struct dirent *ent;
	FBINFO fb;
	LCDGetFBInfo(&fb);
	//Set up the list
    char menuTitle[MAX_FILECHAR];
    
    strcpy(menuTitle, "Software: ");
    strcat(menuTitle , softwarePath);
    
	LMInit(listtemp,menuTitle);
	/*
	 * populate the demo list menu
	 * first open the directory where the demos are located
	 */
	if ((dir = opendir(softwarePath)) != NULL)
	{
		//Next while there are still demo programs to read add them to the item list
		while ((ent = readdir(dir)) != NULL)
		{
			//Exclude two entries that are invalid
			if (strcmp("..",ent->d_name)==0||strcmp(".",ent->d_name)==0)
			{
				continue;
			}
			LMAddItem(listtemp,ent->d_name,NULL);
		}
        closedir(dir);
	}
	else
	{
		fprintf(stdout,"ERROR: unable to read software directory\n");
	}
	
	LCDList(listtemp);
}

void setupDeeperExecMenu(LIST_MENU *listtemp, char *path) {
    DIR *dir;
    struct dirent *ent;
    
    FBINFO fb;
    LCDGetFBInfo(&fb);
    
    char menuTitle[MAX_FILECHAR];
    
    strcpy(menuTitle, "Demos: ");
    strcat(menuTitle, path);
    
    //Set up the list
    LMInit(listtemp, menuTitle);
    /*
     * populate the demo list menu
     * first open the directory where the demos are located
     */
    if ((dir = opendir(path)) != NULL)
    {
        //Next while there are still demo programs to read add them to the item list
        while ((ent = readdir(dir)) != NULL)
        { // Use only programs ending in ".demo"
            if (strcmp("..",ent->d_name)==0||strcmp(".",ent->d_name)==0)
                LMAddItem(listtemp,ent->d_name,NULL);
        }
        closedir(dir);
    }
    
    LCDList(listtemp);
}


int show_exec(void)
{
    char run[MAX_FILECHAR];
	keycode = 0x0;
	int exit = 0;
	LIST_MENU listmenu, *listtemp, *execList;

    KEYSetTM(KEYTM_LISTMENU);
    LCDSetColorRaw(LCD_WHITE, LCD_BLACK, LCD_BGCOL_TRANSPARENT);
    LCDSetMode(LCD_LISTMENU);

    listtemp = LCDGetList();
    listmenu.pitems = 0x0;
    LCDClear();
    setupSoftwareMenu(&listmenu);
    
    STACK stack;
    stack.top = 0;
    sprintf(stack.paths[stack.top], "%s", softwarePath);
    
    LCDMenu("Run","Up","Down","Back");
    LCDRefresh();
    
	while (exit==0)
	{
        LCDRefresh3();
        if (LMSize(listmenu) == 0)
		{
			show_msg("Software folder is empty\n\n  Folder Path: %s\n\n  Returning...", stack.paths[stack.top]);
            if(stack.top == 0) {
                exit = 1;
            }
            else {
                stack.top--;
                setupDeeperExecMenu(&listmenu, stack.paths[stack.top]);
                LCDMenu("Run","Up","Down","Back");
            }
		}

		switch(keycode)
		{
			case KEY1:
			{
                if(listmenu.index<0)
                    break;
                
                execList = LCDGetList();
                LCDClear();
                
                //iterating deeper
                if (strstr(listmenu.pitems[listmenu.index+listmenu.start].label, ".dir")) {
                    if(stack.top == 9) {
                        fprintf(stderr, "Cannot iterate any deeper\n");
                    }
                    stack.top++;
                    sprintf(stack.paths[stack.top], "%s/%s", stack.paths[stack.top-1], listmenu.pitems[listmenu.index+listmenu.start].label);
                    setupDeeperExecMenu(&listmenu, stack.paths[stack.top]);
                }
                
                //running program
                else {
                    sprintf(run,"%s/%s", stack.paths[stack.top], listmenu.pitems[listmenu.index+listmenu.start].label);
                    system(run);
                  //  LCDClear();
                    VWStraight(0,0);
                    LCDList(execList);
                }

				keycode = 0x0;

                LCDMenu("Run","Up","Down","Back");
                LCDRefresh();
				break;
			}
			case KEY4:
			{
                if(stack.top == 0) {
                    exit = 1;
                }
                else {
                    stack.top--;
                    setupDeeperExecMenu(&listmenu, stack.paths[stack.top]);
                }
                keycode = 0x0;
				break;
			}
			case KEY_LISTUP:
			{
				LCDListScrollUp();
				keycode = 0x0;
                LCDRefresh();
				break;
			}
			case KEY_LISTDN:
			{
				LCDListScrollDown();
				keycode = 0x0;
                LCDRefresh();
				break;
			}
			case KEY2: case IRTV_UP:
			{
				IRScrollUp();
				keycode = 0x0;
                LCDRefresh();
				break;
			}
			case KEY3: case IRTV_DOWN:
			{
				IRScrollDown();
				keycode = 0x0;
                LCDRefresh();
				break;
			}
			case IRTV_OK:
			{
				keycode = KEY1;
				break;
			}
                
			default:
			{
				if (keycode == 0x0)
				{
					break;
				}
				int index = 0;
				while(keycode&&!(keycode&KEY_LIST1))
				{
					keycode >>= 1;
                    index++;
				}
                
				LCDListIndex(index);
				keycode = 0x0;
				LCDRefresh();
			}
		}
		usleep(LOOPSLEEPTIME);
	}
	LCDClear();
	LCDSetList(listtemp);
	return 0;
}
