/**
 * \file main.c
 * \brief the main code to run the GUI for the EyeBot
 * \author Marcus Pham
 * \date 23/08/2016
 */

#define ROBIOS_VERSION "7.16"

#define _GNU_SOURCE //for the reading

#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <netdb.h> /* struct hostent */
#include <arpa/inet.h> /**/
#include <signal.h>

// for open()
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xresource.h>
#include "eyebot.h"
#include "header.h"

#include <pthread.h>
#include <signal.h>
#include <wiringPi.h>
#include <unistd.h> /* gethostname() */


#define M6MAIN_TEXTROW 1
#define M6MAIN_TEXTCOL 0

#define M6MAIN_BATTBAR_COLOR LCD_BLUE
#define M6MAIN_HOSTNAME_LEN 64

#define vbatt_min_8 0
#define vbatt_max_8 9
#define lcd_timeout 15

// global variables
int keycode;
int screenRefresh;
ROBIOS_PARAMS robiosParams;

//thread variables
pthread_t threadKeyInput;
pthread_t threadIRInput;
pthread_t threadScreenRefresh;

int menu = 1;

/*
 * thread to read the touchscreen/mouse
 */
void *keyInputThread(void *data)
{
	fprintf(stderr,"Key input thread start\n");
	while (1)
	{ if (gMouseButton == 1)
		{
			keycode = KEYRead();
		}
		usleep(LOOPSLEEPTIME);
	}
	fprintf(stderr,"Key input thread end\n");
	return NULL;
}

void *irInputThread(void *data)
{
	fprintf(stderr,"IR input thread start\n");
	int IRCode = 0;
	while (1)
	{
		if (IRTVGetStatus()==1)
		{
			IRCode = IRTVRead();
			switch (IRCode)
			{
				case IRTV_RED:
				{
					keycode = KEY1;
					break;
				}
				case IRTV_GREEN:
				{
					keycode = KEY2;
					break;
				}
				case IRTV_YELLOW:
				{
					keycode = KEY3;
					break;
				}
				case IRTV_BLUE:
				{
					keycode = KEY4;
					break;
				}
				case IRTV_LEFT:
				{
					keycode = KEY_LISTUP;
					break;
				}
				case IRTV_RIGHT:
				{
					keycode = KEY_LISTDN;
					break;
				}
				case IRTV_UP:
				{
					keycode = IRTV_UP;
					break;
				}
				case IRTV_DOWN:
				{
					keycode = IRTV_DOWN;
					break;
				}
				case IRTV_OK:
				{
					keycode = IRTV_OK;
					break;
				}
			}
		}
	}
	fprintf(stderr,"IR input thread end\n");
}

void *screenRefreshThread(void *data)
{
	fprintf(stderr,"Screen refresh thread start\n");
	XEvent event;
	while (1)
	{
		usleep(LOOPSLEEPTIME);
		if (XPending(gLCDHandle->d)==0)
		{
			continue;
		}
		XPeekEvent(gLCDHandle->d,&event);
		switch (event.type)
		{
			case FocusIn:
			case FocusOut:
			{
				fprintf(stderr,"Focus changed\n");
				XNextEvent(gLCDHandle->d,&event);
				screenRefresh = 1;
				fprintf(stderr,"Done\n");
				break;
			}
		}
	}
	fprintf(stderr,"Screen refresh thread end\n");
	return 0;
}

void printfile(char filename[], char title[])
{
	EYEFONT eyeFontDefaultYellow;
	EYEFONT eyeFontBoldWhite;
	
	strcpy(eyeFontDefaultYellow.fontName,FONTDEFAULT);
	eyeFontDefaultYellow.fontColour = LCD_YELLOW;
	strcpy(eyeFontBoldWhite.fontName,FONTBOLD);
	eyeFontBoldWhite.fontColour = LCD_WHITE;
	
	LCDPrintfFont(eyeFontBoldWhite," %s: ", title);
	int fd;
	fd = open(filename, O_RDONLY);
	if (fd == -1)
	{
		fprintf(stdout, "Error opening file");
	}
	else
	{
		char c;
		while( read(fd, &c, 1) == 1 )
		{
			if( c == '\n' )
				break;
			LCDPrintfFont(eyeFontDefaultYellow,"%c",c);
		}
		close(fd);
    }
}

int haddr2inaddr(char *haddr, struct in_addr *inaddr, int hlen)
{
	int loop;
	/* obviously only working for ipv4! */
	for(loop=hlen-1;loop>=0;loop--)
	{
		inaddr->s_addr <<= 8;
		inaddr->s_addr |= (uint32_t) haddr[loop];
	}
	return hlen==4? 1 : 0;
}

int system_info(void){
    
    EYEFONT eyeFontDefaultYellow;
    EYEFONT eyeFontBoldWhite;

    strcpy(eyeFontDefaultYellow.fontName, FONTDEFAULT);
    eyeFontDefaultYellow.fontColour = LCD_YELLOW;
    strcpy(eyeFontBoldWhite.fontName, FONTBOLD);
    eyeFontBoldWhite.fontColour = LCD_WHITE;
    
    LCDClear();
    
    
    INFO_CPU cpu;
    INFO_MEM mem;
    INFO_MISC misc;

    OSInfoCPU(&cpu);
    OSInfoMem(&mem);
    OSInfoMisc(&misc);
    
    int memTotal = atoi(mem.total)/1024;
    int memFree = atoi(mem.free)/1024;
    LCDSetPos(0,0);
    
    
    LCDPrintfFont(eyeFontBoldWhite,"System Page\n\n");
    
    LCDPrintfFont(eyeFontBoldWhite,"Processor:");
    LCDPrintfFont(eyeFontDefaultYellow,"%s\n", cpu.name);
    
    LCDPrintfFont(eyeFontBoldWhite,"    Speed:");
    LCDPrintfFont(eyeFontDefaultYellow,"%s MHz\n", cpu.mhz);
    
    LCDPrintfFont(eyeFontBoldWhite,"     Arch:");
    LCDPrintfFont(eyeFontDefaultYellow,"%s\n", cpu.arch);
    
    LCDPrintfFont(eyeFontBoldWhite," BogoMIPS:");
    LCDPrintfFont(eyeFontDefaultYellow,"%s\n", cpu.bogomips);
    
    LCDPrintfFont(eyeFontBoldWhite,"\n");
    
    LCDPrintfFont(eyeFontBoldWhite,"Processes: ");
    LCDPrintfFont(eyeFontDefaultYellow,"%s\n", mem.procnum);
    LCDPrintfFont(eyeFontBoldWhite,"Total RAM: ");
    LCDPrintfFont(eyeFontDefaultYellow,"%dMB\n", memTotal);
    LCDPrintfFont(eyeFontBoldWhite," Free RAM: ");
    LCDPrintfFont(eyeFontDefaultYellow,"%dMB\n", memFree);
    
    LCDPrintfFont(eyeFontBoldWhite,"\n");
    LCDPrintfFont(eyeFontBoldWhite,"   Uptime: ");
    LCDPrintfFont(eyeFontDefaultYellow,"%ss\n", misc.uptime);
    
    
    LCDPrintfFont(eyeFontBoldWhite,"  Battery: ");
    LCDPrintfFont(eyeFontDefaultYellow,"%ss\n", misc.vbatt);

    
    LCDMenu("","Network","Admin","Back");
    LCDRefresh();
    int exit = 0;
    while(exit == 0) {
        switch(keycode){
            case KEY1:
                keycode = 0x0;
                break;
            case KEY2:
                LCDClear();
                system_network();
                keycode = 0x0;
                break;
            case KEY3:
                LCDClear();
                system_exit();
                keycode = 0x0;
                break;
            case KEY4:
                exit = 1;
                keycode = 0x0;
            default:
                keycode = 0x0;
                break;
        }
        LCDRefresh2();
    }
    
    LCDRefresh2();
    
    return 0;
}

int network_display() {
    EYEFONT eyeFontDefaultYellow;
    EYEFONT eyeFontBoldWhite;
    
    strcpy(eyeFontDefaultYellow.fontName,FONTDEFAULT);
    eyeFontDefaultYellow.fontColour = LCD_YELLOW;
    strcpy(eyeFontBoldWhite.fontName,FONTBOLD);
    eyeFontBoldWhite.fontColour = LCD_WHITE;
    
    char hostname[M6MAIN_HOSTNAME_LEN];
    int count_alias, count_addr, alength, atype;
    struct hostent *phost;
    struct in_addr t_addr;
    char ipAddress[400] = "";
    
    count_alias = 0;
    count_addr = 0;
    alength = 0;
    atype = 0;
    gethostname(hostname,M6MAIN_HOSTNAME_LEN);
    phost = gethostbyname(hostname);
    if(phost)
    {
        while(phost->h_aliases[count_alias])
            count_alias++;
        while(phost->h_addr_list[count_addr])
            count_addr++;
        alength = phost->h_length;
        atype = phost->h_addrtype;
        haddr2inaddr(phost->h_addr, &t_addr, alength);
    }
    LCDSetPos(0,0);
    LCDPrintfFont(eyeFontBoldWhite,"Network Page\n\n");
    LCDPrintfFont(eyeFontBoldWhite," Hostname : ");
    LCDPrintfFont(eyeFontDefaultYellow,"%s\n", hostname);
    LCDPrintfFont(eyeFontBoldWhite,"Addr-Type : ");
    LCDPrintfFont(eyeFontDefaultYellow,"%s\n", atype==AF_INET ?
                  "AF_INET" : atype==AF_INET6 ?
                  "AF_INET6" : "Unsupported");
    LCDPrintfFont(eyeFontBoldWhite,"  IP-Addr : ");
    ipAddress[0] = '\0';
    sprintf(ipAddress,"%s",OSExecute("ifconfig eth0"));
    if (ipAddress[68] == 'i')
    {
        LCDPrintfFont(eyeFontDefaultYellow,"%s\n", OSExecute("ifconfig eth0 | sed -rn 's/.*r:([^ ]+) .*/\\1/p' | awk '{printf \"%s \",$0} END {print \"\"}'"));
    }
    else
    {
        LCDPrintfFont(eyeFontDefaultYellow,"10.0.0.1\n");
    }
    
    printfile("/sys/class/net/eth0/address", "MAC-Addr ");
    
    char wifiServer[50] = "";
    sprintf(wifiServer,"%s",OSExecute("sudo service isc-dhcp-server status"));
    char pass[100];
    sprintf(pass, "%s" , OSExecute("cat /etc/hostapd/hostapd.conf | grep pass | cut -d '=' -f2"));
    char currIP[100];
    sprintf(currIP, "%s", OSExecute("ifconfig wlan0 | grep addr:|cut -d 'B' -f1|cut -d ':' -f2"));
    
    
    if ((strstr(wifiServer,"not")==NULL)&&(robiosParams.wifiType!=1)&&(strlen(currIP)!=0)) {
        char* buffer = malloc(20*sizeof(char));
        char line[100] = "";
        
        //displaying the SSID
        LCDPrintfFont(eyeFontBoldWhite, "\n Wifi SSID: ");
        
        sprintf(line,"%s", OSExecute("cat /etc/hostapd/hostapd.conf |grep ssid | cut -d '=' -f2"));
        buffer = strtok(line," \n");
        LCDPrintfFont(eyeFontDefaultYellow, "%s             \n", buffer);
        
        sprintf(line,"%s", OSExecute( "ifconfig wlan0 | grep addr:|cut -d 'B' -f1|cut -d ':' -f2"));
        buffer = strtok(line," ");
        
        LCDPrintfFont(eyeFontBoldWhite, " Wifi IP  : ");
        LCDPrintfFont(eyeFontDefaultYellow, "%s             \n", buffer);
        
        LCDPrintfFont(eyeFontBoldWhite, " Wifi Pass: ");
        LCDPrintfFont(eyeFontDefaultYellow, "%s             \n", pass);
        
    }
    
    else if ((strstr(wifiServer,"not")!=NULL)&&(robiosParams.wifiType==1)&&(strlen(currIP)!=0)){
        LCDPrintfFont(eyeFontBoldWhite, "\n Wifi SSID: ");
        LCDPrintfFont(eyeFontDefaultYellow, "%s (SLAVE MODE)\n", robiosParams.slaveSsid);
        
        LCDPrintfFont(eyeFontBoldWhite, " Wifi IP  : ");
        LCDPrintfFont(eyeFontDefaultYellow, "%s          \n", currIP);
        
        LCDPrintfFont(eyeFontBoldWhite, " Wifi Pass: ");
        LCDPrintfFont(eyeFontDefaultYellow, "%s            \n", robiosParams.slavePassword);
    }
    
    else {
        LCDPrintfFont(eyeFontBoldWhite, "\n Wifi SSID: ");
        LCDPrintfFont(eyeFontDefaultYellow, "Not Connected          \n");
        
        LCDPrintfFont(eyeFontBoldWhite, " Wifi IP  : ");
        LCDPrintfFont(eyeFontDefaultYellow, "Not Connected          \n");
        
        LCDPrintfFont(eyeFontBoldWhite, " Wifi Pass: ");
        LCDPrintfFont(eyeFontDefaultYellow, "Not Connected            \n");
    }
    
    return 0;
}

int system_network(void){
    
    network_display();

    int row, col;
    keycode = 0x0;
    
    KEYSetTM(KEYTM_CLASSIC);
    LCDSetColorRaw(LCD_WHITE, LCD_BLACK, LCD_BGCOL_TRANSPARENT);
    LCDSetMode(LCD_NOAUTOREFRESH|LCD_SHOWMENU);
    
    int exit = 0;
    LCDGetPos(&row,&col);
    row++;
    LCDSetPos(row,col);
    
    //changing this menu to be able to swap between Hotspot and Slave connections
    LCDMenu("Hotspot","Slave","Refresh","Back");
    
    LCDRefresh();
    while(exit==0) {
        switch(keycode){
            case KEY1:
                if(robiosParams.hotspotSsid==NULL) {
                    LCDSetPrintf(row, 2,   "No user hotspot configurations...      ");
                    LCDSetPrintf(row+1, 2, "Setting to default...                  ");
                    LCDSetPrintf(row+2, 2, "                                       ");
                    char temp[20]="";
                    char serial[20]="";
                    
                    system("sudo sed -i '13s|iface wlan0 inet manual|iface wlan0 inet static|'  /etc/network/interfaces");
                    system("sudo sed -i '14s|wpa-roam /etc/wpa_supplicant/wpa_supplicant.conf|address 10.1.1.1|'  /etc/network/interfaces");
                    system("sudo sed -i '15s|iface default inet dhcp|netmask 255.255.255.0|'  /etc/network/interfaces");
                    
                    temp[0]='\0';
                    serial[0]='\0';
                    //reading in the serial number into temp
                    sprintf(temp,"%s" , OSExecute("cat /proc/cpuinfo |grep Serial|cut -d' ' -f2"));
                    int i =0;
                    
                    //removing the leading 0's, storing serial into serial
                    while(temp[i]=='0') {
                        i++;
                        continue;
                    }
                    
                    int j=0;
                    
                    for (i; i<(int)strlen(temp); i++) {
                        serial[j] = temp[i];
                        j++;
                    }
                    
                    //Setting the SSID
                    strcpy(temp,"Pi_");
                    strcat(temp,serial);
                    
                    char current[100];
                    sprintf(current, "%s" , OSExecute("cat /etc/hostapd/hostapd.conf | grep ssid | cut -d '=' -f2"));
                    
                    char currIP[100];
                    sprintf(currIP, "%s", OSExecute("ifconfig wlan0 | grep addr:|cut -d 'B' -f1|cut -d ':' -f2"));
                    
                    char cmd[100]="";
                    cmd[0]='\0';
                    sprintf(cmd,"sudo sed -i '3s/.*/ssid=%s/' /etc/hostapd/hostapd.conf",temp);
                    system(cmd);
                    sprintf(cmd,"sudo sed -i '10s/.*/wpa_passphrase=raspberry/' /etc/hostapd/hostapd.conf");
                    system(cmd);
                    
                    //restarting the services for the new changes to ssid
                    system("sudo ifdown --force wlan0");
                    sleep(1);
                    system("sudo ifup wlan0");
                    sleep(1);
                    system("sudo service hostapd restart");
                    sleep(1);
                    system("sudo service isc-dhcp-server restart");
                    
                    robiosParams.wifiType = 0;
                    
                    LCDSetPrintf(row+2, 2, "Completed!");
                }
                
                //custom broadcast case
                else {
                    LCDSetPrintf(row, 2,   "Setting to hotspot...                    ");
                    LCDSetPrintf(row+1, 2, "                                         ");
                    LCDSetPrintf(row+2, 2, "                                         ");
                    
                    system("sudo sed -i '13s|iface wlan0 inet manual|iface wlan0 inet static|'  /etc/network/interfaces");
                    system("sudo sed -i '14s|wpa-roam /etc/wpa_supplicant/wpa_supplicant.conf|address 10.1.1.1|'  /etc/network/interfaces");
                    system("sudo sed -i '15s|iface default inet dhcp|netmask 255.255.255.0|'  /etc/network/interfaces");
                    
                    char current[100];
                    sprintf(current, "%s" , OSExecute("cat /etc/hostapd/hostapd.conf | grep ssid | cut -d '=' -f2"));
                    char currIP[50];
                    sprintf(currIP, "%s", OSExecute("ifconfig wlan | grep addr:|cut -d ':' -f2"));
                    
                        char cmd[100]="";
                        cmd[0]='\0';
                        sprintf(cmd,"sudo sed -i '3s/.*/ssid=%s/' /etc/hostapd/hostapd.conf", robiosParams.hotspotSsid);
                        system(cmd);
                        sprintf(cmd,"sudo sed -i '10s/.*/wpa_passphrase=%s/' /etc/hostapd/hostapd.conf", robiosParams.hotspotPassword);
                        system(cmd);
                        
                        //restarting the services for the new changes to ssid
                        
                        system("sudo ifdown --force wlan0");
                        sleep(1);
                        system("sudo ifup wlan0");
                        sleep(1);
                        system("sudo service hostapd restart");
                        sleep(1);
                        system("sudo service isc-dhcp-server restart");
                    
                    
                    robiosParams.wifiType = 2;
                    LCDSetPrintf(row+2, 2, "Completed!");
                }
                network_display();
                keycode = 0x0;
                break;
            case KEY2:
                if(robiosParams.slaveSsid==NULL) {
                    LCDSetPrintf(row, 2,   "No slave configurations...                ");
                    LCDSetPrintf(row+1, 0, "Failed!                                   ");
                    LCDSetPrintf(row+2, 0, "                                          ");
                }
                else {
                    LCDSetPrintf(row, 2,   "Setting to slave...                       ");
                    LCDSetPrintf(row+1, 0, "                                          ");
                    LCDSetPrintf(row+2, 0, "                                          ");
                    
                    //turning off broadcast servers
                    system("sudo service hostapd stop");
                    system("sudo service isc-dhcp-server stop");
                
                        //fixing the etc/network/interfaces file
                        system("sudo sed -i '13s|iface wlan0 inet static|iface wlan0 inet manual|'  /etc/network/interfaces");
                        system("sudo sed -i '14s|address 10.1.1.1|wpa-roam /etc/wpa_supplicant/wpa_supplicant.conf|'  /etc/network/interfaces");
                        system("sudo sed -i '15s|netmask 255.255.255.0|iface default inet dhcp|'  /etc/network/interfaces");
                        
                        char slaveSSID[M6MAIN_HOSTNAME_LEN];
                        slaveSSID[0] = '\0';
                        sprintf(slaveSSID, "sudo sed -i '5s/.*/  ssid=\"%s\"/' /etc/wpa_supplicant/wpa_supplicant.conf", robiosParams.slaveSsid);
                        
                        char slavePASS[M6MAIN_HOSTNAME_LEN];
                        slavePASS[0] = '\0';
                        sprintf(slavePASS, "sudo sed -i '6s/.*/  psk=\"%s\"/' /etc/wpa_supplicant/wpa_supplicant.conf", robiosParams.slavePassword);
                        
                        //changing the SSID and password to suit
                        system(slaveSSID);
                        system(slavePASS);
                    
                        system("sudo ifdown --force wlan0");
                        sleep(1);
                        system("sudo ifup wlan0");
                        sleep(1);
                }
                robiosParams.wifiType = 1;
                LCDSetPrintf(row+1, 2, "Complete!");
                network_display();
                keycode = 0x0;
                break;
            case KEY3:
                LCDSetPrintf(row, 2,   "Refreshing...                            ");
                LCDSetPrintf(row+1, 0, "                                         ");
                LCDSetPrintf(row+2, 0, "                                         ");
                
                system("sudo ifdown --force wlan0");
                sleep(1);
                system("sudo ifup wlan0");
                sleep(1);
                LCDSetPrintf(row+1, 2, "Done!   ");
                network_display();
                keycode = 0x0;
                break;
            case KEY4:
            {
                system_info();
                exit = 1;
                break;
            }

            default:
                break;
        }
        LCDRefresh2();
    }
    return 0;
}

int system_exit() {
    EYEFONT eyeFontDefaultYellow;
    EYEFONT eyeFontBoldWhite;
    
    strcpy(eyeFontDefaultYellow.fontName,FONTDEFAULT);
    eyeFontDefaultYellow.fontColour = LCD_YELLOW;
    strcpy(eyeFontBoldWhite.fontName,FONTBOLD);
    eyeFontBoldWhite.fontColour = LCD_WHITE;

    int row, col;

    LCDSetPos(0,0);
    LCDPrintfFont(eyeFontBoldWhite,"Admin Page\n\n");
    LCDGetPos(&row,&col);
    LCDSetPos(row,col);
    
    keycode = 0x0;
    
    int exit = 0;
    KEYSetTM(KEYTM_CLASSIC);
    LCDSetColorRaw(LCD_WHITE, LCD_BLACK, LCD_BGCOL_TRANSPARENT);
    LCDSetMode(LCD_NOAUTOREFRESH|LCD_SHOWMENU);
    
    char hostname[50];
    
    gethostname(hostname,30);
    
    char results[100] = "";
    char screenType[10] = "";
    int screenMode = -1;
    sprintf(results, OSExecute("echo /dev/fb*"));
    if((strstr(results, "fb1")==NULL)) {
        sprintf(screenType, " LCD");
        screenMode = 0;
    }
    else {
        sprintf(screenType, " HDMI");
        screenMode = 1;
    }
    
    LCDRefresh();
    while (exit == 0)
    {
        LCDMenu(screenType,"Reboot","Exit","Back");
        LCDRefresh2();
        switch (keycode)
        {
            case KEY1:
            {
                if(screenMode == 0) {
                    char cmd[50]="";
                    cmd[0]='\0';
                    
                    LCDPrintf("Changing to LCD mode...\n");
                    LCDPrintf("Rebooting now...\n");
                    
                    if(robiosParams.lcdRot==1) {
                        system("sudo cp -rf /home/pi/eyebot/bin/lcd/modules-LCD-flip  /etc/modules");
                        system("sudo cp -rf /home/pi/eyebot/bin/lcd/99-calibration-flip.conf  /etc/X11/xorg.conf.d/99-calibration.conf");
                        system("sudo cp -rf /home/pi/eyebot/bin/lcd/99-fbturbo.conf  /usr/share/X11/xorg.conf.d/99-fbturbo.conf");
                        sprintf(cmd,"sudo sed -i '53s/.*/LCD 0 0 1/' /home/pi/eyebot/bin/hdt.txt");
                    }
                    else {
                        system("sudo cp -rf /home/pi/eyebot/bin/lcd/modules-LCD-noflip  /etc/modules");
                        system("sudo cp -rf /home/pi/eyebot/bin/lcd/99-calibration-noflip.conf  /etc/X11/xorg.conf.d/99-calibration.conf");
                        system("sudo cp -rf /home/pi/eyebot/bin/lcd/99-fbturbo.conf  /usr/share/X11/xorg.conf.d/99-fbturbo.conf");
                        
                        sprintf(cmd,"sudo sed -i '53s/.*/LCD 0 0 0/' /home/pi/eyebot/bin/hdt.txt");
                    }
                    system(cmd);
                    sleep(1);
                }
                
                else {
                    char cmd[50]="";
                    cmd[0]='\0';
                    
                    LCDPrintf("Changing to HDMI mode...\n");
                    LCDPrintf("Rebooting now...\n");
                    
                    system("sudo cp -rf /home/pi/eyebot/bin/lcd/modules-HDMI  /etc/modules");
                    system("sudo cp -rf /home/pi/eyebot/bin/lcd/99-fbturbo-HDMI.conf  /usr/share/X11/xorg.conf.d/99-fbturbo.conf");
                    
                    if(robiosParams.lcdSize==1) {
                        if(robiosParams.lcdRot == 1) {
                            sprintf(cmd,"sudo sed -i '53s/.*/LCD 1 1 1/' /home/pi/eyebot/bin/hdt.txt");
                        }
                        else {
                            sprintf(cmd,"sudo sed -i '53s/.*/LCD 1 1 0/' /home/pi/eyebot/bin/hdt.txt");
                        }
                    }
                    else {
                        if(robiosParams.lcdRot == 1) {
                            sprintf(cmd,"sudo sed -i '53s/.*/LCD 1 0 1/' /home/pi/eyebot/bin/hdt.txt");
                        }
                        else {
                            sprintf(cmd,"sudo sed -i '53s/.*/LCD 1 0 0/' /home/pi/eyebot/bin/hdt.txt");
                        }
                    }
                    system(cmd);
                    sleep(1);
                }
                system("sudo reboot");
                keycode = 0x0;
                break;
            }
            case KEY2:
            {
                LCDPrintf("Rebooting now...\n");
                if (strcmp(hostname,"raspberrypi\n"))
                    system("sudo reboot");
                else
                    system("reboot");
                keycode = 0x0;
                break;
            }
            case KEY3:
            {
                if (strcmp(hostname,"raspberrypi\n"))
                    system("sudo pkill EyeBot");
                else
                    system("pkill EyeBot");
                keycode = 0x0;
                break;
            }
            case KEY4:
            {
                system_info();
                exit = 1;
                keycode = 0x0;
                break;
            }
            default:
            {
                keycode = 0x0;

            }
        }
        usleep(LOOPSLEEPTIME);
    }
    return 0;
}

int page_main(void)
{
	FBINFO fb;
	int p, row, col;
	char hostname[M6MAIN_HOSTNAME_LEN];
	char ipAddress[400] = "";
    char temp[20]="";
    char serial[20]="";
    
	EYEFONT eyeFontDefaultYellow;
	EYEFONT eyeFontBoldWhite;
	
	strcpy(eyeFontDefaultYellow.fontName,FONTDEFAULT);
	eyeFontDefaultYellow.fontColour = LCD_YELLOW;
	strcpy(eyeFontBoldWhite.fontName,FONTBOLD);
	eyeFontBoldWhite.fontColour = LCD_WHITE;
	
	pthread_create(&threadKeyInput,NULL,keyInputThread,NULL);
	pthread_create(&threadIRInput,NULL,irInputThread,NULL);
	pthread_create(&threadScreenRefresh,NULL,screenRefreshThread,NULL);
	
	KEYSetTM(KEYTM_CLASSIC);
	LCDSetColorRaw(LCD_WHITE, LCD_BLACK, LCD_BGCOL_TRANSPARENT);
	LCDSetMode(LCD_NOAUTOREFRESH|LCD_SHOWMENU);
	LCDGetFBInfo(&fb);

	LCDClear();
	keycode = 0x0;
    
    //custom case
    if(robiosParams.wifiType==2) {
        system("sudo sed -i '13s|iface wlan0 inet manual|iface wlan0 inet static|'  /etc/network/interfaces");
        system("sudo sed -i '14s|wpa-roam /etc/wpa_supplicant/wpa_supplicant.conf|address 10.1.1.1|'  /etc/network/interfaces");
        system("sudo sed -i '15s|iface default inet dhcp|netmask 255.255.255.0|'  /etc/network/interfaces");
        
        char current[100];
        sprintf(current, "%s" , OSExecute("cat /etc/hostapd/hostapd.conf | grep ssid | cut -d '=' -f2"));
        char currIP[50];
        sprintf(currIP, "%s", OSExecute("ifconfig wlan | grep addr:|cut -d ':' -f2"));
        
        if((strstr(current,robiosParams.hotspotSsid)!=current)||(strstr(currIP, "10.1.1.1")!=currIP)) {
            char cmd[100]="";
            cmd[0]='\0';
            sprintf(cmd,"sudo sed -i '3s/.*/ssid=%s/' /etc/hostapd/hostapd.conf", robiosParams.hotspotSsid);
            system(cmd);
            sprintf(cmd,"sudo sed -i '10s/.*/wpa_passphrase=%s/' /etc/hostapd/hostapd.conf", robiosParams.hotspotPassword);
            system(cmd);
            
            //restarting the services for the new changes to ssid
            
            system("sudo ifdown --force wlan0");
            sleep(1);
            system("sudo ifup wlan0");
            sleep(1);
            system("sudo service hostapd restart");
            sleep(1);
            system("sudo service isc-dhcp-server restart");
        }
    }
    
    //slave case
    else if(robiosParams.wifiType == 1) {
        //turning off broadcast servers
        system("sudo service hostapd stop");
        system("sudo service isc-dhcp-server stop");
        
        char currIP[100];
        sprintf(currIP, "%s", OSExecute("ifconfig wlan0 | grep addr:|cut -d 'B' -f1|cut -d ':' -f2"));
        
        if(true) {
            //fixing the etc/network/interfaces file
            system("sudo sed -i '13s|iface wlan0 inet static|iface wlan0 inet manual|'  /etc/network/interfaces");
            system("sudo sed -i '14s|address 10.1.1.1|wpa-roam /etc/wpa_supplicant/wpa_supplicant.conf|'  /etc/network/interfaces");
            system("sudo sed -i '15s|netmask 255.255.255.0|iface default inet dhcp|'  /etc/network/interfaces");
        
            char slaveSSID[M6MAIN_HOSTNAME_LEN];
            slaveSSID[0] = '\0';
            sprintf(slaveSSID, "sudo sed -i '5s/.*/  ssid=\"%s\"/' /etc/wpa_supplicant/wpa_supplicant.conf", robiosParams.slaveSsid);
        
            char slavePASS[M6MAIN_HOSTNAME_LEN];
            slavePASS[0] = '\0';
            sprintf(slavePASS, "sudo sed -i '6s/.*/  psk=\"%s\"/' /etc/wpa_supplicant/wpa_supplicant.conf", robiosParams.slavePassword);
        
            //changing the SSID and password to suit
            system(slaveSSID);
            system(slavePASS);
            
            sleep(1);
            system("sudo ifdown --force wlan0");
            sleep(1);
            system("sudo ifup wlan0");
        }
    }
    
    //default case
    else {
        system("sudo sed -i '13s|iface wlan0 inet manual|iface wlan0 inet static|'  /etc/network/interfaces");
        system("sudo sed -i '14s|wpa-roam /etc/wpa_supplicant/wpa_supplicant.conf|address 10.1.1.1|'  /etc/network/interfaces");
        system("sudo sed -i '15s|iface default inet dhcp|netmask 255.255.255.0|'  /etc/network/interfaces");
        
        temp[0]='\0';
        serial[0]='\0';
        //reading in the serial number into temp
        sprintf(temp,"%s" , OSExecute("cat /proc/cpuinfo |grep Serial|cut -d' ' -f2"));
        int i =0;
        
        //removing the leading 0's, storing serial into serial
        while(temp[i]=='0') {
            i++;
            continue;
        }
        
        int j;
        j=0;
        
        for (i;i<(int)strlen(temp); i++) {
            serial[j] = temp[i];
            j++;
        }
        
        //Setting the SSID
        strcpy(temp,"Pi_");
        strcat(temp,serial);
        
        char current[100];
        sprintf(current, "%s" , OSExecute("cat /etc/hostapd/hostapd.conf | grep ssid | cut -d '=' -f2"));
        
        char currIP[100];
        sprintf(currIP, "%s", OSExecute("ifconfig wlan0 | grep addr:|cut -d 'B' -f1|cut -d ':' -f2"));
        
        if((strstr(current,temp)!=current)||(strstr(currIP, "10.1.1.1")!=currIP)) {
            char cmd[100]="";
            cmd[0]='\0';
            sprintf(cmd,"sudo sed -i '3s/.*/ssid=%s/' /etc/hostapd/hostapd.conf",temp);
            system(cmd);
            sprintf(cmd,"sudo sed -i '10s/.*/wpa_passphrase=raspberry/' /etc/hostapd/hostapd.conf");
            system(cmd);
            
            //restarting the services for the new changes to ssid
            system("sudo ifdown --force wlan0");
            sleep(1);
            system("sudo ifup wlan0");
            system("sudo service hostapd restart");
            sleep(1);
            system("sudo service isc-dhcp-server restart");
        }
    }
    
        int intVolt = 0;
        char boardVer[64];
        strcpy(boardVer, "                 ");
    
    
        int COUNTSerial = 5;
        LCDClear();
        LCDMenu("System","Hardware","Software","Demo");
    
        do
        {
            //LCDClear();
            LCDSetColorRaw(LCD_WHITE, LCD_BLACK, LCD_BGCOL_TRANSPARENT);
            LCDSetMode(LCD_NOAUTOREFRESH|LCD_SHOWMENU);
            
            row = M6MAIN_TEXTROW;
            col = M6MAIN_TEXTCOL;
            
            if (screenRefresh)
            {
                LCDMenu("System","Hardware","Software","Demo");
                LCDRefresh();
            }
            
            screenRefresh = 0;
            
            if(COUNTSerial == 5) {
                //check format
                int on = OSVersionIO(boardVer);
                if(on==1) {
                    strcpy(boardVer, "Not Connected");
                    
                }
                else {
                    usleep(1000);
                    intVolt = ANALOGVoltage();
                }
                COUNTSerial = 0;
            }
            
            COUNTSerial++;
            
            LCDSetPos(row,col);
        
            LCDPrintfFont(eyeFontBoldWhite," Welcome to EyeBot 7.\n\n RoBIOS version: ");
            LCDPrintfFont(eyeFontDefaultYellow,"  %s\n",ROBIOS_VERSION);
        
            LCDPrintfFont(eyeFontBoldWhite, " IO Board Version: ");
        
            LCDPrintfFont(eyeFontDefaultYellow, "%s             \n\n", boardVer);
            
            if(strcmp(boardVer, "Not Connected")!=0) {
                float decVolt = (float)intVolt/100;
                
                LCDGetPos(&row,&col);
                
                if(decVolt < vbatt_min_8)
                    decVolt = vbatt_min_8;
                if(decVolt > vbatt_max_8)
                    decVolt = vbatt_max_8;
                
                p = 100 * (decVolt / vbatt_max_8);
                
                LCDTextBar(row, col, fb.cursor.xmax, p, M6MAIN_BATTBAR_COLOR);
                
                //displaying the battery status
                LCDSetPrintf(row, col+1, "Battery: ");
                LCDPrintfFont(eyeFontDefaultYellow, "%4.2f V\n\n", decVolt);
            }
            
            else {
                LCDGetPos(&row,&col);
                //LCDTextBar(row, col, fb.cursor.xmax, 0, LCD_BLACK);
                LCDSetPrintf(row, col+1, "                                              ");
                LCDPrintf("                                                            \n");


            }
            
            
            // displaying the IP Address of the ethernet connection, default to 10.0.0.1 if none
            LCDPrintfFont(eyeFontBoldWhite," IP-Addr  : ");
            ipAddress[0] = '\0';
            
            sprintf(ipAddress,"%s",OSExecute("ifconfig eth0"));
            if (ipAddress[68] == 'i')
            {
                LCDPrintfFont(eyeFontDefaultYellow,"%s      \n", OSExecute("ifconfig eth0 | sed -rn 's/.*r:([^ ]+) .*/\\1/p' | awk '{printf \"%s \",$0} END {print \"\"}'"));
            }
        
            else
            {
                LCDPrintfFont(eyeFontDefaultYellow,"10.0.0.1        \n");
            }
        
            //displaying the hostname
            gethostname(hostname,M6MAIN_HOSTNAME_LEN);
            LCDPrintfFont(eyeFontBoldWhite," Hostname : ");
            LCDPrintfFont(eyeFontDefaultYellow,"%s\n", hostname);
        
            //displaying the mac address
            //using fopen
            printfile("/sys/class/net/eth0/address", "MAC-Addr ");
        
            strcpy(ipAddress, "");
        
            char wifiServer[50] = "";
            sprintf(wifiServer,"%s",OSExecute("sudo service isc-dhcp-server status"));
            
            char pass[100];
            sprintf(pass, "%s" , OSExecute("cat /etc/hostapd/hostapd.conf | grep pass | cut -d '=' -f2"));
            
            char currIP[100];
            sprintf(currIP, "%s", OSExecute("ifconfig wlan0 | grep addr:|cut -d 'B' -f1|cut -d ':' -f2"));

            
            if ((strstr(wifiServer,"not")==NULL)&&(robiosParams.wifiType==0)&&(strlen(currIP)!=0)) {
                char* buffer = malloc(20*sizeof(char));
                char line[100] = "";
                
                //displaying the SSID
                LCDPrintfFont(eyeFontBoldWhite, "\n Wifi SSID: ");
                
                sprintf(line,"%s", OSExecute("cat /etc/hostapd/hostapd.conf |grep ssid | cut -d '=' -f2"));
                buffer = strtok(line," \n");
                LCDPrintfFont(eyeFontDefaultYellow, "%s             \n", buffer);
                
                sprintf(line,"%s", OSExecute( "ifconfig wlan0 | grep addr:|cut -d 'B' -f1|cut -d ':' -f2"));
                buffer = strtok(line," ");
                
                LCDPrintfFont(eyeFontBoldWhite, " Wifi IP  : ");
                LCDPrintfFont(eyeFontDefaultYellow, "%s             \n", buffer);
                
                LCDPrintfFont(eyeFontBoldWhite, " Wifi Pass: ");
                LCDPrintfFont(eyeFontDefaultYellow, "%s             \n", pass);
                
            }
            
            else if ((strstr(wifiServer,"not")!=NULL)&&(robiosParams.wifiType==1)&&(strlen(currIP)!=0)){
                LCDPrintfFont(eyeFontBoldWhite, "\n Wifi SSID: ");
                LCDPrintfFont(eyeFontDefaultYellow, "%s (SLAVE MODE)\n", robiosParams.slaveSsid);
                
                LCDPrintfFont(eyeFontBoldWhite, " Wifi IP  : ");
                LCDPrintfFont(eyeFontDefaultYellow, "%s          \n", currIP);
                
                LCDPrintfFont(eyeFontBoldWhite, " Wifi Pass: ");
                LCDPrintfFont(eyeFontDefaultYellow, "%s            \n", robiosParams.slavePassword);
            }
            
            else {
                LCDPrintfFont(eyeFontBoldWhite, "\n Wifi SSID: ");
                LCDPrintfFont(eyeFontDefaultYellow, "Not Connected          \n");
                
                LCDPrintfFont(eyeFontBoldWhite, " Wifi IP  : ");
                LCDPrintfFont(eyeFontDefaultYellow, "Not Connected          \n");
                
                LCDPrintfFont(eyeFontBoldWhite, " Wifi Pass: ");
                LCDPrintfFont(eyeFontDefaultYellow, "Not Connected            \n");
            }

            LCDRefresh2();
		
            switch(keycode)
            {
                case KEY1:
                {
                    system_info();
                    LCDClear();
                    keycode = 0x0;
                    LCDMenu("System","Hardware","Software","Demo");
                    break;
                }
                case KEY2:
                {
                    show_hdt();
                    LCDClear();
                    keycode = 0x0;
                    LCDMenu("System","Hardware","Software","Demo");
                    break;
                }
                case KEY3:
                {
                    show_exec();
                    LCDClear();
                    keycode = 0x0;
                    LCDMenu("System","Hardware","Software","Demo");
                    break;
                }
                case KEY4:
                {
                    demoPage();
                    LCDClear();
                    keycode = 0x0;
                    LCDMenu("System","Hardware","Software","Demo");
                    break;
                }
            }
            usleep(LOOPSLEEPTIME);
        }
	while(1);

	LCDClear();

	return 0;
}

void getParams()
{
	FILE *fp;
	fp = fopen("/home/pi/eyebot/bin/hdt.txt","r");
	if (fp == NULL) {
        fprintf(stderr, "No HDT file present");
        exit(EXIT_FAILURE);
    }
	else {
        char* line = malloc(300*sizeof(char));
        char* buffer = malloc(100*sizeof(char));
        ssize_t readLine;
        size_t len = 0;
        
        //setting defaults
        robiosParams.runROBIOS = 1;
        robiosParams.irEnable = 1;
        robiosParams.irCode = 786;
        robiosParams.irDelay = 5;
        
        robiosParams.hotspotSsid = NULL;
        robiosParams.hotspotPassword = NULL;
        robiosParams.slaveSsid = NULL;
        robiosParams.slavePassword = NULL;
        
        while((readLine = getline(&line, &len, fp)) != -1) {
            if(strstr(line,"IRPARAMS")==line) {
                buffer = strtok(line," \n");

                //checking for the Enable
                buffer = strtok(NULL, " \n");
                robiosParams.irEnable = atoi(buffer);
                fprintf(stdout, "Enable: %d\n", atoi(buffer));
                
                //checking for code
                buffer = strtok(NULL, " \n");
                robiosParams.irCode = atoi(buffer);
                fprintf(stdout, "Code: %d\n", atoi(buffer));
                
                //checking for delay
                buffer = strtok(NULL, " \n");
                robiosParams.irDelay = atoi(buffer);
                fprintf(stdout, "Delay: %d\n", atoi(buffer));
            }
            
            //robios on/off
            if(strstr(line,"ROBIOS")==line) {
                buffer = strtok(line, " \n");
                buffer = strtok(NULL, " \n");
                robiosParams.runROBIOS = atoi(buffer);
                fprintf(stdout, "ROBIOS: %d\n", atoi(buffer));
                buffer = strtok(NULL, " \n");
                robiosParams.fontSize = atoi(buffer);
                fprintf(stdout, "Font Size: %d\n", atoi(buffer));
            }
            
            if(strstr(line,"RPI")==line) {
                buffer = strtok(line," \n");
                buffer = strtok(NULL, " \n");
                robiosParams.rPi = atoi(buffer);
                fprintf(stdout, "RPi version: %d\n", robiosParams.rPi);
            }
            
            //networking
            if(strstr(line, "WIFI")==line) {
                buffer = strtok(line, " \n"); //WIFI
                buffer = strtok(NULL, " \n"); //type
                
                robiosParams.wifiType = atoi(buffer);
                if(robiosParams.wifiType == 2) {
                    fprintf(stdout, "Wifi set to hotspot custom...\n");
                }
                else if(robiosParams.wifiType == 1) {
                    fprintf(stdout, "Wifi set to slave...\n");
                }
                else {
                    fprintf(stdout, "Wifi set to default...\n");
                }
            }
            
            if(strstr(line, "HOTSPOT")==line) {
                buffer = strtok(line, " \n"); //HOTSPOT
                
                buffer = strtok(NULL, " \n"); //ssid
                robiosParams.hotspotSsid = (char*)calloc(1,100);
                memcpy(robiosParams.hotspotSsid, buffer, strlen(buffer));
                
                buffer = strtok(NULL, " \n"); //password
                robiosParams.hotspotPassword = (char*)calloc(1,100);
                memcpy(robiosParams.hotspotPassword, buffer, strlen(buffer));
            }
            
            if(strstr(line, "SLAVE")==line) {
                buffer = strtok(line, " \n"); //SLAVE
                
                buffer = strtok(NULL, " \n"); //ssid
                robiosParams.slaveSsid = (char*)calloc(1,100);
                memcpy(robiosParams.slaveSsid, buffer, strlen(buffer));
                
                buffer = strtok(NULL, " \n"); //password
                robiosParams.slavePassword = (char*)calloc(1,100);
                memcpy(robiosParams.slavePassword, buffer, strlen(buffer));
            }
            
            //robios on/off
            if(strstr(line,"LCD")==line) {
                
                buffer = strtok(line, " \n"); //LCD
                
                buffer = strtok(NULL, " \n"); //1 or 0
                robiosParams.lcdMode = atoi(buffer);
                fprintf(stdout, "LCD: %d\n", atoi(buffer));
    
                buffer = strtok(NULL, " \n"); //Fullscreen 1 or 0
                robiosParams.lcdSize = atoi(buffer);
                fprintf(stdout, "Fullscreen: %d\n", atoi(buffer));
                
                buffer = strtok(NULL, " \n"); //Rotate 1 or 0
                robiosParams.lcdRot = atoi(buffer);
                fprintf(stdout, "Rotation: %d\n", atoi(buffer));
                
                if (robiosParams.lcdMode==1) {
                    system("sudo cp -rf /home/pi/eyebot/bin/lcd/modules-HDMI  /etc/modules");
                    system("sudo cp -rf /home/pi/eyebot/bin/lcd/99-fbturbo-HDMI.conf  /usr/share/X11/xorg.conf.d/99-fbturbo.conf");
                }
                else {
                    if(robiosParams.lcdRot==1) {
                        system("sudo cp -rf /home/pi/eyebot/bin/lcd/modules-LCD-flip  /etc/modules");
                        system("sudo cp -rf /home/pi/eyebot/bin/lcd/99-calibration-flip.conf  /etc/X11/xorg.conf.d/99-calibration.conf");
                        system("sudo cp -rf /home/pi/eyebot/bin/lcd/99-fbturbo.conf  /usr/share/X11/xorg.conf.d/99-fbturbo.conf");
                    }
                    else {
                        system("sudo cp -rf /home/pi/eyebot/bin/lcd/modules-LCD-noflip  /etc/modules");
                        system("sudo cp -rf /home/pi/eyebot/bin/lcd/99-calibration-noflip.conf  /etc/X11/xorg.conf.d/99-calibration.conf");
                        system("sudo cp -rf /home/pi/eyebot/bin/lcd/99-fbturbo.conf  /usr/share/X11/xorg.conf.d/99-fbturbo.conf");
                    }
                }
                
            }
		}
        fprintf(stdout, "Completed Param checking\n");
        free(line);
	}
    
    if (robiosParams.irEnable == 1)	{
        system("gpio mode 29 out");
        system("gpio write 29 1");
		IRTVInit(robiosParams.irCode);
	}
    
    if(robiosParams.runROBIOS == 0) {
        exit(EXIT_SUCCESS);
    }
    
	IRTVSetHoldDelay(robiosParams.irDelay);
    
	if (fp != NULL)
	{
		fclose(fp);
	}
}

// *** MAIN ***
int main(int argc, char *argv[])
{
	getParams();

	keycode = 0x0;
	screenRefresh = 0;

	//Enable multi-thread support
	if (XInitThreads() == 0) {
		fprintf(stderr,"\nERROR: cannot enable multi-thread support\n");
		exit(EXIT_FAILURE);
	}
    
	if(LCDInit()<0) {
		fprintf(stderr, "LCDInit Error! Exiting!\n");
		return -1;
	}
    
    switch(robiosParams.fontSize) {
        case 8:
            strcpy(FONTDEFAULT, "-adobe-courier-medium-r-normal--11-80-100-100-m-60-iso8859-1");
            strcpy(FONTBOLD, "-adobe-courier-bold-r-normal--11-80-100-100-m-60-iso8859-1");
            LCDSetFontRaw(FONTDEFAULT);
            break;
        case 10:
            strcpy(FONTDEFAULT, "-adobe-courier-medium-r-normal--14-100-100-100-m-90-iso8859-1");
            strcpy(FONTBOLD, "-adobe-courier-bold-r-normal--14-100-100-100-m-90-iso8859-1");
            LCDSetFontRaw(FONTDEFAULT);
            break;
        case 12:
            strcpy(FONTDEFAULT, "-adobe-courier-medium-r-normal--17-120-100-100-m-100-iso8859-1");
            strcpy(FONTBOLD, "-adobe-courier-bold-r-normal--17-120-100-100-m-100-iso8859-1");
            LCDSetFontRaw(FONTDEFAULT);
            break;
        case 14:
            strcpy(FONTDEFAULT, "-adobe-courier-medium-r-normal--20-140-100-100-m-110-iso8859-1");
            strcpy(FONTBOLD, "-adobe-courier-bold-r-normal--20-140-100-100-m-110-iso8859-1");
            LCDSetFontRaw(FONTDEFAULT);
            break;
        case 18:
            strcpy(FONTDEFAULT, "-adobe-courier-medium-r-normal--25-180-100-100-m-150-iso8859-1");
            strcpy(FONTBOLD, "-adobe-courier-bold-r-normal--25-180-100-100-m-150-iso8859-1");
            LCDSetFontRaw(FONTDEFAULT);
            break;
        case 24:
            strcpy(FONTDEFAULT, "-adobe-courier-medium-r-normal--34-240-100-100-m-200-iso8859-1");
            strcpy(FONTBOLD, "-adobe-courier-bold-r-normal--34-240-100-100-m-200-iso8859-1");
            LCDSetFontRaw(FONTDEFAULT);
            break;
        default:
            strcpy(FONTDEFAULT, "7x14");
            strcpy(FONTBOLD, "7x14bold");
            LCDSetFontRaw(FONTDEFAULT);
            fprintf(stdout, "Invalid Font Size: Defaulting..., choose 8, 10, 12, 14, 18 or 24\n");
            break;
    }
    
    if(KEYInit()<0) {
        fprintf(stderr, "KEYInit Error! Exiting!\n");
        return -1;
    }
	
    //show main page
	page_main();

	// release I/O
	LCDRelease();
	KEYRelease();

	return 0;
}
