/**
 * \file		key.h
 * \author      Marcus Pham & Remi KEAT
 * \brief		Header file for the key functions
 */

#ifndef KEY_H_
#define KEY_H_

#include "types.h"
#include <stdio.h>

//initialise check for key functions
int KEY_INITIAL;

int KEYInit(void);
int KEYRelease(void);

int KEYInside(int x, int y, BOX rect);
int KEYIdle(int idle);
int KEYSetTM(int mode);
int KEYGetTM(TOUCH_MAP** ptouch_map);
int KEYSetRegion(int index, BOX *region);
int KEYGetRegion(int index, BOX *region);
int KEYNoTouch(TOUCH_EVENT* rawtouch);
int KEYGetRaw(TOUCH_EVENT* rawtouch);

int KEYDecode(TOUCH_EVENT* rawtouch);
int KEYActivateEscape(int escape);
COORD_PAIR KEYXYRaw(void);

#endif /* KEY_H_ */
