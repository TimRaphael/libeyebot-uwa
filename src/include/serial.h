/**
 * \file   serial.h
 * \brief  Defines all the functions to connect to a serial USB/serial connection
 * \author Marcus Pham
 */

#ifndef SERIAL_H_
#define	SERIAL_H_

#include <stdio.h>     // Standard input/output definitions
#include <string.h>    // String function definitions
#include <unistd.h>   // UNIX standard function definitions
#include <fcntl.h>      // File control definitions
#include <errno.h>     // Error number definitions
#include <termios.h>  // POSIX terminal control definitions
#include <time.h>
#include <stdbool.h> //For boolean usage

#include "types.h"
#include "system.h"

#define TIMEOUT 100
#define DEFAULT_BAUD 115200

char USBPATH1[HDT_MAX_PATHCHAR];
char USBPATH2[HDT_MAX_PATHCHAR];
char USBPATH3[HDT_MAX_PATHCHAR];
char USBPATH4[HDT_MAX_PATHCHAR];

int SERPathInit;
int SERType;
int SERBaud[5];
int SERHandshake[5];

int fd;                                             //the default port


#endif	/* SERIAL_H */

