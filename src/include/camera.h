/**
 *  \file camera.h
 *  \brief Camera routines for the Raspberry Pi 2, modded for the EyeBot.
 *  \author Jeremy Tan & Marcus Pham
 */

#ifndef _CAMERA_H
#define _CAMERA_H

#include <opencv/cv.h>
#include <opencv2/highgui/highgui_c.h>

#include "types.h"

int CAMSetMode(int mode);
int CAMGetMode ();

#endif
