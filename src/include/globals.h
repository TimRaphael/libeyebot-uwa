/**
 * \file    globals.h
 * \brief   Header file for global variables
 * \author  Remi KEAT
 */

#ifndef GLOBALS_H_
#define GLOBALS_H_

#include "types.h"

struct mpsse_context* gDeviceHandle;
LCD_HANDLE* gLCDHandle;
bool gLCDEnabled;
int gCurPosX, gCurPosY;
int gMousePosX, gMousePosY, gMouseButton;
TOUCH_MAP* gTouchMap;

#endif /* GLOBALS_H_ */
