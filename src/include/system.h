/**
 * \file	system.h
 * \brief   Header file for system functions
 * \author  Remi KEAT & Marcus Pham
 */

/*! \mainpage EyeBot 7 API
 *
 * \section Introduction
 *
 * An API to desribe the available functions on the EyeBot 7.
 * This enables users to control hardware components connected to the IO board like:
 *      - Motors
 *      - Psds
 *      - Servos
 *      - Encoders
 *      - Digital IO
 *      - Magnet
 *
 * The EyeBot 7 API also contains functions for controlling Raspberry Pi specific functions:
 *      - LCD
 *      - LCD Touch-screen and
 *      - Camera  Lists
 *      - IRTV remote
 *      - Serial Communication
 *      - Audio Functions
 *
 * As well as some core functionality:
 *      - Lists (on the LCD)
 *      - Multitasking
 *      - Timers
 *
 * All configurations are able to be set inside the hdt file (hardware description table)
 * (eg. no motors, tables, SSID, drive, USB serial params)
 * default location: /home/pi/eyebot/bin/hdt.txt
 *
 * You can also access this page when connected to the EyeBot via its IP Address
 *
 * \subsection Usage
 *
 * Programs requiring the usage of the functions defined here must:
 * 1.   Use the header command: #include "eyebot.h"
 * 2.   Compile the program with command line: gccarm (takes place of gcc compiler)
 *      The usual arguments to the compiler are also available
 *
 * \subsection Credits
 * The EyeBot 7 was designed, created and programmed at the University of Western Australia by:
 *
 * Director:    Professor Thomas Braunl
 *
 * Advisor:     Ivan Neubronner
 *
 * Hardware:    Franco Hidalgo (2015) & Thomas Smith (2014)
 *
 * Software:    Marcus Pham (2015) & Stuart Howard (2014)
 *
 * Additional Credit:
 *              Remi Keat (2013), Ke Feng - Image Processing (2015), Scott Mon (2015)
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

#include "types.h"
#include <sys/sysinfo.h>

void OSStrcpy_n(char* __dest, const char* __src, size_t __n);
void OSTrimline(char *src);

int OSError(char *msg, int number, bool deadend);
int OSInfoCPU (INFO_CPU* infoCPU);
int OSInfoMem (INFO_MEM* infoMem);
int OSInfoProc (INFO_PROC* infoProc);
int OSInfoMisc (INFO_MISC* infoMisc);

#endif /* SYSTEM_H_ */
