/**
 * \file    psd_sensors.h
 * \brief   Header file for the PSD sensors functions
 * \author  Marcus Pham
 */

#ifndef PSD_SENSORS_H_
#define PSD_SENSORS_H_

#include "serial.h"
#include "types.h"

HDT_PSD *PSDFirst;

int PSDInitialised;

int PSDInit();

#endif /* PSD_SENSORS_H_ */
