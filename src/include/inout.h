/**
 * \file   inout.h
 * \brief  Defines all the functions to connect to a serial USB/serial connection
 * \author Marcus Pham
 */

#ifndef INOUT_H_
#define	INOUT_H_

#include "serial.h"

int DIGITALInitialised;

int DIGITALInit(void);                          // starts the serial
int IOBoardVer(char* version);

#endif /* INOUT_H */
