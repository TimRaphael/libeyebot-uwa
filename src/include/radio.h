

#include "types.h"

#define SERVERPORT "4950"	// the port users will be connecting to

#define MAXBUFLEN 100

// globals
extern int sockfdListening;


/*		Input:          NONE
		Output:         (return code)   0 = OK
						-1 = Error: Radio functions not enabled (Radio-Key required)
		Semantics:      Initializes and starts the radio communication.*/
int RADIOInit(void);


/*		Input:          (id)        the EyeBot ID number of the message 
                        (buf)  		message contents
						(size) 		message length
		Output:         (return code) 0 = OK
                                      1 = send buffer is full or message is too long.
		Semantics:      Send message to another EyeBot. Message
                        length must be below or equal to MAXBUFLEN.*/
int RADIOSend(int id, BYTE* buf, int size);

/*      Input:          NONE
        Output:         (id)            EyeBot ID number of the message source
                        (buf)        	message contents
						(size) 			message length
        Semantics:      Returns the next message buffered. Messages are
                        returned in the order they are
                        received. Receive will block the calling
                        process if no message has been received until
                        the next one comes in.  The buffer must have
                        room for MAXBUFLEN bytes.*/
int RADIORecv(int* id, BYTE* buf, int* size);

/*      Input:          NONE
        Output:         (id)            EyeBot ID number of the message source
						returns the number of user messages in the buffer
        Semantics:      Function returns the number of buffered messages.
                        This function should be called before
                        receiving, if blocking is to be avoided.*/
int RADIOCheck(int *id);

/* 		Input:          NONE
		Output:         (return code) 0 = OK
		Semantics:      Terminate radio communication.*/
int RADIORelease(void);









