/**
 * \file improc.h
 * \brief  image processing functions
 * \author Thomas Braunl
 */

#ifndef IMPROC_H_
#define IMPROC_H_

#include "types.h"

/** Success code **/
#define IP_SUCCESS 1
/** Failure code **/
#define IP_FAILURE 0

#endif


