/**
 * \file		irtv.h
 * \brief   Header file for the IRTV functions
 * \author  Remi KEAT & Marcus Pham
 */

#ifndef IRTV_H_
#define IRTV_H_

#include "spi.h"
#include <lirc/lirc_client.h>
#include "types.h"


int IRTV_INITIAL;

int IRTVInit(int semantics);
void IRTVRelease(void);
int IRTVGetControllerCode();
void IRTVSetControllerCode(int semantics);
void IRTVSetHoldDelay(int delay);


#endif /* IRTV_H_ */
