/**
 * \file audio.h
 * \brief Header file for audio functionality
 * \author Rhys Kessey
*/

#ifndef AUDIO_H_
#define AUDIO_H_

#include <inttypes.h>

#include "types.h"

void speaker_init(void);

#endif /* AUDIO_H_ */
