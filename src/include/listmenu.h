/**
 * \file    listmenu.h
 * \brief   creates lists
 * \author  Marcus Pham (Remi Keat)
 */

#ifndef LISTMENU_H_
#define LISTMENU_H_

#include "types.h"

void LMInit(LIST_MENU *,char *);
void LMAddItem(LIST_MENU *,char *,void *);
void LMFreeItems(LIST_MENU *);
int LMSize(LIST_MENU);
void LMToggleScroll(LIST_MENU *);
int LMGetScroll(LIST_MENU);
void LMChangeItemText(LIST_MENU *listmenu, char *newText,int index);
int LMGetIndex(LIST_MENU listmenu);
char *LMGetItemText(LIST_MENU listmenu);

#endif
