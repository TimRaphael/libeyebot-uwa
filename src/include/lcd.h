
/**
 * \file    lcd.h
 * \brief   Header file for the LCD functions
 * \author  Marcus Pham & Remi KEAT 
 * Using some functions by George Peter Staplin 2003 GeorgePS@XMission.com
 */

#ifndef LCD_H_
#define LCD_H_

#define _GNU_SOURCE

#include <X11/keysym.h>
#include <jerror.h>

#include <X11/Xutil.h>
#include <X11/Xresource.h>
#include <X11/keysym.h>

#include "types.h"
#include <stdarg.h>
#include "globals.h"

#define LCD_MAX_LIST_ITEM MAX(1,(gLCDHandle->height/(gLCDHandle->fontHeight*2))-2)

//defining the colours
#define LCD_WHITE LCDGetColorFromName("white")
#define LCD_SILVER LCDGetColorFromName("light gray")
#define LCD_LIGHTGRAY LCDGetColorFromName("light gray")
#define LCD_LIGHTGREY LCDGetColorFromName("light grey")
#define LCD_GRAY LCDGetColorFromName("gray")
#define LCD_DARKGRAY LCDGetColorFromName("dark gray")
#define LCD_DARKGREY LCDGetColorFromName("dark grey")
#define LCD_BLACK LCDGetColorFromName("black")
#define LCD_BLUE LCDGetColorFromName("blue")
#define LCD_NAVY LCDGetColorFromName("navy")
#define LCD_AQUA LCDGetColorFromName("aquamarine")
#define LCD_CYAN LCDGetColorFromName("cyan")
#define LCD_TEAL LCDGetColorFromName("dark cyan")
#define LCD_FUCHSIA LCDGetColorFromName("magenta")
#define LCD_MAGENTA LCDGetColorFromName("magenta")
#define LCD_PURPLE LCDGetColorFromName("purple")
#define LCD_RED LCDGetColorFromName("red")
#define LCD_MAROON LCDGetColorFromName("maroon")
#define LCD_YELLOW LCDGetColorFromName("yellow")
#define LCD_OLIVE LCDGetColorFromName("dark olive green")
#define LCD_LIME LCDGetColorFromName("lime green")
#define LCD_GREEN LCDGetColorFromName("green")

/* constants for text colorflags */
#define LCD_BGCOL_TRANSPARENT         0x01
#define LCD_BGCOL_NOTRANSPARENT       0x10

#define LCD_BGCOL_INVERSE             0x02
#define LCD_BGCOL_NOINVERSE           0x20

#define LCD_FGCOL_INVERSE             0x04
#define LCD_FGCOL_NOINVERSE           0x40

/* constants for lcd modes */
#define LCD_AUTOREFRESH               0x0001
#define LCD_NOAUTOREFRESH             0x0100

#define LCD_SCROLLING                 0x0002
#define LCD_NOSCROLLING               0x0200

#define LCD_LINEFEED                  0x0004
#define LCD_NOLINEFEED                0x0400

#define LCD_SHOWMENU                  0x0008
#define LCD_HIDEMENU                  0x0800

#define LCD_LISTMENU				  0x0010
#define LCD_CLASSICMENU				  0x1000

#define LCD_FB_ROTATE                 0x0080
#define LCD_FB_NOROTATION             0x8000

/*
 * define fonts using their alias
 * font aliases can be found in the following file:
 * 	/etc/X11/fonts/misc/xfonts-base.alias
 */
char FONTDEFAULT[200];
char FONTBOLD[200];
char MENUFONT[200];

XColor LCDGetColorFromName(char* colorName);
XColor LCDRGBtoXCol(COLOR rgb);

int LCDInit();
int LCDRelease();

int LCDNeedRefresh(void);
int LCDRefresh2(void);
int LCDRefresh3(void);

int LCDGetFBInfo(FBINFO* pinfo);

int LCDGetMode(void);
int LCDResetMode(int mode);

int LCDPrintfFont(EYEFONT eyeFont,const char *format, ...);
int LCDSetPrintfFont(int row, int column, EYEFONT eyeFont, const char *format, ...);

int LCDDrawMenu(void);
int LCDMenuIRaw(int pos, char *string, XColor fgcol, XColor bgcol, void* userp);
MENU_ITEM* LCDMenuItem(int index);

int LCDDrawList(void);
int LCDList(LIST_MENU *menulist);
int LCDSetList(LIST_MENU* menulist);
LIST_MENU* LCDGetList(void);
RECT* LCDListBox(int pos);
MENU_ITEM* LCDListActiveItem(void);

int LCDListCount(void);
int LCDListIndex(int index);
int LCDListScrollUp(void);
int LCDListScrollDown(void);

int LCDFrame(int x1, int y1, int x2, int y2, XColor color);
RECT LCDTextBar(int row, int column, int length, int fill, XColor color);

int LCDPutImageRGB(int xpos, int ypos, int xsize, int ysize, BYTE* data);

//RAW functions
int LCDPixelRaw(int x, int y, XColor color);
XColor LCDInvertPixelRaw(XColor color);
XColor LCDGetPixelRaw(int x, int y);

int LCDAreaRaw(int x1, int y1, int x2, int y2, XColor color);
int LCDLineRaw(int x1, int y1, int x2, int y2, XColor color);

int LCDSetColorRaw(XColor fgcol, XColor bgcol, char colorflags);
int LCDSetFontRaw(char *fontAlias);

int LCDGetDimensions(int* pix_x, int* pix_y, int* dim_x, int* dim_y);

#endif /* LCD_H_ */
