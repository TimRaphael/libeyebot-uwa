/**
 * \file    servos_and_motors.h
 * \brief   Header file for the servos and motors functions
 * \author  Marcus Pham
 */

#ifndef SERVOS_AND_MOTORS_H_
#define SERVOS_AND_MOTORS_H_

#include "serial.h"
#include "hdt.h"
#include "types.h"

#define SERVO_LOW 0
#define SERVO_HIGH 255

int ENCODERBaseVal[4];
HDT_MOTOR *MOTORFirst;
HDT_SERVO *SERVOFirst;
int MOTORInitialised;
int SERVOInitialised;

int MOTORInit();
int SERVOInit();

#endif /* SERVOS_AND_MOTORS_H_ */
