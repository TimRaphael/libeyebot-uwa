/**
 * \file		vomega.h
 * \brief		Header file for the VW functions
 * \author      Marcus Pham
 */

#ifndef VOMEGA_H_
#define VOMEGA_H_

#include "serial.h"
#include "types.h"

int VWInitialised;

int VWInit();

#endif /* VOMEGA_H_ */
