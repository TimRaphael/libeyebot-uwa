/**
 * \file		vomega.c
 * \brief		Defines the VW functions
 * \author      Marcus Pham
 */

#include "vomega.h"
#include "eyebot.h"
#include <stdlib.h>

/**
 * \brief		Initialises the VW with settings defined in hdt file
 * \return      0 on success, 1 othewrise
 */
int VWInit() {

    unsigned int ticks_l, ticks_r, distance, type, maxSpeed, encoder_nr;

    FILE *fp;
    fp = fopen(HDT_FILE,"r");
    if (fp == NULL)
    {
        fprintf(stderr, "No HDT file present");
        return 1;
    }
    else
    {
        char* line = malloc(300*sizeof(char));
        char* buffer = malloc(100*sizeof(char));
        ssize_t read;
        size_t len = 0;

        while((read = getline(&line, &len, fp)) != -1) {
            if(strstr(line,"ENCODER")==line) {
                buffer = strtok(line," \n");
                buffer = strtok(NULL," \n");
				encoder_nr = atoi(buffer);
                buffer = strtok(NULL," \n");
				// Encoder number one is on the left side
				if(encoder_nr == 1)
					ticks_l = atoi(buffer);
				// Encoder number two is on the right side
				if(encoder_nr == 2)
					ticks_r = atoi(buffer);
            }
            if(strstr(line, "DRIVE")==line) {
                int M1,M2;
                buffer = strtok(line," \n");
                buffer = strtok(NULL," \n");
                distance = atoi(buffer);

                buffer = strtok(NULL," \n");
                maxSpeed = atoi(buffer);

                buffer = strtok(NULL," \n");
                M1 = atoi(buffer);

                buffer = strtok(NULL," \n");
                M2 = atoi(buffer);

                if((!M1)&&(!M2)) {
                    type = 4;
                }
                else if((!M1)&&(M2)) {
                    type = 1;
                }
                else if((M1)&&(!M2)) {
                    type = 2;
                }
                else {
                    type = 3;
                }
            }
        }
    }

    if (fp != NULL)
    {
        fclose(fp);
    }

    if(VWInitialised == 1) {
        return 0;
    }

    //starting serial send
    int temp;
    temp = SERInit(IOBOARD, DEFAULT_BAUD, 0);
    if (temp != 0) {
        return 1;
    }
    
    //seperating bytes;
    char cmd[8];
    cmd[0]='w'+128;
    cmd[1]=ticks_l/256;
    cmd[2]=ticks_l%256;
    cmd[3]=distance/256;
    cmd[4]=distance%256;
    cmd[5]=maxSpeed/256;
    cmd[6]=maxSpeed%256;
    cmd[7]=type;

    if(SERCheck(IOBOARD)!=0) {
        fprintf(stderr, "VW: Timeout!!!\n");
        VWInitialised = 0;
        return 1;
    }

    for(int i =0; i<8; i++) {
        SERSendChar(IOBOARD, cmd[i]);
    }
    
    VWInitialised = 1;

    return 0;
}

/**
 * \brief		Sets the VW speed, angle
 * \param       linSpeed the straightline speed (mm/s)
 * \param       angSpeed the angular turning speed (100 * rad/s)
 * \return      0 on success, 1 othewrise
 */
int VWSetSpeed(int linSpeed, int angSpeed) {
    if(VWInitialised == 0) {
        if(VWInit()==1) {
            return 1;
        }
    }

    char cmd[5];
    cmd[0]='x'+128;

    cmd[1]=(char)(linSpeed>>8);
    cmd[2]=(char)linSpeed;
    cmd[3]=(char)(angSpeed>>8);
    cmd[4]=(char)angSpeed;
    if(SERCheck(IOBOARD)!=0) {
        fprintf(stderr, "VW: Timeout!!!\n");
        VWInitialised = 0;
        return 1;
    }

    for(int i =0; i<5; i++) {
        SERSendChar(IOBOARD, cmd[i]);
    }

    return 0;
}

/**
 * \brief		Obtains the current speeds
 * \param       linspeed the integer to store the linear speed (mm/s)
 * \param       angSpeed the integer to store the angular speed  (100 * rad/s)
 * \return      0 on success, 1 othewrise
 */
int VWGetSpeed(int *linSpeed, int *angSpeed) {
    if(VWInitialised == 0) {
        if(VWInit()==1) {
            return 1;
        }
    }
    char cmd[1];
    cmd[0]='X'+128;

    if(SERCheck(IOBOARD)!=0) {
        fprintf(stderr, "VW: Timeout!!!\n");
        VWInitialised = 0;
        return 1;
    }

    for(int i =0; i<1; i++) {
        SERSendChar(IOBOARD, cmd[i]);
    }

    char buf[4];
    while(SERReceiveChar(IOBOARD)!='>') {
        continue;
    }


    for (int i =0; i < 4; i++) {
        buf[i] = SERReceiveChar(IOBOARD);
    }

    *linSpeed=(buf[0]<<24 | buf[1]<<16 )/(1<<16) ;   //to keep sign
    *angSpeed=(buf[2]<<24 | buf[3]<<16 )/(1<<16) ;   //to keep sign

    return 0;
}

/**
 * \brief		Sets the position wrt current position
 * \param       x the horizontal component (mm)
 * \param       y the vertical component (mm)
 * \param       phi the turn angle (rad*100)
 * \return      0 on success, 1 othewrise
 */
int VWSetPosition(int x, int y, int phi) {
    if(VWInitialised == 0) {
        if(VWInit()==1) {
            return 1;
        }
    }

    char cmd[7];
    cmd[0]='Q'+128;
    cmd[1]=(char)(x>>8);
    cmd[2]=(char)x;
    cmd[3]=(char)(y>>8);
    cmd[4]=(char)y;
    cmd[5]=(char)(phi>>8);
    cmd[6]=(char)phi;

    if(SERCheck(IOBOARD)!=0) {
        fprintf(stderr, "VW: Timeout!!!\n");
        VWInitialised = 0;
        return 1;
    }

    for(int i =0; i<7; i++) {
        SERSendChar(IOBOARD, cmd[i]);
        }

    return 0;
}

/**
 * \brief		Gets the position wrt original position
 * \param       x stores the horizontal component (mm)
 * \param       y stores the vertical component (mm)
 * \param       phi stores the turn angle (rad*100)
 * \return      0 on success, 1 othewrise
 */
int VWGetPosition(int *x, int *y, int *phi) {
    if(VWInitialised == 0) {
        if(VWInit()==1) {
            return 1;
        }
    }

    char cmd[1];
    cmd[0]='q'+128;

    if(SERCheck(IOBOARD)!=0) {
        fprintf(stderr, "VW: Timeout!!!\n");
        VWInitialised = 0;
        return 1;
    }

    for(int i =0; i<1; i++) {
        SERSendChar(IOBOARD, cmd[i]);
    }

    char buf[6];

    while(SERReceiveChar(IOBOARD)!='>') {
        continue;
    }


    for (int i =0; i<6; i++) {
        buf[i] = SERReceiveChar(IOBOARD);
    }
    
    *x=(buf[0]<<24 | buf[1]<<16)/(1<<16) ;   //to keep sign
    *y=(buf[2]<<24 | buf[3]<<16)/(1<<16) ;   //to keep sign
    *phi=(buf[4]<<24 | buf[5]<<16)/(1<<16) ;   //to keep sign

    return 0;
}

/**
 * \brief		Starts VW control
 * \param       Vv
 * \param       Tv
 * \param       Vw
 * \param       Vw
 * \return      0 on success, 1 othewrise
 */
int VWControl(int Vv, int Tv, int Vw, int Tw) {
    if(VWInitialised == 0) {
        if(VWInit()==1) {
            return 1;
        }
    }

    char cmd[5];
    cmd[0]='A'+128;
    cmd[1]=Vv;
    cmd[2]=Tv;
    cmd[3]=Vw;
    cmd[4]=Tw;

    if(SERCheck(IOBOARD)!=0) {
        fprintf(stderr, "VW: Timeout!!!\n");
        VWInitialised = 0;
        return 1;
    }

    for(int i =0; i<5; i++) {
        SERSendChar(IOBOARD, cmd[i]);
    }

    return 0;
}

/**
 * \brief		Stopss VW control
 * \return      0 on success, 1 othewrise
 */
int VWControlOff(void) {
    if(VWInitialised == 0) {
        if(VWInit()==1) {
            return 1;
        }
    }

    char cmd[1];
    cmd[0]='W'+128;

    if(SERCheck(IOBOARD)!=0) {
        fprintf(stderr, "VW: Timeout!!!\n");
        VWInitialised = 0;
        return 1;
    }

    for(int i =0; i<1; i++) {
        SERSendChar(IOBOARD, cmd[i]);
    }

    return 0;
}

/**
 * \brief		Drives straight
 * \param       dist the distance to travel
 * \param       lin_speed the speed to travel at
 * \return      0 on success, 1 othewrise
 */
int VWStraight(int dist, int lin_speed) {
    if(VWInitialised == 0) {
        if(VWInit()==1) {
            return 1;
        }
    }

    char cmd[5];
    cmd[0]='y'+128;
    cmd[1]=(char)(lin_speed>>8);
    cmd[2]=(char)lin_speed;
    cmd[3]=(char)(dist>>8);
    cmd[4]=(char)dist;

    if(SERCheck(IOBOARD)!=0) {
        fprintf(stderr, "VW: Timeout!!!\n");
        VWInitialised = 0;
        return 1;
    }

    for(int i =0; i<5; i++) {
        SERSendChar(IOBOARD, cmd[i]);
    }

    return 0;
}

/**
 * \brief		Turns the vehicle on the spot
 * \param       angle the angle to turn to in Radians*100 (eg. 314 = Rotation Half Circle)
 * \param       ang_speed the speed to turn the angle
 * \return      0 on success, 1 othewrise
 */
int VWTurn(int angle, int ang_speed) {
    if(VWInitialised == 0) {
        if(VWInit()==1) {
            return 1;
        }
    }

    angle = angle/10;

    char cmd[5];
    cmd[0]='Y'+128; //should be upper y
    cmd[1]=(char)(ang_speed>>8);
    cmd[2]=(char)ang_speed;
    cmd[3]=(char)(angle>>8);
    cmd[4]=(char)angle;

    if(SERCheck(IOBOARD)!=0) {
        fprintf(stderr, "VW: Timeout!!!\n");
        VWInitialised = 0;
        return 1;
    }

    for(int i =0; i<5; i++) {
        SERSendChar(IOBOARD, cmd[i]);
    }

    return 0;
}

/**
 * \brief		Turns the vehicle in a curve
 * \param       dist the radial distance (in mm)
 * \param       angle the angle to rotate in Radians * 100 (eg. 314 = half circle)
 * \param       lin_speed the speed to rotate
 * \return      0 on success, 1 othewrise
 */
int VWCurve(int dist, int angle, int lin_speed) {
    if(VWInitialised == 0) {
        if(VWInit()==1) {
            return 1;
        }
    }

    char cmd[7];
    cmd[0]='C'+128;
    cmd[1]=(char)(dist>>8);
    cmd[2]=(char)dist;
    cmd[3]=(char)(angle>>8);
    cmd[4]=(char)angle;
    cmd[5]=(char)(lin_speed>>8);
    cmd[6]=(char)lin_speed;
    if(SERCheck(IOBOARD)!=0) {
        fprintf(stderr, "VW: Timeout!!!\n");
        VWInitialised = 0;
        return 1;
    }

    for(int i =0; i<7; i++) {
        SERSendChar(IOBOARD, cmd[i]);
    }

    return 0;
}

/**
 * \brief		Turns the vehicle in a curve
 * \param       dx the distance in front of the vehicle to drive (in mm)
 * \param       dy the distance perpendicular to the direction of travel (in mm)
 * \param       lin_speed the speed to rotate
 * \return      0 on success, 1 othewrise
 */
int VWDrive(int dx, int dy, int lin_speed) {
    
    double r;
    double theta;
    double phi;
    double hyp;
    double dist;
    
    hyp = sqrt(dx*dx+dy*dy);
    phi = atan2(dx,dy);
    r = hyp/(sin(M_PI-2*phi))*sin(phi);
    
    theta = M_PI-2*phi;
    dist = r*theta;
    
    VWCurve((int)dist, (int)(theta*100), lin_speed);
    
    return 0;
}

/**
 * \brief		Checks the remaining drive time
 * \return      the remaining time
 */
int VWDriveRemain() {
    if(VWInitialised == 0) {
        if(VWInit()==1) {
            return -1;
        }
    }

    char cmd[1];
    cmd[0]='z'+128;

    if(SERCheck(IOBOARD)!=0) {
        fprintf(stderr, "VW: Timeout!!!\n");
        VWInitialised = 0;
        return 1;
    }

    for(int i =0; i<1; i++) {
        SERSendChar(IOBOARD, cmd[i]);
    }

    char buf[2];

    while(SERReceiveChar(IOBOARD)!='>') {
        continue;
    }

    if(SERReceive(IOBOARD, buf)!=2) {
        fprintf(stderr, "VW: Incorrect No. Received Bytes!!!\n");
        VWInitialised = 0;
        return -1;
    }

    unsigned int ret;
    ret = 0|0|(buf[0] << 8)|buf[1];
    return ret;
}

/**
 * \brief		Checks to see if the current drive is complete
 * \return      1 on completion, 0 for still driving, -1 on failure
 */
int VWDriveDone() {
    if(VWInitialised == 0) {
        if(VWInit()==1) {
            return -1;
        }
    }

    char cmd[1];
    cmd[0]='Z'+128;

    if(SERCheck(IOBOARD)!=0) {
        fprintf(stderr, "VW: Timeout!!!\n");
        VWInitialised = 0;
        return 1;
    }

    for(int i =0; i<1; i++) {
        SERSendChar(IOBOARD, cmd[i]);
    }

    char buf;

    while(SERReceiveChar(IOBOARD)!='>') {
        continue;
    }

    buf = SERReceiveChar(IOBOARD);

    unsigned int ret;
    ret = 0|0|0|buf;

    return ret;
    /*
    if(ret == 0) {
        return 0;
    }
    else if(ret==1){
        return 1;
    }
    else {
        return -1;
    }*/
}

/**
 * \brief		Blocks until the drive is complete
 * \return      0 on success, 1 othewrise
 */
int VWDriveWait() {
    if(VWInitialised == 0) {
        if(VWInit()==1) {
            return -1;
        }
    }

    char cmd[1];
    cmd[0]='Z'+128;
    unsigned int ret = 0;
    char buf;

    while(ret!=1) {
        if(SERCheck(IOBOARD)!=0) {
            fprintf(stderr, "VW: Timeout!!!\n");
            VWInitialised = 0;
            return 1;
        }

        for(int i =0; i<1; i++) {
            SERSendChar(IOBOARD, cmd[i]);
        }

        while(SERReceiveChar(IOBOARD)!='>') {
            continue;
        }

        buf = SERReceiveChar(IOBOARD);
        ret = 0|0|0|buf;
    }
    return 0;
}

/**
 * \brief		Checks to see if the vehicle is stalled
 * \return      0 on stalled, 1 othewrise
 */
int VWStalled() {
    if(VWInitialised == 0) {
        if(VWInit()==1) {
            return -1;
        }
    }

    char cmd[1];
    cmd[0]='Z'+128;

    if(SERCheck(IOBOARD)!=0) {
        fprintf(stderr, "VW: Timeout!!!\n");
        VWInitialised = 0;
        return 1;
    }

    for(int i =0; i<1; i++) {
        SERSendChar(IOBOARD, cmd[i]);
    }

    char buf;

    while(SERReceiveChar(IOBOARD)!='>') {
        continue;
    }

    buf = SERReceiveChar(IOBOARD);

    unsigned int ret;
    ret = 0|0|0|buf;

    if(ret == 2) {
        return 0;
    }

    else {
        return 1;
    }

    return 0;
}
