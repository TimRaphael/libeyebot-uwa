/**
 * \file    audio.c
 * \brief   Defines functions for audio through the inbuilt speaker
 * \author  Rhys Kessey
 */

#include "audio.h"
#include "eyebot.h"
#include "timer.h"
#include "inout.h"

int _playingTone = 0;
int _playing = 0;
int _input_freq = 0;
int _input_time = 0;
int _toggle = 0;
TIMER _tone_timer = 0;
int _max_toggles = 0;
volatile int _num_toggles = 0;

void speaker_init(void) {
	wiringPiSetup();
	pinMode(2, OUTPUT);
	return;
}
void tone_toggle(void) {
	if (_toggle) {
		digitalWrite(2, LOW);
		_toggle = 0;
		_num_toggles += 1;
	}
	else {
		digitalWrite(2, HIGH);
		_toggle = 1;
		_num_toggles += 1;
	}
	if (_num_toggles >= _max_toggles) {
		OSDetachTimer(_tone_timer);
		_num_toggles = 0;
		_playingTone = 0;
	}
	return;
}

// Play tone with FREQ for time/100 sec
int AUTone(int freq, int time){
	int scale = 5000 / freq;
	TIMER t1 = OSAttachTimer(scale, tone_toggle);
	_playingTone = 1;
	_tone_timer = t1;
	_max_toggles = time*freq/500;
	return 0;
}

// Play beep sound
int AUBeep(void){
    return AUTone(200,500);
}

// Non-blocking test if tone has finished
int AUCheckTone(void){
	return _playingTone;
}

// Play audio sample
int AUPlay(BYTE* sample){
	return 0;
}

// Non-blocking test if playing has finished
int AUCheckPlay(void){
	return _playing;
}
/*
	Mic functions use Analog output 7 (A7 command) to retrieve data
	Ask Marcus how to send and retrieve serial requests
	ANALOGRead(7);
*/
int AUMicrophone(void){
	return ANALOGRead(7);
}

// Record sound for time/100sec using sample FREQ
int AURecord(BYTE* buf, int time, long freq){
	return 0;
}

// Non-blocking check whether recording has finished
int AUCheckRecord(void){
	return 0;
}

