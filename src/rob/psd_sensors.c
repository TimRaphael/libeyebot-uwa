/**
 * \file    psd_sensors.c
 * \brief   Defines functions to use the PSD sensors
 * \author  Marcus Pham
 */

#include "psd_sensors.h"
#include "hdt.h"
#include "eyebot.h"

/**
 * \brief   Initialises the IO board and sets up tables from hdt.txt
 * \return  0 on success, 1 otherwise
 */
int PSDInit() {
    if(PSDInitialised == 1) {
        return 0;
    }
    
    int temp;
    temp = SERInit(IOBOARD, DEFAULT_BAUD, 0);
    if (temp != 0) {
        return 1;
    }
    
    //loading servos
    PSDFirst = HDTLoadPSD(HDT_FILE, NULL);
    
    //loading tables
    PSDFirst->ptable = HDTLoadTable(HDT_FILE, (HDT_DEVICE*)PSDFirst);

    PSDInitialised = 1;
    return 0;
}

/**
 * \brief   Delivers raw-data measured by the selected PSD
 * \param   psd the psd to read
 * \return  the actual raw-data (not converted)
 */
int PSDGetRaw(int psd) {
    if(PSDInitialised == 0) {
        if(PSDInit()==1) {
            return -1;
        }
    }
    
    char cmd[2];
    cmd[0]='p'+128;
    cmd[1]=psd;
    
    if(SERCheck(IOBOARD)!=0) {
        fprintf(stderr, "PSD: Timeout!!!\n");
        PSDInitialised = 0;
        return -1;
    }
    
    for(int i =0; i<2; i++) {
        SERSendChar(IOBOARD, cmd[i]);
    }

    while(SERReceiveChar(IOBOARD)!='>') {
        continue;
    }
    
    char buf[2];
    
    buf[0]=SERReceiveChar(IOBOARD);
    buf[1]=SERReceiveChar(IOBOARD);

    unsigned int ret;
    ret = 0|0|(buf[0] << 8)|buf[1];
    return ret;
}

/**
 * \brief   Delivers actual timestamp or distance measured by the selected PSD.
 * \param   psd the number of the psd to read
 * \return  the actual distance in mm (converted through internal table)
 */
int PSDGet(int psd) {
    if(PSDInitialised == 0) {
        if(PSDInit()==1) {
            return -1;
        }
    }
    
    int rawValue = PSDGetRaw(psd);
    
    HDT_PSD* pPSD = PSDFirst;
    
    //loop through all the motors until find correct pmotor->regaddr, select that motor
    for(int i = 1; i<psd; i++) {
        pPSD = pPSD->pnext;
    }
    
    //check for null/no table entry, if null then send raw value, otherwise
    if((pPSD->tabname == NULL)||(pPSD->ptable->size==0)) {
        fprintf(stderr, "PSD: No table, returning raw value\n");
        return rawValue;
    }
    
    int realValue = rawValue;
    realValue =(pPSD->ptable->size*rawValue)/4096;

    return realValue;
}

