/**
 * \file		key.c
 * \author      Marcus Pham & Remi KEAT
 * \brief		Defines functions for the key input
 */

#include "key.h"
#include "globals.h"
#include "lcd.h"
#include "eyebot.h"

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

static int key_esc = KEY_INVALID;

/**
 * \brief   Open the evdev device file for reading touch events.
 * Load the key configuration file (if found), else use the hardcoded default value.
 * \return  int retVal : 0 on success\n
 * Negative value on failure
 */
int KEYInit(void)
{
    if(KEY_INITIAL == 1) {
        return 1;
    }
    
	gTouchMap = malloc(sizeof(TOUCH_MAP));
	KEYSetTM(KEYTM_CLASSIC);
    KEY_INITIAL = 1;
	return 0;
}

/**
 * \brief   Set mode for key touch map.
 * \param   KEYMODE mode : Requested touch map mode
 * \return  KEYMODE retMod : Current touch map mode
 */
int KEYSetTM(int mode)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
	int i;
	int buttonWidth = gLCDHandle->width/4;
	gTouchMap->mode = mode;
	int menuHeight = gLCDHandle->fontHeight*2;

	int offset = 0;
	if (gLCDHandle->listMenu)
	{
		if (gLCDHandle->listMenu->scroll != 0)
		{
			offset = 1;
		}
	}
    
	for (i = 0; i < KEYTM_MAX_REGIONS; i++)
	{
		gTouchMap->rect[i].tl.x = 0;
		gTouchMap->rect[i].tl.y = 0;
		gTouchMap->rect[i].br.x = 0;
		gTouchMap->rect[i].br.x = 0;
	}
	switch (mode)
	{
            //setting the touch for the list
		case KEYTM_LISTMENU:
			for (i = 4; i < LCD_MAX_LIST_ITEM+3-offset; i++)
			{
				gTouchMap->rect[i].tl.x = 0;
				gTouchMap->rect[i].tl.y = (i+offset-4+1)*menuHeight;
				gTouchMap->rect[i].br.x = gLCDHandle->width;
				gTouchMap->rect[i].br.y = (i+offset-4+2)*menuHeight;
			}
			break;
		default:
			break;
	}
    
    //setting touch area for the bottom menu
	for (i = 0; i < 4; i++)
	{
		gTouchMap->rect[i].tl.x = i*buttonWidth;
		//gTouchMap->rect[i].tl.y = gLCDHandle->height-MENU_HEIGHT;
        gTouchMap->rect[i].tl.y = gLCDHandle->height-menuHeight;
		gTouchMap->rect[i].br.x = (i+1)*buttonWidth;
		gTouchMap->rect[i].br.y = gLCDHandle->height;
	}
    
    //setting touch area for the scrolling buttons
	if (offset != 0)
	{
        //scroll up
		gTouchMap->rect[KEYTM_MAX_REGIONS-2].tl.x = 0;
		gTouchMap->rect[KEYTM_MAX_REGIONS-2].tl.y = 1*menuHeight;
		gTouchMap->rect[KEYTM_MAX_REGIONS-2].br.x = gLCDHandle->width;
		gTouchMap->rect[KEYTM_MAX_REGIONS-2].br.y = 2*menuHeight;
        
		gTouchMap->rect[KEYTM_MAX_REGIONS-1].tl.x = 0;
		gTouchMap->rect[KEYTM_MAX_REGIONS-1].tl.y = (LCD_MAX_LIST_ITEM)*menuHeight;
		gTouchMap->rect[KEYTM_MAX_REGIONS-1].br.x = gLCDHandle->width;
		gTouchMap->rect[KEYTM_MAX_REGIONS-1].br.y = (LCD_MAX_LIST_ITEM+1)*menuHeight;
	}
	return mode;
}

/**
 * \brief   Get current mode and touch map (region map).
 * \param   TOUCH_MAP** ptouch_map : Pointer to a TOUCH_MAP structure
 * \return  KEYMODE retMod : Current touch map mode
 */
int KEYGetTM(TOUCH_MAP** ptouch_map)
{
    if(KEY_INITIAL != 1) {
        KEYInit();
    }
    
	if (ptouch_map != 0x0)
	{
		*ptouch_map = gTouchMap;
	}
	return gTouchMap->mode;
}

/**
 * \brief   Manually set region data into current touch map.
 * Only used in KEYTM_REGIONS mode. If region is 0x0, resets the touch map
 * (mode becomes KEYTM_UNKNOWN).
 * \param   int index : Index for region
 * \param   BOX* region : Pointer to a region data
 * \return  int retVal : 0 on success\n
 * Negative value on failure
 */
int KEYSetRegion(int index, BOX *region)
{
    if(KEY_INITIAL != 1) {
        KEYInit();
    }
    
	gTouchMap->rect[index] = *region;
	return 0;
}

/**
 * \brief   Copy specific region data out from the current touch map.
 * Only used in KEYTM_REGIONS mode.
 * \param   int index : Index for region
 * \param   BOX* region : Pointer to a storage for region data
 * \return  int retVal : 0 on success\n
 * Negative value on failure
 */
int KEYGetRegion(int index, BOX *region)
{
    if(KEY_INITIAL != 1) {
        KEYInit();
    }
    
	*region = gTouchMap->rect[index];
	return 0;
}

/**
 * \brief   Validate there's no touch on screen surface
 * \param   TOUCH_EVENT* rawtouch : pointer to TOUCH_EVENT structure this is optional!
 * only if raw data needed! else, use 0x0!
 * \return  int retVal :\n
 * 0 - being touched\n
 * 1 - not touched
 */
int KEYNoTouch(TOUCH_EVENT* rawtouch)
{
    if(KEY_INITIAL != 1) {
        KEYInit();
    }
    
	if (gMouseButton == 1)
	{
		return 0;
	}
	return 1;
}

/**
 * \brief   Gets raw touch info - a non-blocking function.
 * Mainly used for calibration and testing.
 * \param   TOUCH_EVENT* rawtouch : pointer to TOUCH_EVENT structure
 * \return  int retVal :\n
 * 0 if sync signal received!\n
 * Negative value if otherwise
 */
int KEYGetRaw(TOUCH_EVENT* rawtouch)
{
    if(KEY_INITIAL != 1) {
        KEYInit();
    }
    
	rawtouch->point.x = gMousePosX;
	rawtouch->point.y = gMousePosY;
	rawtouch->status = gMouseButton;
	return 0;
}

/**
 * \brief   Decode raw touch info into a keycode based on the current touch map.
 * Mainly used for testing.
 * \param   TOUCH_EVENT* rawtouch : pointer to TOUCH_EVENT structure
 * \return  KEYCODE keyCode : Status of touch data (variable in rawtouch)
 */
int KEYDecode(TOUCH_EVENT* rawtouch)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
        LCD_INITIAL = 1;
    }
    
    if(KEY_INITIAL != 1) {
        KEYInit();
    }
    
    //the area number (key number 0-3 the menu, 4+ = list)
	int k;
	for (k = 0; k < KEYTM_MAX_REGIONS; k++)
	{
		if (KEYInside(rawtouch->point.x, rawtouch->point.y, gTouchMap->rect[k]))
		{
			if (gLCDHandle->listMenu)
			{
                //if list too long -> scrolling
				if (gLCDHandle->listMenu->scroll != 0)
				{
					if (k == KEYTM_MAX_REGIONS-2)
					{
						return KEY_LISTUP;
					}
					else if (k == KEYTM_MAX_REGIONS-1)
					{
						return KEY_LISTDN;
					}
				}
			}
			return 0x01<<k;
		}
	}
	return 0;
}

/**
 * \brief   Close the evdev device file and stop checking any key touch event
 * \return  int retVal : 0 on success\n
 * Negative value on failure
 */
int KEYRelease(void)
{
    if(KEY_INITIAL != 1) {
        KEYInit();
    }
    
	free(gTouchMap);
	gTouchMap = NULL;
	return 0;
}

/**
 * \brief		Check if a point is inside a rectangle
 * \param		int x : X-coordinate of the point
 * \param   int y : Y-coordinate of the point
 * \param   BOX rect : rectangle structure
 * \return  int retVal : non-null if inside
 */

int KEYInside(int x, int y, BOX rect)
{
	if (x >= rect.tl.x && x <= rect.br.x)
	{
		if (y >= rect.tl.y && y <= rect.br.y)
		{
			return 1;
		}
	}
	return 0;
}

/**
 * \brief		Enable/disable event checking procedure
 * \param		int idle : user request
 * \details Valid values for idle:
 * - KEY_GOIDLE - deactivate event checking
 * - KEY_NOIDLE - activate event checking
 * - KEY_STATE - request current status
 *
 * \return  int status : Idle status of event checking procedure
 */
int KEYIdle(int idle)
{
    if(KEY_INITIAL != 1) {
        KEYInit();
    }
	return 0;
}

/**
 * \brief		Wait for specific keys only.
 * \param		KEYCODE excode : Expected keycode values (bit XORed)
 * \return  (int) KEYCODE retKey : Keycode value
 */
int KEYWait(int excode)
{
    if(KEY_INITIAL != 1) {
        KEYInit();
    }
    
	int readKey;
	
	do
	{
        //LCDNeedRefresh();
		readKey = KEYGet();
	}
    while (excode != (int) ANYKEY && (readKey & excode) == 0);

	gMouseButton = 0;
	return readKey;
}

/**
 * \brief		Wait for a touch event and return keycode (including KEY_INVALID - undefined keycode).
 * \return  (int) KEYCODE retKey : Keycode value
 */
int KEYGet(void)
{
    if(KEY_INITIAL != 1) {
        KEYInit();
    }
    
    int readKey = KEYRead();
    while(readKey == NOKEY) {
        //LCDNeedRefresh();
        readKey = KEYRead();
    }
    gMouseButton = 0;
	return (int) readKey;
}

/**
 * \brief		Wait for a touch event and return the XY-coordinate.
 * \return  COORD_PAIR retCoord : Coordinate pair
 */
COORD_PAIR KEYXYRaw()
{
    if(KEY_INITIAL != 1) {
        KEYInit();
    }
    
	COORD_PAIR point;
	while (gMouseButton == 0)
	{
		LCDNeedRefresh();
	}
	point.x = gMousePosX;
	point.y = gMousePosY;
	return point;
}

/**
 * \brief	Waits for a key press and returns the coordinates of the pressed position
 * \return  0 always
 */
int KEYGetXY(int* x, int* y) {
    
    if(KEY_INITIAL != 1) {
        KEYInit();
    }
    
    COORD_PAIR temp;
    gMouseButton = 0;
    temp = KEYXYRaw();
    *x = temp.x;
    *y = temp.y;
    return 0;
}

/**
 * \brief	Returns the coordinates of the pressed position
 * \return  0 on a keypress, 1 otherwise
 */
int KEYReadXY(int* x, int* y) {
    
    if(KEY_INITIAL != 1) {
        KEYInit();
    }
    
    COORD_PAIR temp;
    
    int k;
    
    LCDRefresh3();
    TOUCH_EVENT key;
    KEYGetRaw(&key);
    
    if (gMouseButton == 1)
    {
        gMouseButton = -1;
        *x = temp.x;
        *y = temp.y;
        return 0;
    }
    
    else if (gMouseButton == -1)
    {
        *x = temp.x;
        *y = temp.y;
        return 0;
    }
    
    return 1;
}

/**
 * \brief		Read a keycode and returns. Function does not wait, thus includes KEY_TIMEOUT.
 * \return  (int) KEYCODE retKey : Keycode value
 */
int KEYRead(void)
{
    if(KEY_INITIAL != 1) {
        KEYInit();
    }
    
    //checking for key reads off the LCD
	int k;
    
    LCDRefresh3();
    TOUCH_EVENT key;
    KEYGetRaw(&key);
    
	if (gMouseButton == 1)
	{
		gMouseButton = -1;
		return KEYDecode(&key);
	}
    
	else if (gMouseButton == -1)
	{
		return (int) NOKEY;
	}
    
	return (int) NOKEY;
}


int KEYActivateEscape(int escape)
{
	if(escape<0)
		return key_esc==KEY_ESCAPE?1:0;
	if(escape)
		key_esc = KEY_ESCAPE;
	else
		key_esc = KEY_INVALID;
	return key_esc==KEY_ESCAPE?1:0;
}
