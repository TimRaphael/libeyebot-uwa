/**
 * \file    servos_and_motors.c
 * \brief   Defines functions to control servos and motors
 * \author  Marcus Pham
 */

#include "servos_and_motors.h"
#include "hdt.h"
#include "eyebot.h"
#include "stdio.h"

/**
 * \brief   Initialises the IO board and sets up tables for servos
 * \return  0 on success, 1 otherwise
 */
int SERVOInit() {
    if(SERVOInitialised==1) {
        return 0;
    }
    
    int temp;
    temp = SERInit(IOBOARD, DEFAULT_BAUD, 0);
    if (temp != 0) {
        return 1;
    }
    
    //loading servos
    SERVOFirst = HDTLoadSERVO(HDT_FILE, NULL);

    //loading tables
    SERVOFirst->ptable = HDTLoadTable(HDT_FILE, (HDT_DEVICE*)SERVOFirst);
    
    SERVOInitialised = 1;
    
    
    FILE *fp;
    fp = fopen("/home/pi/eyebot/bin/hdt.txt","r");
    if (fp == NULL)
    {
        fprintf(stderr, "No HDT file present");
        exit(EXIT_FAILURE);
    }
    
    else
    {
        char* line = malloc(300*sizeof(char));
        char* buffer = malloc(100*sizeof(char));
        ssize_t read;
        size_t len = 0;
        
        int servo, low, high;
        
        while((read = getline(&line, &len, fp)) != -1) {
            if(strstr(line,"SERVO")==line) {
                buffer = strtok(line," \n");
                //servo number
                buffer = strtok(NULL, " \n");
                servo = atoi(buffer);
                //low number
                buffer = strtok(NULL, " \n");
                low = atoi(buffer);
                //high number
                buffer = strtok(NULL, " \n");
                high = atoi(buffer);
                
                SERVORange(servo, low, high);
            }
        }
    }
    return 0;
}

/**
 * \brief   Initialises the IO board and sets up tables for motors
 * \return  0 on success, 1 otherwise
 */
int MOTORInit() {
    if(MOTORInitialised==1) {
        return 0;
    }
    
    int temp;
    temp = SERInit(IOBOARD, DEFAULT_BAUD, 0);
    if (temp != 0) {
        return 1;
    }
    
    //setting encoder val
    for(int i =0; i<4; i++) {
        ENCODERBaseVal[i] = 0;
    }
    
    //loading motor info
    MOTORFirst = HDTLoadMOTOR(HDT_FILE, NULL);
    
    //loading tables
    MOTORFirst->ptable = HDTLoadTable(HDT_FILE, (HDT_DEVICE*)MOTORFirst);
    
    MOTORInitialised = 1;
    return 0;
}

/**
 * \brief   Sets the range for the servos
 * \param servo the servo to set
 * \param low the lower limit
 * \param high the upper limit
 * \return  0 on success, 1 otherwise
 */
int SERVORange(int servo, int low, int high) {
    if(SERVOInitialised == 0) {
        if(SERVOInit()==1) {
            return 1;
        }
    }
    
    char cmd[4];
    cmd[0]='S'+128;
    cmd[1]=servo;
    cmd[2]=low;
    cmd[3]=high;
    
    if(SERCheck(IOBOARD)!=0) {
        fprintf(stderr, "SERVO: Timeout!!!\n");
        SERVOInitialised=0;
        return 1;
    }
    
    for(int i =0; i<4; i++) {
        SERSendChar(IOBOARD, cmd[i]);
    }
    
    return 0;
}

/**
 * \brief   Set the given servos to the same given angle depending on table value
 * \param   servo the servo number
 * \param   angle the angle to set it to
 * \return  0 on success, 1 on failure
 */
int SERVOSet(int servo, int angle){
    if(SERVOInitialised == 0) {
        if(SERVOInit()==1) {
            return 1;
        }
    }
    
    HDT_SERVO* pServo = SERVOFirst;
    
    //loop through all the motors until find correct pmotor->regaddr, select that motor
    for(int i = 1; i<servo; i++) {
        pServo = pServo->pnext;
    }
    
    //check for null/no table entry, if null then send raw value, otherwise
    if((pServo->tabname == NULL)||(pServo->ptable->size==0)) {
        printf("No Servo table\n");
        SERVOSetRaw(servo,angle);
    }
    
    //map speed to corresponding table value
    int rawAngle = angle;
    
    //only if input is from 0-255
    if(rawAngle>pServo->high) {
        rawAngle = pServo->high;
    }
    
    if(rawAngle<pServo->low) {
        rawAngle = pServo->low;
    }
    
    int index =(pServo->ptable->size*rawAngle)/256; //if users input from 0-255
    
    rawAngle = pServo->ptable->data[index];
    
    //send raw value to IO board
    SERVOSetRaw(servo, rawAngle);
    return 0;
}

/**
 * \brief   Set the given servos to the same given angle in raw mode
 * \param   servo the servo to set
 * \param   int angle : valid values = 0-255
 * \return  0 on success, 1 otherwise
 */
int SERVOSetRaw(int servo, int angle) {
    if(SERVOInitialised == 0) {
        if(SERVOInit()==1) {
            return 1;
        }
    }
    
    char cmd[3];
    cmd[0]='s'+128;
    cmd[1]=servo;
    cmd[2]=angle;
    
    if(SERCheck(IOBOARD)!=0) {
        fprintf(stderr, "SERVO: Timeout!!!\n");
        SERVOInitialised=0;
        return 1;
    }
    
    for(int i =0; i<3; i++) {
        SERSendChar(IOBOARD, cmd[i]);
    }
    
	return 0;
}

/**
 * \brief   Set the given motor to a given speed
 * \param   motor the motor to set
 * \param   speed : motor speed
 * \details Valid values for speed :
 * - -100 to 100 (full backward to full forward)
 * - 0 for full stop
 * \return  0 on success, 1 on failure
 */
int MOTORDriveRaw(int motor, int speed) {
    if(MOTORInitialised == 0) {
        if(MOTORInit()==1) {
            return 1;
        }
    }
    
    char cmd[3];
    cmd[0]='m'+128;
    cmd[1]=motor;
    cmd[2]=speed;
    
    if(SERCheck(IOBOARD)!=0) {
        fprintf(stderr, "MOTOR: Timeout!!!\n");
        MOTORInitialised=0;
        return 1;
    }
    
    for(int i =0; i<3; i++) {
        SERSendChar(IOBOARD, cmd[i]);
    }
    
    return 0;
}

/**
 * \brief   Set the given motors to the same given speed dependant on hdt table
 * \param   motor the motor to set
 * \param   speed : motor speed in percent
 * \details Valid values for speed :
 * - -100 to 100 (full backward to full forward)
 * - 0 for full stop
 * \return  0 on success, 1 on failure
 */
int MOTORDrive(int motor, int speed) {
    if(MOTORInitialised == 0) {
        if(MOTORInit()==1) {
            return 1;
        }
    }
    
    HDT_MOTOR* pMotor = MOTORFirst;
    
    //loop through all the motors until find correct pmotor->regaddr, select that motor
    for(int i = 1; i<motor; i++) {
        pMotor = pMotor->pnext;
    }
    
    //check for null/no table entry, if null then send raw value, otherwise
    if((pMotor->tabname == NULL)||(pMotor->ptable->size==0)) {
        printf("No Motor Table\n");
        MOTORDriveRaw(motor,speed);
    }
    //map speed to corresponding table value
    int rawSpeed = 0;
    if(speed>0) {
        rawSpeed = speed;
    }
    else {
        rawSpeed = speed*(-1);
    }
    
    int index =(pMotor->ptable->size*rawSpeed)/101;
    
    rawSpeed = pMotor->ptable->data[index];
    
    if (speed<0) {
        rawSpeed = rawSpeed*(-1);
    }
    
    //send raw value to IO board
    MOTORDriveRaw(motor, rawSpeed);
    return 0;
}

/**
 * \brief   Set the given motors with PID control
 * \param   motor the motor to set
 * \param   p the p setting
 * \param   i the i setting
 * \param   d the d setting
 * \return  0 on success, 1 on failure
 */
int MOTORPID(int motor, int p, int i, int d) {
    if(MOTORInitialised == 0) {
        if(MOTORInit()==1) {
            return 1;
        }
    }
    
    char cmd[5];
    cmd[0]='d'+128;
    cmd[1]=motor;
    cmd[2]=p;
    cmd[3]=i;
    cmd[4]=d;
    
    if(SERCheck(IOBOARD)!=0) {
        fprintf(stderr, "MOTOR: Timeout!!!\n");
        MOTORInitialised=0;
        return 1;
    }
    
    for(int i =0; i<5; i++) {
        SERSendChar(IOBOARD, cmd[i]);
    }

    return 0;
}

/**
 * \brief   Shuts off PID control
 * \param   motor the motor to set
 * \return  0 on success, 1 on failure
 */
int MOTORPIDoff(int motor) {
    
    if(MOTORInitialised == 0) {
        if(MOTORInit()==1) {
            return 1;
        }
    }
    
    char cmd[2];
    cmd[0]='D'+128;
    cmd[1]=motor;
    
    if(SERCheck(IOBOARD)!=0) {
        fprintf(stderr, "MOTOR: Timeout!!!\n");
        MOTORInitialised=0;
        return 1;
    }
    
    for(int i =0; i<2; i++) {
        SERSendChar(IOBOARD, cmd[i]);
    }
    
    return 0;
}

/**
 * \brief   Sets the controlled motor speed in ticks/sec
 * \param   motor the motor to set
 * \param   ticks the number of ticks per sec
 * \return  0 on success, 1 on failure
 */
int MOTORSpeed(int motor, int ticks) {
    if(MOTORInitialised == 0) {
        if(MOTORInit()==1) {
            return 1;
        }
    }
    
    char cmd[3];
    cmd[0]='M'+128;
    cmd[1]=motor;
    cmd[2]=ticks;
    
    if(SERCheck(IOBOARD)!=0) {
        fprintf(stderr, "MOTOR: Timeout!!!\n");
        MOTORInitialised=0;
        return 1;
    }
    
    for(int i =0; i<3; i++) {
        SERSendChar(IOBOARD, cmd[i]);
    }

    return 0;
}

/**
 * \brief   Reads the value from the encoder
 * \param   quad the encoder to read from
 * \return  the encoder value on success, 1 on failure
 */
int ENCODERRead(int quad) {
    if(MOTORInitialised == 0) {
        if(MOTORInit()==1) {
            return -1;
        }
    }
    
    char cmd[2];
    cmd[0]='e'+128;
    cmd[1]=quad;
    
    if(SERCheck(IOBOARD)!=0) {
        fprintf(stdout, "ENCODER: Timeout!!!\n");
        MOTORInitialised=0;
        return -1;
    }
    
    for(int i =0; i<2; i++) {
        SERSendChar(IOBOARD, cmd[i]);
    }

    while(SERReceiveChar(IOBOARD)!='>') {
        continue;
    }
    
    unsigned char buf[4];
    
    buf[0] = SERReceiveChar(IOBOARD);
    buf[1] = SERReceiveChar(IOBOARD);
    buf[2] = SERReceiveChar(IOBOARD);
    buf[3] = SERReceiveChar(IOBOARD);
    
    
    unsigned int ret;
    ret = (buf[0]<<24)|(buf[1]<<16)|(buf[2] << 8)|buf[3];
    
    return ret-ENCODERBaseVal[quad-1];
}

/**
 * \brief   Resets the encoder, in reality just stores the current value of the encoder and subtracts it from future values
 * \param   quad the encoder to reset
 * \return  0 on success, 1 on failure
 */
int ENCODERReset(int quad) {
    if(MOTORInitialised == 0) {
        if(MOTORInit()==1) {
            return 1;
        }
    }
    
    char cmd[2];
    cmd[0]='e'+128;
    cmd[1]=quad;
    
    if(SERCheck(IOBOARD)!=0) {
        fprintf(stderr, "ENCODER: Timeout!!!\n");
        MOTORInitialised=0;
        return -1;
    }
    
    for(int i =0; i<2; i++) {
        SERSendChar(IOBOARD, cmd[i]);
    }

    while(SERReceiveChar(IOBOARD)!='>') {
        continue;
    }
    
    char buf[4];
    
    buf[0] = SERReceiveChar(IOBOARD);
    buf[1] = SERReceiveChar(IOBOARD);
    buf[2] = SERReceiveChar(IOBOARD);
    buf[3] = SERReceiveChar(IOBOARD);
    
    int ret;
    ret = (buf[0]<<24)|(buf[1]<<16)|(buf[2] << 8)|buf[3];
    
    ENCODERBaseVal[quad-1] = ret;

    return 0;
}