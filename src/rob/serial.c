/**
 * \file    serial.c
 * \brief   Defines functions to communicate via serial
 * \author  Marcus Pham
 */

#include "serial.h"
#include "eyebot.h"
#include <stdlib.h>

/**
 * \brief   Obtains the system paths to the serial usb devices matching description in hdt.txt
 * \return  0 on success, 1 on failure/alerady initialised
 */
int SERGetPaths() {
    if(SERPathInit == 1) {
        return 1;
    }
    
    //default path to IOBoard
    sprintf(USBPATH1, "/dev/ttyACM0");
    
    FILE *fp;
    fp = fopen(HDT_FILE,"r");
    if (fp == NULL) {
        fprintf(stderr, "No HDT file present");
        return 1;
    }
    
    else {
        char* line;
        char* buffer;
        
        char type[100];
        char params[100];
        ssize_t read;
        size_t len = 0;
        
        char results[1000];
        sprintf(results, OSExecute("/home/pi/eyebot/bin/usbSearch"));
        
        while((read = getline(&line, &len, fp)) != -1) {
            if(strstr(line,"USB")==line) {
                buffer = strtok(line, " \n");
                
                buffer = strtok(NULL, " \n"); //the number used
                int number = atoi(buffer);
                
                buffer = strtok(NULL, " \n"); //the keyword to search
                
                char* pathBuffer;
                pathBuffer = strtok(results, " _-\n");
                
                while(pathBuffer!=NULL) {
                    //if correct, find the corresponding value after ':'
                    if(strcmp(pathBuffer,buffer)==0) {
                        pathBuffer = strtok(NULL, ":\n");
                        pathBuffer = strtok(NULL, ":\n");
                        break;
                    }
                    pathBuffer = strtok(NULL, " _-\n");
                }
                
                switch (number) {

                    case 1:
                        if(pathBuffer == NULL) {
                            break;
                        }
                        sprintf(USBPATH1, "%s", pathBuffer);
                        break;
                    case 2:
                        if(pathBuffer == NULL) {
                            break;
                        }
                        sprintf(USBPATH2, "%s", pathBuffer);
                        break;
                    case 3:
                        if(pathBuffer == NULL) {
                            break;
                        }
                        sprintf(USBPATH3, "%s", pathBuffer);
                        break;
                    case 4:
                        if(pathBuffer == NULL) {
                            break;
                        }
                        sprintf(USBPATH4, "%s", pathBuffer);
                        break;
                    default:
                    {
                        fprintf(stdout, "Invalid usb port number (1-4)");
                        return 1;
                    }
                        
                }
            }
        }
    }
    SERPathInit = 1;
    return 0;
}

/**
 * \brief   sets the attributes for the port
 * \param   fd      the port handle
 * \param   speed   the baud rate
 * \param   parity  the parity of the port
 * \param   handshake the handshake type
 * \return  0 on success, -1 otherwise
 */
int SERSet_interface_attribs (int fd, int speed, int parity, int handshake) {
    struct termios tty;
    memset (&tty, 0, sizeof tty);
    if (tcgetattr (fd, &tty) != 0)
    {
        printf("error from tcgetattr\n");
        return -1;
    }
    
    cfsetospeed (&tty, speed);
    cfsetispeed (&tty, speed);
    
    tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
    // disable IGNBRK for mismatched speed tests; otherwise receive break
    // as \000 chars
    tty.c_iflag &= ~IGNBRK;         // disable break processing
    tty.c_lflag = 0;                // no signaling chars, no echo,
    // no canonical processing
    tty.c_oflag = 0;                // no remapping, no delays
    tty.c_cc[VMIN]  = 0;            // read doesn't block
    tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout
    
    tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl
    
    tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
    // enable reading
    tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
    tty.c_cflag |= parity;
    tty.c_cflag &= ~CSTOPB;
    /*if(handshake==1) {
        tty.c_cflag &= ~CRTSCTS;
    }*/
    
    if (tcsetattr (fd, TCSANOW, &tty) != 0)
    {
        printf("error from tcsetattr\n");
        return -1;
    }
    return 0;
}

/**
 * \brief   Sets the blocking flags for the serial port
 * \param   fd      the port handle
 * \param   should_block   an boolean whether to block port or not
 */
void SERSet_blocking (int fd, int should_block) {
    struct termios tty;
    memset (&tty, 0, sizeof tty);
    if (tcgetattr (fd, &tty) != 0)
    {
        printf("error from tggetattr\n");
        return;
    }
    
    tty.c_cc[VMIN]  = should_block ? 1 : 0;
    tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout
}

/**
 * \brief   Initialises the port
 * \param   interface the inteface type
 * \param   baud      the baud rate
 * \param   handshake  the handshake type
 * \return  0 on success, 1 otherwise
 */
int SERInit(int interface, int baud, int handshake)
{
    if(SERPathInit!=1) {
        SERGetPaths();
    }
    
    char path[100];;
    switch(interface) {
        case 1:
            strcpy(path,USBPATH1);
            SERType = 1;
            SERBaud[interface] = baud;
            SERHandshake[interface] = handshake;
            break;
        case 2:
            strcpy(path,USBPATH2);
            SERType = 2;
            SERBaud[interface] = baud;
            SERHandshake[interface] = handshake;
            break;
        case 3:
            strcpy(path,USBPATH3);
            SERType = 3;
            SERBaud[interface] = baud;
            SERHandshake[interface] = handshake;
            break;
        case 4:
            strcpy(path,USBPATH4);
            SERType = 4;
            SERBaud[interface] = baud;
            SERHandshake[interface] = handshake;
            break;
        default:
            fprintf(stderr, "incorrect interface, %d invalid\n", interface);
            return 1;
    }
    
    //opening the port
    fd = open (path, O_RDWR | O_NOCTTY | O_SYNC);
    if (fd < 0)
    {
        fprintf(stderr, "error opening port\n");
        return 1;
    }
    
    switch(baud) {
        case 0:
            SERSet_interface_attribs (fd, B0, 0, handshake);
            break;
        case 50:
            SERSet_interface_attribs (fd, B50, 0, handshake);
            break;
        case 75:
            SERSet_interface_attribs (fd, B75, 0, handshake);
            break;
        case 110:
            SERSet_interface_attribs (fd, B110, 0, handshake);
            break;
        case 134:
            SERSet_interface_attribs (fd, B134, 0, handshake);
            break;
        case 150:
            SERSet_interface_attribs (fd, B150, 0, handshake);
            break;
        case 200:
            SERSet_interface_attribs (fd, B200, 0, handshake);
            break;
        case 300:
            SERSet_interface_attribs (fd, B300, 0, handshake);
            break;
        case 600:
            SERSet_interface_attribs (fd, B600, 0, handshake);
            break;
        case 1200:
            SERSet_interface_attribs (fd, B1200, 0, handshake);
            break;
        case 1800:
            SERSet_interface_attribs (fd, B1800, 0, handshake);
            break;
        case 2400:
            SERSet_interface_attribs (fd, B2400, 0, handshake);
            break;
        case 4800:
            SERSet_interface_attribs (fd, B4800, 0, handshake);
            break;
        case 9600:
            SERSet_interface_attribs (fd, B9600, 0, handshake);
            break;
        case 19200:
            SERSet_interface_attribs (fd, B19200, 0, handshake);
            break;
        case 38400:
            SERSet_interface_attribs (fd, B38400, 0, handshake);
            break;
        case 57600:
            SERSet_interface_attribs (fd, B57600, 0, handshake);
            break;
        case 115200:
            SERSet_interface_attribs (fd, B115200, 0, handshake);
            break;
        case 230400:
            SERSet_interface_attribs (fd, B230400, 0, handshake);
            break;
        default:
            fprintf(stdout, "invalid baud rate!!!\n");
            return 1;
    }
    
    //setting blocking
    SERSet_blocking (fd, 0);                // set no blocking
    return 0;
}

/**
 * \brief   Sends a single character to the given port handle
 * \param   interface the inteface type
 * \param   ch      the character to send
 * \return  0 on success, 1 otherwise
 */
int SERSendChar(int interface, char ch) {
    if(SERType != interface) {
        SERInit(interface, SERBaud[interface], SERHandshake[interface]);
    }
    
    char temp[1];
    temp[0]=ch;
    int n = write(fd,temp,1);
    if (n==1) {
        return 0;
    }
    else {
        return 1;
    }
}

/**
 * \brief   Sends a string to the given port handle
 * \param   interface the inteface type
 * \param   buf      the string to send
 * \param   size     the length of the string to send
 * \return  0 on success, 1 otherwise
 */
int SERSend(int interface, char *buf) {
    if(SERType != interface) {
        SERInit(interface, SERHandshake[interface], SERBaud[interface]);
    }
    
    int size = sizeof(buf);
    
    int n = write(fd,buf,size);
    if(n==sizeof(buf)) {
        return 0;
    }
    else
        return 1;
}

/**
 * \brief   Receives a string from port handle
 * \param   interface the inteface type
 * \param   buf      the string to store the received bytes
 * \param   size     the length of the string to receive
 * \return  n       the length of the received string 
 */
int SERReceive(int interface, char *buf) {
    if(SERType != interface) {
        SERInit(interface, SERBaud[interface], SERHandshake[interface]);
    }
    
    int size = sizeof(buf);
    
    char buffer[size+1];
    int n = read(fd,buffer,size);
    
    buffer[n+1]='\0';
    
    char *temp = buf;
    strcpy(temp, buffer);
    
    return n;
}

/**
 * \brief   Receives a single character from the given port handle
 * \param   interface the inteface type
 * \return  the character received
 */
char SERReceiveChar(int interface) {
    if(SERType != interface) {
        SERInit(interface, SERHandshake[interface], SERBaud[interface]);
    }
    char ch[1];
    int n = read(fd,ch,1);
    return ch[0];
}

/**
 * \brief   checks and flushes the port by sending enter until seeing prompt
 * \param   interface the inteface type
 * \return  0 on success, 1 otherwise
 */
bool SERCheck(int interface) {
    if(SERType != interface) {
        SERInit(interface, SERBaud[interface], SERHandshake[interface]);
    }
    
    int i=0;
    for(i; i<TIMEOUT; i++) {
        SERSendChar(interface,13);
        usleep(100);
        if(SERReceiveChar(interface)=='>') {
            SERFlush(interface);
            return 0;
        }
    }
    SERClose(interface);
    return 1;
}

/**
 * \brief   flushes the port
 * \param   interface the inteface type
 * \return  0 on success, 1 otherwise
 */
int SERFlush(int interface) {
    if(SERType != interface) {
        SERInit(interface, SERBaud[interface], SERHandshake[interface]);
    }
    usleep(10000); //required to make flush work, for some reason
    tcflush(fd,TCIOFLUSH);
    return 0;
}

/**
 * \brief   closes the port
 * \param   interface the inteface type
 * \return  0 on success, 1 otherwise
 */
int SERClose(int interface) {
    if(SERType != interface) {
        SERInit(interface, SERBaud[interface], SERHandshake[interface]);
    }
    SERType = -1;
    close(fd);
    return 0;
}
