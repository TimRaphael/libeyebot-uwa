/**
 * \file		irtv.c
 * \brief   Defines IRTV functions
 * \author  Remi KEAT
 * \brief Currently under construction by Marcus Pham to initialise thread to handle IRTV keypresses on initialisation
 */

#include "irtv.h"
#include "eyebot.h"
#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>

int controllerCode;
int running = 0, holdDelay = 5;

//file descriptor for the lirc
int fd;

/**
 * \brief	Initializes the IR remote control decoder by calling IRTVInit() with the device name found in the corresponding HDT entry.
 * \param   DeviceSemantics semantics, for the IR remote this will be the controller code
 * \return  int retVal :
 * \details
 * - 0 = ok
 * - 1 = HDT file error
 * - 2 = invalid or missing "IRTV" HDT entry for this semantics
 * - 3 = unable to initialise IR input
 */
int IRTVInit(int semantics) {
    
    if(IRTV_INITIAL == 1) {
        return 1;
    }
    
	char *IRTVFileName = malloc(200);
	char *command = malloc(200);;
	//set the name for the IRTV config file to use
	sprintf(IRTVFileName,"/home/pi/eyebot/bin/ir/%d_lircd.conf",semantics);

	//setup the command that will be executed
	sprintf(command,"sudo cp %s /etc/lirc/lircd.conf",IRTVFileName);
	//replace the config that IR input is currently using with the one we want
	system(command);
	/*
	 * need to call the following system command to allow for the IR input
	 * Restart is used to make sure that if the config file is changed the correct
	 * 	config file is used. If it is not restarted then the previous config file
	 * 	is used. Note: if is says that it failed to stop LIRC in most cases it is
	 * 	fine because it may already have been stopped. If you know that it is 
	 * 	currently running and it fails to stop it then something may be wrong.
	 */
	system("sudo /etc/init.d/lirc restart");
	//Initiate LIRC. If it fails then return and indicate the error
    
    fd = lirc_init("lirc", 1);
    
    if(fd==-1)
    {
		fprintf(stderr,"Unable to initialise IR input");
		//stop the IR input because we were unable to initialise the input
		system("sudo /etc/init.d/lirc stop");
		free(IRTVFileName);
		free(command);
		return 3;
	}
    controllerCode = semantics;
	free(IRTVFileName);
	free(command);
	running = 1;
    
    //about to change settings to non-blocking
    
    int flags = fcntl(fd, F_GETFL, 0);
    if(flags!=-1){
        fcntl(fd,  F_SETFL, flags | O_NONBLOCK);
    }
    
    IRTV_INITIAL=1;
	return 0;
}



/**
 * \brief		Waits for IR input and returns the keycode.
 * \return 	int retVal : Next code from the buffer
 * \details 0 = no key
 */
int IRTVGet(void) {
    
    if(IRTV_INITIAL!=1) {
        IRTVInit(786);
    }
    
    int codeIndex = 24;
    char *code;
    char temp[3] = "";
    int retChar = 0;
    int irDelay = 0;
    
    while(retChar==0) {
    
    //exit if the return value is -1 because the LIRC socket is closed
    if (lirc_nextcode(&code)==-1){
        return -1;
    }
    
    if(code!=NULL) {
        temp[0] = code[17];
        temp[1] = code[18];
        temp[2] = '\0';
        
        sscanf(temp,"%x",&irDelay);
        if (irDelay > 0 && irDelay < holdDelay)
        {
            return 0;
        }
        
        if (code[codeIndex] == 'R'){
            if (code[codeIndex+1] == 'E')
            {
                retChar = code[codeIndex];
            }
            else
            {
                retChar = '>';
            }
        }
        else
        {
            retChar = code[codeIndex];
        }
        
        free(code);
    }
    }
    
    return retChar;
    
}

/**
 * \brief   Returns the  IR input
 * \return 	int retVal : Next code from the buffer
 * \details 0 = no key
 */
int IRTVRead(void) {
    
    if(IRTV_INITIAL!=1) {
        IRTVInit(786);
    }
    
    int codeIndex = 24;
    char *code;
    char temp[3] = "";
    int retChar = 0;
    
    //exit if the return value is -1 because the LIRC socket is closed
    if (lirc_nextcode(&code)==-1){
        return -1;
    }
    
    if(code!=NULL) {
        temp[0] = code[17];
        temp[1] = code[18];
        temp[2] = '\0';
        
        if (code[codeIndex] == 'R'){
            if (code[codeIndex+1] == 'E')
            {
                retChar = code[codeIndex];
            }
            else
            {
                retChar = '>';
            }
        }
        else
        {
            retChar = code[codeIndex];
        }
        
        free(code);
    }
    
    return retChar;
}

/**
 * \brief		Terminates the remote control decoder and releases the irtv thread.
 */
void IRTVRelease(void) {
    if(IRTV_INITIAL!=1) {
        IRTVInit(786);
    }
    
	lirc_deinit();
	/* 
	 * need to call the following command to stop IR input otherwise the pi could 
	 * 		still be controlled with the remote control
	 */
	system("sudo /etc/init.d/lirc stop");
	running = 0;
}

int IRTVFlush(void) {
    if(IRTV_INITIAL!=1) {
        IRTVInit(786);
    }
    
    IRTVRelease();
    return 0;
}

/**
 * \brief		Checks to see if IR input is enabled;
 * \return		int retVal: whether IR input is enabled
 * \details		
 * 	-0: not enabled
 * 	-1: enabled
 */
int IRTVGetStatus(){
    
    if(IRTV_INITIAL!=1) {
        IRTVInit(786);
    }
    
	return running;
}

void IRTVSetControllerCode(int semantics) {
    if(IRTV_INITIAL!=1) {
        IRTVInit(786);
    }
    
	controllerCode = semantics;
}

int IRTVGetControllerCode(){
    
    if(IRTV_INITIAL!=1) {
        IRTVInit(786);
    }

	return controllerCode;
}

/**
 * \brief	sets the hold to prevent multiple presses being registered 
 * 			when a IR remote button is pressed
 * \param	int delay: the value to set the delay to, larger value equal larger delay
 */
void IRTVSetHoldDelay(int delay){
    
    if(IRTV_INITIAL!=1) {
        IRTVInit(786);
    }
    
	holdDelay = delay;
}
