/**
 * \file improc.c
 * \brief Defines the image processing functions for the eyebot
 * \author Thomas Braunl
 * \author Marcus Pham
 */

#include "improc.h"
#include "eyebot.h"

#define black 0
#define white 255

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

/**
 * \brief Sets the mode for the image processing.
 * \param mode the mode CAMMode type specifying the resolution of the camera
 * \return 0 if initialised correctly, otherwise 1
 */
int IPSetSize(int mode)
{
    switch (mode){
        case QQVGA:
        IP_WIDTH = QQVGA_X;
        IP_HEIGHT = QQVGA_Y;
        break;
        case  QVGA:
        IP_WIDTH = QVGA_X;
        IP_HEIGHT = QVGA_Y;
        break;
        case  VGA:
        IP_WIDTH = VGA_X;
        IP_HEIGHT = VGA_Y;
        break;
        case CAM1MP:
        IP_WIDTH = CAM1MP_X;
        IP_HEIGHT = CAM1MP_Y;
        case CAMHD:
        IP_WIDTH = CAMHD_X;
        IP_HEIGHT = CAMHD_Y;
        case CAM5MP:
        IP_WIDTH = CAM5MP_X;
        IP_HEIGHT = CAM5MP_Y;
        default:
        return IP_FAILURE;
    }
    
    IPInitialised = 1;
    IP_PIXELS = IP_WIDTH * IP_HEIGHT;
    IP_SIZE   = 3 * IP_PIXELS;
    return IP_SUCCESS;
}

/**
 * \brief Read PNM file, fill/crop if req.; return 0 for color, 1 for gray image
 * \param filename the file to open
 * \param img the buffer to store the image
 * \return Returns: 1 =PBM, 2 =PGM, 3 =PPM (or 4, 5, 6 for binary versions)
 */
int IPReadFile(char *filename, BYTE* img)  // Read PNM file, fill/crop if req.; return 0 for color, 1 for gray image
// Returns: 1 =PBM, 2 =PGM, 3 =PPM (or 4, 5, 6 for binary versions)
{   char next, code[100];
    int  codecount[4] = {0, 1, 1, 3};  /* 1 byte for PBM, PGM, 3 bytes for PPM */
    int  codenum, ascii, width, height, levels, count = 0;
    int  maxheight, maxwidth, num, last;
    FILE *fp = fopen(filename, "r");
    
    if (fp == NULL) { printf("!error: read file\n"); return -1;}
    
    fscanf(fp, "%s", code);
    codenum = (int) (code[1] - '0'); // make into an integer
    printf("%d",codenum);
    ascii = codenum<4;  /* P1,2,3 are ASCII, P4,5,6 are binary */
    if (!ascii) codenum = codenum - 3;  //reduce to 1..3
    if (codenum<1 || codenum>3) { printf("bad image code\n"); return -1;}
    do next = fgetc(fp);  // clear white space
    while (next=='\n' || next==' ');
    
    while (next=='#')  /* comment until end of line */
    { do
      { printf("%c", next);
        next = fgetc(fp);
      } while (next !='\n');
      printf("\n");
      next = fgetc(fp);
    }
    ungetc(next, fp);  // give character back
    fscanf(fp, "%d %d", &width, &height);
    if (codenum>1) fscanf(fp, "%d", &levels);
    printf("Read %s file %s: W%d H%d\n", code, filename, width, height);
    
    do next = fgetc(fp);  // clear white space
    while (next=='\n' || next==' ');
    ungetc(next, fp);  // give character back
    
    if (codenum==1 && ascii) // PBM ASCII
    { for (int i=0; i<height; i++)
        for (int j=0; j<width; j++)
        { fscanf(fp, "%d", num);
            if (i<IP_HEIGHT && j<IP_WIDTH)  // read image
            if (num) img[count++] = 0; else img[count++] = 255;
        }
        for (int j=width; j<IP_WIDTH; j++) img[count++] = 0; // fill line end
        
        for (int i=height; i<IP_HEIGHT; i++)
        for (int j=0; j<IP_HEIGHT; j++)
        img[count++] = 0; // fill remaining lines
    }
    
    else if (codenum==1 && !ascii) // PBM BINARY
    { for (int i=0; i<height; i++)
        { for (int j=0; j<width; j+=8)
            { if (i<IP_HEIGHT && j<IP_WIDTH)  // read image
                { num = fgetc(fp);
                    if (j+8 < width) last = 8; else last = width % 8;
                    for (int l=0; l<last; l++)
                    if (num & (0x80 >> l)) img[count++] = 0; else img[count++] = 255;
                }
                else fgetc(fp);  // read pixels beyond parameter size
            }
            for (int j=width; j<IP_WIDTH; j++)
            img[count++] = 0; // fill line end
        }
        
        for (int i=height; i<IP_HEIGHT; i++)
        for (int j=0; j<IP_WIDTH; j++)
        img[count++] = 0;  // fill remaining lines
    }
    
    
    else // PGM (gray), PPM (color)
    { maxwidth  = MAX(width,  IP_WIDTH);
        maxheight = MAX(height, IP_HEIGHT);
        for (int i=0; i<maxheight; i++)
        { for (int j=0; j<maxwidth; j++)
            for (int k=0; k<codecount[codenum]; k++)
            { if (i<MIN(IP_HEIGHT,height) && j<MIN(IP_WIDTH,width))  // read image
                { if (ascii) fscanf(fp, "%d", (int *) &img[count++]);
                    else img[count++] = fgetc(fp);
                }
                else if (j>=IP_WIDTH || i>=IP_HEIGHT) fgetc(fp);  // read pixels beyond parameter size
                else if (j>=width || i>=height) img[count++] = 0;  // fill remaining area with black pixels
            }
        }
    }
    fclose(fp);
    return codenum; // (P) 1..3 fpr PBM, PGM, PPM
}

/**
 * \brief Write color PNM file
 * \param filename the file to save to
 * \param img the buffer to store the image
 * \return 0 always
 */
int IPWriteFile(char *filename, BYTE* img)      // Write color PNM file
{   FILE *fp = fopen(filename, "w");
    int code;
    int ascii = 0;  // write as binary file
    
    if (fp == NULL) { printf("error: write file\n"); return -1;}
    
    if (ascii) code=3; else code=6;
    fprintf(fp, "P%d\n%d %d\n255\n", code, IP_WIDTH,IP_HEIGHT);
    for (int i=0; i<IP_SIZE; i++)
      if (ascii) fprintf(fp, " %d", img[i]);
        else     fputc(img[i], fp);
    fprintf(fp,"\n");
    fclose(fp);
    return 0;
}

/**
 * \brief Write greyscale PNM file
 * \param filename the file to save to
 * \param img the buffer to store the image
 * \return 0 always
 */
int IPWriteFileGray(char *filename, BYTE* img)  // Write gray scale PGM file
{
    FILE *fp = fopen(filename, "w");
    int code;
    int ascii = 0;  // write as binary file
    
    if (fp == NULL) { printf("error: write file\n"); return -1;}
    
    if (ascii) code=2; else code=5;
    fprintf(fp, "P%d\n%d %d\n255\n", code, IP_WIDTH,IP_HEIGHT);
    for (int i=0; i<IP_PIXELS; i++)
      if (ascii) fprintf(fp, " %d", img[i]);
        else     fputc(img[i], fp);
    fprintf(fp,"\n");
    fclose(fp);
    return 0;
}

/**
 * \brief apply Laplace tranform to the image
 * \param imageIn the input image
 * \param imageOut the formatted output image
 * \return 0 always
 */
void IPLaplace(BYTE* grayIn, BYTE* grayOut)
{   int i, delta;
    
    for (i = IP_WIDTH; i < (IP_HEIGHT-1)*IP_WIDTH; i++)
    {
        delta  = abs(4 * grayIn[i] - grayIn[i-1] - grayIn[i+1] -
                        -grayIn[i-IP_WIDTH] -grayIn[i+IP_WIDTH]);
        
        if (delta > white) delta = white;
        grayOut[i] = (BYTE) delta;
    }
    
}

/**
 * \brief apply Sobel tranform to the image
 * \param imageIn the input image
 * \param imageOut the formatted output image
 * \return 0 always
 */
void IPSobel(BYTE *imageIn, BYTE *imageOut)
{   int i, grad, deltaX, deltaY;
    
    memset(imageOut, 0, IP_WIDTH); // clear first row
    
    for (i = IP_WIDTH; i < (IP_HEIGHT-1)*IP_WIDTH; i++)
    {   deltaX = 2*imageIn[i+1] + imageIn[i-IP_WIDTH+1] + imageIn[i+IP_WIDTH+1]
                -2*imageIn[i-1] - imageIn[i-IP_WIDTH-1] - imageIn[i+IP_WIDTH-1];
           
        deltaY = imageIn[i-IP_WIDTH-1] + 2*imageIn[i-IP_WIDTH] + imageIn[i-IP_WIDTH+1]
                -imageIn[i+IP_WIDTH-1] - 2*imageIn[i+IP_WIDTH] - imageIn[i+IP_WIDTH+1];
        
        grad = (abs(deltaX) + abs(deltaY)) / 3;
        if (grad > white) grad = white;
        imageOut[i] = (BYTE) grad;
    }
    
    memset(imageOut + i, 0, IP_WIDTH); // clear last row
}

/**
 * \brief Transfer color to gray image
 * \param imgIn the input image
 * \param grayOut the formatted output image
 * \return 0 always
 */
void IPCol2Gray(BYTE* imgIn, BYTE* grayOut)      // Transfer color to gray image
{
    for (int i=0; i<IP_PIXELS; i++) {
        grayOut[i] = ((imgIn[3*i]+imgIn[3*i+2])>>2) + (imgIn[3*i+1]>>1);   // gray = R/4 + G/2 + B/4;
    }
}

/**
 * \brief Transfer grey to color (3-byte) image
 * \param imgIn the input image
 * \param colOut the formatted output image
 * \return 0 always
 */
void IPGray2Col(BYTE* imgIn, BYTE* colOut)
{
    for (int i=0; i<IP_PIXELS; i++)
    { colOut[3*i]   = imgIn[i];
        colOut[3*i+1] = imgIn[i];
        colOut[3*i+2] = imgIn[i];
    }
}

/**
 * \brief Transfer grey to color (3-byte) image
 * \param imgIn the input image
 * \param colOut the formatted output image
 * \return 0 always
 */
COLOR IPPRGB2Col(BYTE r, BYTE g, BYTE b)
{ // RGB to color for pixel
  return (r<<16) | (b<<8) | b;
}


void IPPCol2RGB(COLOR col, BYTE* r, BYTE* g, BYTE* b)
{ // color to RGB for pixel
  *r = (col>>16) & 0xFF;
  *g = (col>>8)  & 0xFF;
  *b =  col      & 0xFF;
}


void IPRGB2Col(BYTE* r, BYTE* g, BYTE* b, BYTE* imgOut)
{ // Transform 3*gray to color image
  for (int i=0; i<IP_PIXELS; i++)
  { imgOut[3*i  ] = r[i];
    imgOut[3*i+1] = g[i];
    imgOut[3*i+2] = b[i];
  }
}


#define THRES_I 25 
void IPPRGB2HSI(BYTE r, BYTE g, BYTE b, BYTE* h, BYTE* s, BYTE* i)
// Convert RGB to hue, sat, int. Use hue=0 for gray values (NO HUE!)
{ BYTE delta, max, min;

  h=0; s=0; i=0; /* init output values */  
  max   = MAX(r, MAX(g,b));
  min   = MIN(r, MIN(g,b));
  delta = max - min;

  *i = (r+b)>>2 + g>>1; /* gray = R/4 + G/2 + B/4; */
  if (*i>0) *s = 1 - min/(*i);  

  if (2*delta > max && (*i) > THRES_I)  /* not gray */
  { if (r==max) *h =  42 + 42*(g-b)/delta; /* 1*42 */
    if (g==max) *h = 126 + 42*(b-r)/delta; /* 3*42 */
    if (b==max) *h = 210 + 42*(r-g)/delta; /* 5*42 */
  } /* now: hue is in range [0, 1..252] */
}



BYTE IPPRGB2Hue(BYTE r, BYTE g, BYTE b)
// Convert RGB to hue, sat, int. Use hue=0 for gray values (NO HUE!)
{ BYTE  h, s, i;
  IPPRGB2HSI(r, g, b, &h, &s, &i);
  return h;
}


void IPPCol2HSI(COLOR c, BYTE* h, BYTE* s, BYTE* i)
// Convert single color value to hue, sat, int. Use hue=0 for gray values (NO HUE!)
{ BYTE r, g, b;
    
  r = (c>>16) & 0xFF;
  g = (c>>8)  & 0xFF;
  b =  c      & 0xFF;

  IPPRGB2HSI(r, g, b, h, s, i);
}


void IPCol2HSI(BYTE* img, BYTE* h, BYTE* s, BYTE* i)
{ for(int j=0; j<IP_PIXELS; j++)
    IPPRGB2HSI(img[j],img[j+1],img[j+2], &h[j], &s[j], &i[j]);
}


void IPOverlay(BYTE* c1, BYTE* c2, BYTE* cOut)
// Overlay c2 onto c1, all color images
{ for (int i=0; i<IP_SIZE; i+=3)
    if (c2[i] | c2[i+1] | c2[i+2])
      { cOut[i]=c2[i]; cOut[i+1]=c2[i+1]; cOut[i+2]=c2[i+2];}
    else
      { cOut[i]=c1[i]; cOut[i+1]=c1[i+1]; cOut[i+2]=c1[i+2];}
}


void IPOverlayGray(BYTE* g1, BYTE* g2, COLOR col, BYTE* cOut)
// Overlay gray image g2 onto g1, rgb (0=red, 1=green, 2=blue), outImg in color
{ int  i, i3;
  BYTE r, g, b;

  IPPCol2RGB(col, &r, &g, &b);
  
  for (i=0; i<IP_PIXELS; i++)
  { i3 = 3*i;
    if (g2[i])
      { cOut[i3]=r;     cOut[i3+1]=g;     cOut[i3+2]=b; }
    else
      { cOut[i3]=g1[i]; cOut[i3+1]=g1[i]; cOut[i3+2]=g1[i]; }
  }
}

