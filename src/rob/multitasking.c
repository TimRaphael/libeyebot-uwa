/**
 * \file    multitasking.c
 * \brief   Defines multitasking functions
 * \author  Marcus Pham
 */

#include "multitasking.h"
#include "eyebot.h"
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>

#include <pthread.h>
#include <semaphore.h>

/**
 * \brief initialises the threads
 * \return 0 always
 */
int  MTInit() {
    return 0;
}

/**
 * \brief sleeps for 10ms ie n/100s
 * \param n the number of periods waited
 * \return 0 always
 */
int MTSleep(int n) {
    usleep(10000);
    return 0;
}

/**
 * \brief Create and initialize thread with given ID
 * \param fct the function to spawn
 * \param if td the id of the function
 * \return the task item
 */
TASK MTSpawn (void* (*fct) (void*), int id) {
    TASK t;
	pthread_create(&(t.thread), NULL, fct, NULL);
    t.identifier = id;
	return t;
}

/**
 * \brief kills the task t
 * \param TASK t the task to kill
 * \return 0 on success, 1 otherwise
 */
int MTKill(TASK t) {
	return pthread_cancel(t.thread);
}

/**
 * \brief gets the ID of the taks
 * \param TASK t the task to  search for ID
 * \return 0 on success, 1 otherwise
 */
int MTGetUID(TASK t) {
    return t.identifier;
}

/**
 * \brief kills current task
 * \return 0 on success, 1 otherwise
 */
int MTExit(void) {
    return 0;
}


/**
 * \brief initialises the semaphores
 * \param SEMA sem the semaphore
 * \param val the value to set it at
 * \return 0 on success, 1 otherwise
 */
int SEMAInit(SEMA *sem, int val) {
    sem_t *psemtmp;
    
    psemtmp = (sem_t *)(sem);
    
    return sem_init(psemtmp,0,val);
}


/**
 * \brief locks the given semaphoe
 * \param SEMA sem the semaphore to lock
 * \return 0 on success, 1 otherwise
 */
int SEMALock(SEMA *sem){
    sem_t *psemtmp;
    
    psemtmp = (sem_t *)(sem);
    
    return sem_wait(psemtmp);
}

/**
 * \brief unlocks the given semaphore
 * \param SEMA sem the semaphore to unlock
 * \return 0 on success, 1 otherwise
 */
int  SEMAUnlock(SEMA *sem){
    return sem_post(sem);
}

/**
 * \brief releases the semaphore
 * \param SEMA sem the semaphore to unlock
 * \return 0 on success, 1 otherwise
 */
int SEMARelease(SEMA *sem) {
    return sem_destroy(sem);
}



