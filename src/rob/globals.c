/**
 * \file    globals.c
 * \brief   Defines global variables
 * \author  Remi KEAT
 */

#include "globals.h"
#include "eyebot.h"

struct mpsse_context* gDeviceHandle;
LCD_HANDLE* gLCDHandle;
bool gLCDEnabled;
int gCurPosX, gCurPosY;
int gMousePosX, gMousePosY, gMouseButton;
TOUCH_MAP* gTouchMap;

