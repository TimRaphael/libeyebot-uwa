/**
 * \file    lcd.c
 * \brief   Defines functions to interact with the LCD screen
 * \author  Marcus Pham & Remi KEAT
 */

#include "lcd.h"
#include "system.h"
#include "key.h"
#include "listmenu.h"
#include "improc.h"
#include "eyebot.h"

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))


int RPi_ver = 3;

/**
 * \brief   Error Handler
 * \param   display the Display object
 * \param   theEvent the event that occurred
 * \return  0 always
 */
static int LCDAppErrHandler(Display *display, XErrorEvent *theEvent)
{
	gLCDHandle->X11Error = true;
	return 0;
}

/**
 * \brief   Return the XColor color from the color name
 * \param   char* colorName
 * \return  XColor color
 */
XColor LCDGetColorFromName(char* colorName)
{
	XColor color;
	XAllocNamedColor(gLCDHandle->d, gLCDHandle->colormap, colorName, &color, &color);
	return color;
}

/**
 * \brief   Convcerts an RGB colour to XColor
 * \param   rgb the COLOR object
 * \return  col the XColor equivalent
 */
XColor LCDRGBtoXCol(COLOR rgb) {
    XColor col;
     col.red = ((rgb>>16)&0xFF)*257;
     col.green = ((rgb>>8)&0xFF)*257;
     col.blue = ((rgb)&0xFF)*257;
     XAllocColor(gLCDHandle->d, DefaultColormap(gLCDHandle->d,0), &col);
    
    return col;
}

/**
 * \brief   Draw a color rectangle frame with (x1,y1) as top-left coordinate and (x2,y2) as the bottom-right coordinate.
 * \param   int x1 : X-coordinate of top-left pixel
 * \param   int y1 : Y-coordinate of top-left pixel
 * \param   int x2 : X-coordinate of bottom-right pixel
 * \param   int y2 : Y-coordinate of bottom-right pixel
 * \param   XColor color : RGB fill color value
 * \return  int retVal : always 0
 */
int LCDFrame(int x1, int y1, int x2, int y2, XColor color)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
    XSetForeground(gLCDHandle->d, gLCDHandle->gc, color.pixel);
    XDrawRectangle(gLCDHandle->d, gLCDHandle->w, gLCDHandle->gc, x1, y1, x2-x1, y2-y1);
    return 0;
}

/**
 * \brief   Draw the menu
 * \return  int retVal : always 0
 */
int LCDDrawMenu(void)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
	int i;
	if (gLCDHandle->mode&LCD_SHOWMENU)
	{
        LCDSetFontRaw(FONTDEFAULT);
		int buttonWidth = gLCDHandle->width/4;
    
		int lastRow = gLCDHandle->height/gLCDHandle->fontHeight-2;

		int columnWidth = gLCDHandle->width/gLCDHandle->fontWidth/4;
        
		XColor lastFgTextColor = gLCDHandle->fgTextColor;
		XColor lastBgTextColor = gLCDHandle->bgTextColor;
		char lastColorflag = gLCDHandle->colorflag;
		char* token;
		char* copy;
		int textLen;
		int row;
		int cur_pos_x = gCurPosX;
		int cur_pos_y = gCurPosY;
        
		for (i = 0; i < 4; i++)
		{
			row = lastRow;
			LCDSetColorRaw(gLCDHandle->menuItems[i].fgcol, gLCDHandle->menuItems[i].bgcol, LCD_BGCOL_TRANSPARENT);
            LCDAreaRaw(i*buttonWidth+1, gLCDHandle->height-gLCDHandle->fontHeight*2.5+1, (i+1)*buttonWidth, gLCDHandle->height, gLCDHandle->menuItems[i].bgcol);
            LCDFrame(i*buttonWidth, gLCDHandle->height-gLCDHandle->fontHeight*2.5, (i+1)*buttonWidth-1, gLCDHandle->height-1, LCD_BLACK);
			copy = strdup(gLCDHandle->menuItems[i].label);
			textLen = strlen(gLCDHandle->menuItems[i].label);
			while ((token = strsep(&copy, "\n")) != NULL)
			{
				LCDSetPrintf(row,i*columnWidth+2, token);
				textLen -= strlen(token);
				if (textLen != 0)
				{
					row++;
					textLen -= 1;
				}
			}
		}
        
		LCDSetColorRaw(lastFgTextColor, lastBgTextColor, lastColorflag);
		gCurPosX = cur_pos_x;
		gCurPosY = cur_pos_y;
	}
    LCDSetFontRaw(FONTDEFAULT);
	return 0;
}

/**
 * \brief   Draw the list
 * \return  int retVal : always 0
 */
int LCDDrawList(void)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
	int offset;
	int itemIndex;
    
	EYEFONT eyeFont;
	strcpy(eyeFont.fontName,FONTDEFAULT);
	eyeFont.fontColour = LCD_BLACK;
    
	if (gLCDHandle->listMenu != NULL && (gLCDHandle->mode&LCD_LISTMENU))
	{
		if (gLCDHandle->listMenu->size != 0)
		{
			int listMaxNumber = 0;
			int i;
			int menuHeight = gLCDHandle->fontHeight*2;
			XColor lastFgTextColor = gLCDHandle->fgTextColor;
			XColor lastBgTextColor = gLCDHandle->bgTextColor;
			char lastColorflag = gLCDHandle->colorflag;
			LIST_MENU*  menulist = gLCDHandle->listMenu;
            
            LCDAreaRaw(0+1, 0+1,  gLCDHandle->width, menuHeight, menulist->bgcol);
            LCDFrame(0, 0, gLCDHandle->width-1, menuHeight-1, LCD_BLACK);
            
			LCDSetColorRaw(menulist->fgcol, menulist->bgcol, LCD_BGCOL_TRANSPARENT);
			LCDSetPrintf(0, 0, menulist->title);
			if (menulist->scroll == 0)
			{
				offset = 0;
				listMaxNumber = gLCDHandle->listMenu->size;
			}
            
			else
			{
                LCDAreaRaw(0+1, menuHeight+1,  gLCDHandle->width, 2*menuHeight, LCD_RED);
                LCDFrame(0, menuHeight, gLCDHandle->width-1, 2*menuHeight-1, LCD_BLACK);
                
				LCDSetPrintf(2, 1, "SCROLL UP");
                LCDAreaRaw(0+1, (LCD_MAX_LIST_ITEM)*menuHeight+1,  gLCDHandle->width, (LCD_MAX_LIST_ITEM+1)*menuHeight, LCD_RED);
                LCDFrame(0, (LCD_MAX_LIST_ITEM)*menuHeight, gLCDHandle->width-1, (LCD_MAX_LIST_ITEM+1)*menuHeight-1, LCD_BLACK);
                
				LCDSetPrintf(2*(LCD_MAX_LIST_ITEM), 1, "SCROLL DOWN");
				offset = 1;
				listMaxNumber = LCD_MAX_LIST_ITEM;
			}
            //drawing the list
			for (i = offset; i < listMaxNumber-offset; i++)
			{
				itemIndex = menulist->start+i-offset;
				if (itemIndex < menulist->size && itemIndex >= 0)
				{
                    LCDAreaRaw(0+1, (i+1)*menuHeight+1,  gLCDHandle->width, (i+2)*menuHeight, menulist->pitems[itemIndex].bgcol);
                    LCDFrame(0, (i+1)*menuHeight, gLCDHandle->width-1, (i+2)*menuHeight-1, LCD_BLACK);
                    
					LCDSetColorRaw(menulist->pitems[itemIndex].fgcol, menulist->pitems[itemIndex].bgcol, LCD_BGCOL_TRANSPARENT);
					LCDSetPrintf(2*(i+1), 2, menulist->pitems[itemIndex].label);
					if (i-offset == menulist->index)
					{
						LCDSetPrintfFont(2*(i+1), 0,eyeFont, "> ");
						LCDSetFontRaw(FONTDEFAULT);
					}
				}
				else
				{
                    LCDAreaRaw(0+1, (i+1)*menuHeight+1,  gLCDHandle->width, (i+2)*menuHeight, menulist->pitems[menulist->size-1].bgcol);
                    LCDFrame(0, (i+1)*menuHeight, gLCDHandle->width-1, (i+2)*menuHeight-1, LCD_BLACK);
				}
			}
			LCDSetColorRaw(lastFgTextColor, lastBgTextColor, lastColorflag);
			KEYSetTM(KEYTM_LISTMENU);
		}
	}
	return 0;
}

/**
 * \brief   Initialize the LCD
 * \return  int retVal : always 0, if already initilialised return 1
 */
int LCDInit()
{
    if(LCD_INITIAL == 1) {
        return 1;
    }
    
    IM_TYPE = QQVGA;
    IM_LEFT = 0;
    IM_TOP = 0;
    IM_WIDTH = QQVGA_X;
    IM_LENGTH = QQVGA_Y;
    
    //loading extra fonts
    system("xset fp+ /usr/local/share/fonts/X11/100dpi/");
    system("xset fp rehash");
    
	int s,lcdNum, width, height;
	gLCDHandle = malloc(sizeof(LCD_HANDLE));
	Display *d;
	Screen *scr;
	Window w;
	HINTS hints;
	Atom property;

	hints.flags = 2;
	hints.decorations = 0;
    
    /*
    //Enable multi-thread support
    if (XInitThreads() == 0)
    {
        fprintf(stderr,"\nERROR: cannot enable multi-thread support\n");
        exit(EXIT_FAILURE);
    }
*/
	/* Open the connetion with the server */
	d = XOpenDisplay(NULL);
	if (d == NULL) {
		fprintf(stderr, "Cannot open the display\n");
		gLCDEnabled = false;
		return -1;
	}
	
	s = DefaultScreen(d);
	scr = DefaultScreenOfDisplay(d);
	lcdNum = s;	
    
    LCDGetSize(&width, &height);
    
    int LCDMODE = 0;
    
    FILE *fp;
    fp = fopen("/home/pi/eyebot/bin/hdt.txt","r");
    if (fp == NULL)
    {
        fprintf(stderr, "No HDT file present");
        exit(EXIT_FAILURE);
    }
    
    char* line = malloc(300*sizeof(char));
    char* buffer = malloc(100*sizeof(char));
    ssize_t read;
    size_t len = 0;
    
    while((read = getline(&line, &len, fp)) != -1) {
        if(strstr(line,"LCD")==line) {
            buffer = strtok(line, " \n"); //LCD
            buffer = strtok(NULL, " \n"); //HDMI/LCD
            buffer = strtok(NULL, " \n"); //Fullscreen
            LCDMODE = atoi(buffer);
        }
        if(strstr(line,"RPI")==line) {
            buffer = strtok(line, " \n"); //RPI
            buffer = strtok(NULL, " \n"); //Ver
            RPi_ver = atoi(buffer);
        }
    }
    
    int px, py, dx, dy;
    LCDGetDimensions(&px, &py, &dx, &dy);
    
    if(LCDMODE == 1) {
        width = px;
        height = py;
    }
    
    if(RPi_ver == 1) {
        strcpy(FONTDEFAULT, "6x13");
        strcpy(FONTBOLD, "6x13bold");
        strcpy(MENUFONT, "6x13bold");
    }
    else {
        strcpy(FONTDEFAULT, "7x14");
        strcpy(FONTBOLD, "7x14bold");
        strcpy(MENUFONT, "7x14bold");
    }

	/* Create the window */
	w = XCreateSimpleWindow(d, RootWindow(d, s), 0, 0, width, height, 1, BlackPixel(d, s), WhitePixel(d, s));
    
    XStoreName(d, w, "EyeBot7");
    
    //This here removes the title bar!!
    if(px<500) {
        property = XInternAtom(d, "_MOTIF_WM_HINTS", True);
        XChangeProperty(d, w, property, property, 32, PropModeReplace, (unsigned char *)&hints, 5);
    }
    
	GC gc = XCreateGC(d, w, 0, 0);
	Colormap colormap = XDefaultColormap(d, s);
    
    XFontStruct* fontstruct;

    fontstruct = XLoadQueryFont(d, FONTDEFAULT);
    
	Atom delWindow = XInternAtom(d, "WM_DELETE_WINDOW", 0);
	XSetWMProtocols(d, w, &delWindow, 1);

	/* Choose the interesting events */
	XSelectInput(d, w, /*ExposureMask | KeyPressMask |*/ KeyReleaseMask | ButtonPressMask | FocusChangeMask);

	/* Show the window */
	XMapWindow(d, w);
    
    XMoveWindow(d, w, 0, 0);

	gLCDHandle->d = d;
	gLCDHandle->s = s;
	gLCDHandle->lcdNum = lcdNum;
	gLCDHandle->w = w;
	gLCDHandle->colormap = colormap;
	gLCDHandle->gc = gc;
    
    gLCDHandle->fontType = HELVETICA;
    gLCDHandle->fontVariation = NORMAL;
    gLCDHandle->fontSize = 10;
    
	gLCDHandle->fontstruct = fontstruct;
	gLCDHandle->fontHeight = fontstruct->max_bounds.ascent+fontstruct->max_bounds.descent;
	gLCDHandle->fontWidth = XTextWidth(fontstruct, "a", 1);
	gLCDHandle->height = height;
	gLCDHandle->width = width;
	gLCDHandle->bgTextColor = LCD_BLACK;
	gLCDHandle->fgTextColor = LCD_WHITE;
	gLCDHandle->startCurPosX = 10;
	gLCDHandle->startCurPosY = gLCDHandle->fontHeight/2-2+gLCDHandle->fontHeight;
	gLCDHandle->fd = ConnectionNumber(d);
	gLCDHandle->mode = LCD_CLASSICMENU;
	gLCDHandle->X11Error = false;
	gCurPosX = gLCDHandle->startCurPosX;
	gCurPosY = gLCDHandle->startCurPosY;
	gLCDHandle->colorflag = 0x00;
	gLCDHandle->scr = scr;
    
	//set the colour of the button text and the background colour
	//button 1
	gLCDHandle->menuItems[0].fgcol = LCD_BLACK;
	gLCDHandle->menuItems[0].bgcol = LCD_RED;
	//button 2
	gLCDHandle->menuItems[1].fgcol = LCD_BLACK;
	gLCDHandle->menuItems[1].bgcol = LCD_GREEN;
	//button 3
	gLCDHandle->menuItems[2].fgcol = LCD_BLACK;
	gLCDHandle->menuItems[2].bgcol = LCD_YELLOW;
	//button 4
	gLCDHandle->menuItems[3].fgcol = LCD_BLACK;
	gLCDHandle->menuItems[3].bgcol = LCD_BLUE;

	gLCDHandle->listMenu = NULL;
	gLCDEnabled = true;
	XSetFont(gLCDHandle->d,gLCDHandle->gc,gLCDHandle->fontstruct->fid);
    
    LCD_INITIAL = 1;
    usleep(100000);
    LCDClear();
	return 0;
}

/**
 * \brief   Clear the LCD display and all display buffers ie. turn it black
 * \return  int retVal : always 0
 */
int LCDClear(void)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
        LCD_INITIAL = 1;
    }
    
	LCDAreaRaw(0, 0, gLCDHandle->width, gLCDHandle->height, LCD_BLACK);
	gCurPosX = gLCDHandle->startCurPosX;
	gCurPosY = gLCDHandle->startCurPosY;
	LCDSetColorRaw(LCD_WHITE, LCD_BLACK, LCD_BGCOL_TRANSPARENT);
	return 0;
}

/**
 * \brief   Update the internal mode flag bits.
 * \param	int mode : LCD Mode flag
 * \return  int retVal : always 0
 */
int LCDSetMode(int mode)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
        LCD_INITIAL = 1;
    }
    
	if (mode & LCD_AUTOREFRESH)	gLCDHandle->mode |= LCD_AUTOREFRESH;
	if (mode & LCD_SCROLLING)	gLCDHandle->mode |= LCD_SCROLLING;
	if (mode & LCD_LINEFEED) gLCDHandle->mode |= LCD_LINEFEED;
	if (mode & LCD_SHOWMENU) gLCDHandle->mode |= LCD_SHOWMENU;
	if (mode & LCD_FB_ROTATE) gLCDHandle->mode |= LCD_FB_ROTATE;
	if (mode & LCD_LISTMENU) gLCDHandle->mode |= LCD_LISTMENU;

	if (mode & LCD_NOAUTOREFRESH) gLCDHandle->mode &= ~(LCD_AUTOREFRESH);
	if (mode & LCD_NOSCROLLING) gLCDHandle->mode &= ~(LCD_SCROLLING);
	if (mode & LCD_NOLINEFEED) gLCDHandle->mode &= ~(LCD_LINEFEED);
	if (mode & LCD_HIDEMENU)
	{
		gLCDHandle->mode &= ~(LCD_SHOWMENU);
		gLCDHandle->mode &= ~(LCD_LISTMENU);
	}
	if (mode & LCD_FB_NOROTATION) gLCDHandle->mode &= ~(LCD_FB_ROTATE);
	if (mode & LCD_CLASSICMENU) gLCDHandle->mode &= ~(LCD_LISTMENU);
	return 0;
}

/**
 * \brief   Get the internal mode flag bits.
 * \return  int mode : Current mode flag bits
 */
int LCDGetMode(void)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
        LCD_INITIAL = 1;
    }
    
	return gLCDHandle->mode;
}

/**
 * \brief   Reset the internal mode flag bits to a previously saved mode.
 * \param   int mode : Mode flag - bit XORed
 * \return  int retVal : always 0
 */
int LCDResetMode(int mode)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
        LCD_INITIAL = 1;
    }
    
	gLCDHandle->mode = mode & 0xFF;
	return 0;
}

/**
 * \brief   Set menu entries in KEY_CLASSIC mode (4-buttons).
 * Also sets the LCD_SHOWMENU flag and refresh the LCD.
 * \param   char* string1 :  Menu entry for KEY1 in classic mode
 * \param   char* string2 :  Menu entry for KEY2 in classic mode
 * \param   char* string3 :  Menu entry for KEY3 in classic mode
 * \param   char* string4 :  Menu entry for KEY4 in classic mode
 * \return  int retVal : always 0
 */
int LCDMenu(char *string1, char *string2, char *string3, char *string4)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
        LCD_INITIAL = 1;
    }
    
	strcpy(gLCDHandle->menuItems[0].label, string1);
	strcpy(gLCDHandle->menuItems[1].label, string2);
	strcpy(gLCDHandle->menuItems[2].label, string3);
	strcpy(gLCDHandle->menuItems[3].label, string4);
	LCDSetMode(LCD_SHOWMENU);
	LCDDrawMenu();
    LCDRefresh3();
	return 0;
}

/**
 * \brief   Set specific menu entry in KEY_CLASSIC mode (index given by pos).
 * Color customization for specific key is now possible (fgcol/bgcol).
 * A user-specific data can be linked to the menu using userp pointer.
 * Will also set the LCD_SHOWMENU flag and refresh the LCD.
 * \param   int pos : Select menu entry in classic mode
 * \param   char* string : Menu entry for the key at specified index
 * \param   XColor fgcol : Textcolor for the menu
 * \param   XColor bgcol : Background color for the menu
 * \param   void* userp : A general purpose pointer for user-specific data
 * \return  int retVal : always 0
 */
int LCDMenuIRaw(int pos, char *string, XColor fgcol, XColor bgcol, void* userp)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
        LCD_INITIAL = 1;
    }
    
	strcpy(gLCDHandle->menuItems[pos].label, string);
	gLCDHandle->menuItems[pos].fgcol = fgcol;
	gLCDHandle->menuItems[pos].bgcol = bgcol;
	LCDSetMode(LCD_SHOWMENU);
	LCDDrawMenu();
	return 0;
}

/**
 * \brief   A wrapper for LCDMenuIRaw
 * \param   int pos : Select menu entry in classic mode (from 1->4)
 * \param   char* string : Menu entry for the key at specified index
 * \param   XColor fgcol : Textcolor for the menu
 * \param   XColor bgcol : Background color for the menu
 * \param   void* userp : A general purpose pointer for user-specific data
 * \return  int retVal : always 0
 */
int LCDMenuI(int pos, char *string, COLOR fg, COLOR bg) {
    
    if(LCD_INITIAL!=1) {
        LCDInit();
        LCD_INITIAL = 1;
    }
    
    XColor rawfg = LCDRGBtoXCol(fg);
    
    XColor rawbg = LCDRGBtoXCol(bg);
    
    LCDMenuIRaw(pos-1, string,rawfg,rawbg, NULL);
    
    LCDRefresh3();
    return 0;
}

/**
 * \brief	Return the menuitem at a given position
 * \param   int index : position of the menuitem (from 1->4)
 * \return  MENU_ITEM* menuItem
 */
MENU_ITEM* LCDMenuItem(int index)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
	return &(gLCDHandle->menuItems[index-1]);
}

/**
 * \brief   Setup the list menu display and update appropriate info in the LIST_MENU structure pointed by menulist (e.g. scroll, count).
 * Will also set the LCD_LISTMENU flag and refresh the LCD.
 * \param   LIST_MENU* menulist : Listmenu to be used for display
 * \return  int retVal : always 0
 */
int LCDList(LIST_MENU *menulist)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
	gLCDHandle->listMenu = menulist;
	LCDSetMode(LCD_LISTMENU);
	LCDDrawList();
	return 0;
}

/**
 * \brief   Unlike LCDList(), this will blindly assign menulist to the mainlist for display.
 * Doesn't update anything in the menulist structure, nor modify any internal flags.
 * Useful to maintain multiple lists for menu display.
 * \param   LIST_MENU* menulist : Listmenu to be used for display
 * \return  int retVal : always 0
 */
int LCDSetList(LIST_MENU* menulist)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
	gLCDHandle->listMenu = menulist;
	LCDSetMode(LCD_LISTMENU);
	LCDDrawList();
	return 0;
}

/**
 * \brief   Get the currently active list menu.
 * \return  LIST_MENU* retListMenu : Pointer to LIST_MENU structure
 */
LIST_MENU* LCDGetList(void)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
	return gLCDHandle->listMenu;
}

/**
 * \brief   Get the frame info of a specific list item in form of a RECT structure.
 * \param   int pos : Index of list item
 * \return  RECT* retMenuRect : Pointer to a RECT structure
 */
RECT* LCDListBox(int pos)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
	RECT* menuRect = malloc(sizeof(RECT));
	menuRect->x = 0;
	menuRect->y = (1+pos)*2*gLCDHandle->fontHeight;
	menuRect->height = 2*gLCDHandle->fontHeight;
	menuRect->width = gLCDHandle->width;
	return menuRect;
}

/**
 * \brief   Get the selected menuitem in the list menu – using index & start variable in LIST_MENU.
 * Will return  0x0 (NUL) if no item is currently selected.
 * \return  MENU_ITEM* retMenuItem : Pointer to a MENU_ITEM structure
 */
MENU_ITEM* LCDListActiveItem(void)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
	int index = gLCDHandle->listMenu->index;
	return &(gLCDHandle->listMenu->pitems[index]);
}

/**
 * \brief   Draw a color-filled rectangle with (x1,y1) as top-left coordinate and (x2,y2) as the bottom-right coordinate.
 * \param   int x1 : X-coordinate of top-left pixel
 * \param   int y1 : Y-coordinate of top-left pixel
 * \param   int x2 : X-coordinate of bottom-right pixel
 * \param   int y2 : Y-coordinate of bottom-right pixel
 * \param   XColor color : RGB fill color value
 * \return  int retVal : always 0
 */
int LCDAreaRaw(int x1, int y1, int x2, int y2, XColor color)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
	XSetForeground(gLCDHandle->d, gLCDHandle->gc, color.pixel);
	XFillRectangle(gLCDHandle->d, gLCDHandle->w, gLCDHandle->gc, x1, y1, x2-x1, y2-y1);
	return 0;
}

/**
 * \brief   A wrapper for LCDAreaRaw
 * \param   int x1 : X-coordinate of top-left pixel
 * \param   int y1 : Y-coordinate of top-left pixel
 * \param   int x2 : X-coordinate of bottom-right pixel
 * \param   int y2 : Y-coordinate of bottom-right pixel
 * \param   XColor color : RGB fill color value
 * \return  int retVal : always 0
 */
int LCDArea(int x1, int y1, int x2, int y2, COLOR col, int fill) {
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
    if(fill == 1) {
        XColor rawCol = LCDRGBtoXCol(col);
        LCDAreaRaw(x1,y1,x2,y2,rawCol);
    }
    else if (fill == 0) {
        LCDLine(x1,y1,x1,y2,col);
        LCDLine(x1,y2,x2,y2,col);
        LCDLine(x2,y2,x2,y1,col);
        LCDLine(x2,y1,x1,y1,col);
    }
    LCDRefresh3();
    return 0;
}

/**
 * \brief   Set the default color for text (including background) and related flags (e.g. for transparent background).
 * \param   XColor fgcol : Default color for text
 * \param   XColor bgcol : Default color for text background
 * \param   char colorflags : Mode flag for text color
 * \details Valid value for colorflags :
 * - LCD_BGCOL_TRANSPARENT
 * - LCD_BGCOL_INVERSE
 * - LCD_FGCOL_INVERSE
 * - LCD_BGCOL_NOTRANSPARE
 * - LCD_BGCOL_NOINVERSE
 * - LCD_FGCOL_NOINVERSE
 *
 * \return  int retVal : always 0
 */
int LCDSetColorRaw(XColor fgcol, XColor bgcol, char colorflags)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
        LCD_INITIAL = 1;
    }
    
	gLCDHandle->fgTextColor = fgcol;
	gLCDHandle->bgTextColor = bgcol;
	gLCDHandle->colorflag = colorflags;
	return 0;
}

/**
 * \brief   A wrapper for LCDSetColorRaw
 * \param   fg : Default color for text
 * \param   bg : Default color for text background
 * \return  0 always
 */
int LCDSetColor(COLOR fg, COLOR bg) {
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
    XColor rawfg = LCDRGBtoXCol(fg);
    XColor rawbg = LCDRGBtoXCol(bg);
    
    LCDSetColorRaw(rawfg, rawbg, LCD_BGCOL_TRANSPARENT);
    return 0;
}

/**
 * \brief   Sets the font using alias in Xlib
 * \param   fontAlias the alias for the font
 * \return  0 always
 */
int LCDSetFontRaw(char *fontAlias)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
    XFreeFont(gLCDHandle->d,gLCDHandle->fontstruct);
    
    gLCDHandle->fontstruct = XLoadQueryFont(gLCDHandle->d, fontAlias);
    gLCDHandle->fontHeight = gLCDHandle->fontstruct->max_bounds.ascent+gLCDHandle->fontstruct->max_bounds.descent;
    gLCDHandle->fontWidth = XTextWidth(gLCDHandle->fontstruct, "a", 1);
    XSetFont(gLCDHandle->d,gLCDHandle->gc,gLCDHandle->fontstruct->fid);
    return 0;
}


/**
 * \brief   Sets the font using fontName and variation
 * \param   int fontNmae the type of font
 * \param   int variation the variation (NORMAL/BOLD)
 * \return  0 always
 */
int LCDSetFont(int fontName, int variation) {
    
    if(LCD_INITIAL!=1) {
        LCDInit();
        LCD_INITIAL = 1;
    }
    
    if ((gLCDHandle->fontSize==8)||(gLCDHandle->fontSize==10)||(gLCDHandle->fontSize==12)||(gLCDHandle->fontSize==14)||(gLCDHandle->fontSize==18)||(gLCDHandle->fontSize==24)) {
    }
    else {
        fprintf(stderr, "invalid font size, please select from : 8, 10, 12, 14, 18, 24\n");
        return 1;
    }
    
    char fontString[200] = "";
    
    switch(fontName) {
        case HELVETICA:
            if(variation == BOLD) {
                sprintf(fontString, "-*-helvetica-bold-r-*-*-*-%d-100-100-*-*-*", gLCDHandle->fontSize*10);
            }
            else {
                sprintf(fontString, "-*-helvetica-medium-r-*-*-*-%d-100-100-*-*-*", gLCDHandle->fontSize*10);
            }
            break;
        case TIMES:
            if(variation == BOLD) {
                sprintf(fontString, "-*-times-bold-r-*-*-*-%d-100-100-*-*-*", gLCDHandle->fontSize*10);
            }
            else {
                sprintf(fontString, "-*-times-medium-r-*-*-*-%d-100-100-*-*-*", gLCDHandle->fontSize*10);
            }
            break;
        case COURIER:
            if(variation == BOLD) {
                sprintf(fontString, "-*-courier-*-r-*-*-*-%d-100-100-*-*-*", gLCDHandle->fontSize*10);
            }
            else {
                sprintf(fontString, "-*-courier-bold-r-*-*-*-%d-100-100-*-*-*", gLCDHandle->fontSize*10);
            }
            break;
        default:
            fprintf(stderr, "invalid fontType received %d\n", fontName);
            return 1;
    }
    
    char** fontsFound;
    int count;
    
    fontsFound = XListFonts(gLCDHandle->d, fontString, 100, &count);
    
    for(int i =0; i<count; i++) {
        printf("%s\n", fontsFound[i]);
    }
    
    gLCDHandle->fontType = fontName;
    gLCDHandle->fontVariation = variation;
    
    XFreeFont(gLCDHandle->d, gLCDHandle->fontstruct);
    gLCDHandle->fontstruct = XLoadQueryFont(gLCDHandle->d, fontsFound[0]);
    
    gLCDHandle->fontHeight = gLCDHandle->fontstruct->max_bounds.ascent+gLCDHandle->fontstruct->max_bounds.descent;
    gLCDHandle->fontWidth = XTextWidth(gLCDHandle->fontstruct, "a", 1);
    
    XSetFont(gLCDHandle->d,gLCDHandle->gc,gLCDHandle->fontstruct->fid);
    return 0;
}

/**
 * \brief   Sets the font size
 * \param   int fontsize the size of font
 * \return  0 aon success, 1 for invalid fontsize
 */
int LCDSetFontSize(int fontsize) {
    if(LCD_INITIAL!=1) {
        LCDInit();
        LCD_INITIAL = 1;
    }
    
    if ((fontsize==8)||(fontsize==10)||(fontsize==12)||(fontsize==14)||(fontsize==18)||(fontsize==24)) {
    }
    else {
        fprintf(stderr, "invalid font size, please select from : 8, 10, 12, 14, 18, 24\n");
        return 1;
    }
    
    gLCDHandle->fontSize = fontsize;
    LCDSetFont(gLCDHandle->fontType, gLCDHandle->fontVariation);
    
    return 0;
}

/**
 * \brief   Print formatted string to LCD and refresh LCD. Cursor position is updated.
 * \param   const char* format : Formatted string
 * \return  int retVal : always 0
 */
int LCDPrintfFont(EYEFONT eyeFont,const char *format, ...)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
	char* text;
	va_list args;
	va_start(args, format);
    
	vasprintf(&text, format, args);
    
	char* token;
	char* copy = strdup(text);
    
	int textWidth;
	int textLen = strlen(text);

    LCDSetFontRaw(eyeFont.fontName);
    
	XSetForeground(gLCDHandle->d, gLCDHandle->gc, eyeFont.fontColour.pixel);
	XSetBackground(gLCDHandle->d, gLCDHandle->gc, gLCDHandle->bgTextColor.pixel);
    
	while ((token = strsep(&copy, "\n")) != NULL)
	{
		textWidth = XTextWidth(gLCDHandle->fontstruct, token, strlen(token));
		if (gCurPosX+textWidth > gLCDHandle->width)
		{
			gCurPosX = gLCDHandle->startCurPosX;
			gCurPosY += gLCDHandle->fontHeight;
		}
		if (gLCDHandle->colorflag == LCD_BGCOL_TRANSPARENT)
		{
			XSetBackground(gLCDHandle->d, gLCDHandle->gc, LCDGetPixelRaw(gCurPosX, gCurPosY).pixel);
			XDrawImageString(gLCDHandle->d, gLCDHandle->w, gLCDHandle->gc, gCurPosX, gCurPosY, token, strlen(token));
			XSetBackground(gLCDHandle->d, gLCDHandle->gc, gLCDHandle->bgTextColor.pixel);
		}
		else
		{
			XDrawImageString(gLCDHandle->d, gLCDHandle->w, gLCDHandle->gc, gCurPosX, gCurPosY, token, strlen(token));
		}
		textLen -= strlen(token);
		if (textLen != 0)
		{
			gCurPosX = gLCDHandle->startCurPosX;
			gCurPosY += gLCDHandle->fontHeight;
			textLen -= 1;
		}
		else
		{
			gCurPosX += textWidth;
		}
	}
	free(text);
	va_end(args);
    LCDRefresh3();
	return 0;
}

/**
 * \brief   Print formatted string to LCD and refresh LCD. Cursor position is updated.
 * \param   const char* format : Formatted string
 * \return  int retVal : always 0
 */
int LCDPrintf(const char *format, ...)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
	va_list args;
	va_start(args,format);
    
    if(gLCDHandle == NULL) {
        vfprintf(stdout, format, args);
        va_end(args);
        return 0;
    }
    
    char *text;
	vasprintf(&text, format, args);
    
    char* token;
    char* copy = strdup(text);
    
    int textWidth;
    int textLen = strlen(text);
    
    XSetForeground(gLCDHandle->d, gLCDHandle->gc, gLCDHandle->fgTextColor.pixel);
    XSetBackground(gLCDHandle->d, gLCDHandle->gc, gLCDHandle->bgTextColor.pixel);
    
    while ((token = strsep(&copy, "\n")) != NULL)
    {
        textWidth = XTextWidth(gLCDHandle->fontstruct, token, strlen(token));
        if (gCurPosX+textWidth > gLCDHandle->width)
        {
            gCurPosX = gLCDHandle->startCurPosX;
            gCurPosY += gLCDHandle->fontHeight;
        }
        if (gLCDHandle->colorflag == LCD_BGCOL_TRANSPARENT)
        {
            XSetBackground(gLCDHandle->d, gLCDHandle->gc, LCDGetPixelRaw(gCurPosX, gCurPosY).pixel);
            XDrawImageString(gLCDHandle->d, gLCDHandle->w, gLCDHandle->gc, gCurPosX, gCurPosY, token, strlen(token));
            XSetBackground(gLCDHandle->d, gLCDHandle->gc, gLCDHandle->bgTextColor.pixel);
        }
        else
        {
            XDrawImageString(gLCDHandle->d, gLCDHandle->w, gLCDHandle->gc, gCurPosX, gCurPosY, token, strlen(token));
        }
        textLen -= strlen(token);
        if (textLen != 0)
        {
            gCurPosX = gLCDHandle->startCurPosX;
            gCurPosY += gLCDHandle->fontHeight;
            textLen -= 1;
        }
        else
        {
            gCurPosX += textWidth;
        }
    }
    free(text);
    va_end(args);
    LCDRefresh3();
    return 0;
}

/**
 * \brief   LCDPrintf with text position specified.
 * \param   int row : Cursor position
 * \param   int column : Cursor position
 * \param   const char* format : Formatted string
 * \return  int retVal : always 0
 */
int LCDSetPrintf(int row, int column, const char *format, ...)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
        LCD_INITIAL = 1;
    }
    
	char* text;
	va_list args;
	va_start(args, format);
	int temp_cur_pos_x = gLCDHandle->startCurPosX+column*gLCDHandle->fontWidth;
	int temp_cur_pos_y = gLCDHandle->startCurPosY+row*gLCDHandle->fontHeight;
	vasprintf(&text, format, args);
	char* token;
	char* copy = strdup(text);
	int textWidth;
	int textLen = strlen(text);
	XSetForeground(gLCDHandle->d, gLCDHandle->gc, gLCDHandle->fgTextColor.pixel);
	XSetBackground(gLCDHandle->d, gLCDHandle->gc, gLCDHandle->bgTextColor.pixel);
	while ((token = strsep(&copy, "\n")) != NULL)
	{
		textWidth = XTextWidth(gLCDHandle->fontstruct, token, strlen(token));
		if (temp_cur_pos_x+textWidth > gLCDHandle->width)
		{
			temp_cur_pos_x = gLCDHandle->startCurPosX;
			temp_cur_pos_y += gLCDHandle->fontHeight;
		}
		if (gLCDHandle->colorflag == LCD_BGCOL_TRANSPARENT)
		{
			XSetBackground(gLCDHandle->d, gLCDHandle->gc, LCDGetPixelRaw(temp_cur_pos_x, temp_cur_pos_y).pixel);
			XDrawImageString(gLCDHandle->d, gLCDHandle->w, gLCDHandle->gc, temp_cur_pos_x, temp_cur_pos_y, token, strlen(token));
			XSetBackground(gLCDHandle->d, gLCDHandle->gc, gLCDHandle->bgTextColor.pixel);
		}
		else
		{
			XDrawImageString(gLCDHandle->d, gLCDHandle->w, gLCDHandle->gc, temp_cur_pos_x, temp_cur_pos_y, token, strlen(token));
		}
		textLen -= strlen(token);
		if (textLen != 0)
		{
			temp_cur_pos_x = gLCDHandle->startCurPosX;
			temp_cur_pos_y += gLCDHandle->fontHeight;
			textLen -= 1;
		}
		else
		{
			temp_cur_pos_x += textWidth;
		}
	}
	gCurPosX = temp_cur_pos_x;
	gCurPosY = temp_cur_pos_y;
	free(text);
	va_end(args);
    LCDRefresh3();
	return 0;
}

/**
 * \brief   LCDPrintf with text position and text colour specified.
 * \param   int row : Cursor position
 * \param   int column : Cursor position
 * \param	XColor colour : the colour of the text
 * \param   const char* format : Formatted string
 * \return  int retVal : always 0
 */
int LCDSetPrintfFont(int row, int column, EYEFONT eyeFont, const char *format, ...)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
        LCD_INITIAL = 1;
    }
    
	char* text;
	va_list args;
	va_start(args, format);
    
	LCDSetFontRaw(eyeFont.fontName);
    
	int temp_cur_pos_x = gLCDHandle->startCurPosX+column*gLCDHandle->fontWidth;
	int temp_cur_pos_y = gLCDHandle->startCurPosY+row*gLCDHandle->fontHeight;
	vasprintf(&text, format, args);
	char* token;
	char* copy = strdup(text);
	int textWidth;
	int textLen = strlen(text);
	XSetForeground(gLCDHandle->d, gLCDHandle->gc, eyeFont.fontColour.pixel);
	XSetBackground(gLCDHandle->d, gLCDHandle->gc, gLCDHandle->bgTextColor.pixel);
	while ((token = strsep(&copy, "\n")) != NULL)
	{
		textWidth = XTextWidth(gLCDHandle->fontstruct, token, strlen(token));
		if (temp_cur_pos_x+textWidth > gLCDHandle->width)
		{
			temp_cur_pos_x = gLCDHandle->startCurPosX;
			temp_cur_pos_y += gLCDHandle->fontHeight;
		}
		XDrawImageString(gLCDHandle->d, gLCDHandle->w, gLCDHandle->gc, temp_cur_pos_x, temp_cur_pos_y, token, strlen(token));
		textLen -= strlen(token);
		if (textLen != 0)
		{
			temp_cur_pos_x = gLCDHandle->startCurPosX;
			temp_cur_pos_y += gLCDHandle->fontHeight;
			textLen -= 1;
		}
		else
		{
			temp_cur_pos_x += textWidth;
		}
	}
	gCurPosX = temp_cur_pos_x;
	gCurPosY = temp_cur_pos_y;
	free(text);
	va_end(args);
    LCDRefresh3();
	return 0;
}

/**
 * \brief   Set the text cursor position to (row, column).
 * \param   int row : Text cursor row index
 * \param   int column : Text cursor column index
 * \return  int retVal : always 0
 */
int LCDSetPos(int row, int column)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
	gCurPosX = gLCDHandle->startCurPosX+column*gLCDHandle->fontWidth;
	gCurPosY = gLCDHandle->startCurPosY+row*gLCDHandle->fontHeight;
	return 0;
}

/**
 * \brief   Get the text cursor position.
 * \param   int* row : Pointer to cursor row index
 * \param   int* column : Pointer to cursor column index
 * \return  int retVal : always 0
 */
int LCDGetPos(int *row, int *column)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
	*column = (gCurPosX-gLCDHandle->startCurPosX)/gLCDHandle->fontWidth;
	*row = (gCurPosY-gLCDHandle->startCurPosY)/gLCDHandle->fontHeight;
	return 0;
}

/**
 * \brief   Sets the color of the pixel at (x,y) coordinate to color.
 * \param   int x : X-coordinate of the pixel
 * \param   int y : Y-coordinate of the pixel
 * \param   XColor color : color value for the pixel
 * \return  int retVal : always 0
 */
int LCDPixelRaw(int x, int y, XColor color)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
	XSetForeground(gLCDHandle->d, gLCDHandle->gc, color.pixel);
	XDrawPoint(gLCDHandle->d, gLCDHandle->w, gLCDHandle->gc, x, y);
	return 0;
}

/**
 * \brief   A wrapper for LCDSet Pixel
 * \param   int x : X-coordinate of the pixel
 * \param   int y : Y-coordinate of the pixel
 * \param   col : RGB color value for the pixel
 * \return  int retVal : always 0
 */
int LCDPixel(int x, int y, COLOR col) {
    if(LCD_INITIAL!=1) {
        LCDInit();
        LCD_INITIAL = 1;
    }
    
    XColor rawCol = LCDRGBtoXCol(col);
    LCDPixelRaw(x,y,rawCol);
    LCDRefresh3();
    return 0;
}

/**
 * \brief   Get the RGB color value of the pixel at (x,y) coordinate.
 * \param   int x : X-coordinate of the pixel
 * \param   int y : Y-coordinate of the pixel
 * \return  XColor color : RGB color value
 */
XColor LCDGetPixelRaw(int x, int y)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
  XImage *image;
  XColor color;
  XErrorHandler old_handler;
  old_handler = XSetErrorHandler(LCDAppErrHandler);
  image = XGetImage(gLCDHandle->d, gLCDHandle->w, x, y, 1, 1, AllPlanes, XYPixmap);
  if (gLCDHandle->X11Error == True)
  {
  	color = LCD_WHITE;
  	gLCDHandle->X11Error = False;
  }
  else
  {
  	color.pixel = XGetPixel(image, 0, 0);
  }
  XSetErrorHandler(old_handler);
  XFree(image);
  XQueryColor(gLCDHandle->d, gLCDHandle->colormap, &color);
  return color;
}

/**
 * \brief   Obtain the colour at a given pixel
 * \param   int x: The horizontal axis value
 * \param   int y: The vertical axis value
 * \return  COLOR color : RGB color value
 */
COLOR LCDGetPixel(int x, int y) {
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
    XColor color;
    color = LCDGetPixelRaw(x, y);
    
    int rgb = (((color.red/256)&0x0ff)<<16)|(((color.green/256)&0x0ff)<<8)|((color.blue/256)&0x0ff);
    return rgb;
}

/**
 * \brief   Invert a RGB color.
 * \param   XColor color : RGB color value
 * \return  XColor color : RGB color value
 */
XColor LCDPixelInvertRaw(XColor color)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
	XColor colorInverted;
	colorInverted.red = 0xFFFF - color.red;
	colorInverted.green = 0xFFFF - color.green;
	colorInverted.blue = 0xFFFF -color.blue;
	XAllocColor(gLCDHandle->d, gLCDHandle->colormap, &colorInverted);
	return colorInverted;
}

/**
 * \brief   Bit-invert the color of the pixel at (x,y) coordinate.
 * \param   int x : X-coordinate of the pixel
 * \param   int y : Y-coordinate of the pixel
 * \return  int retVal : always 0
 */
int LCDPixelInvert(int x, int y)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
	LCDPixelRaw(x, y, LCDPixelInvertRaw(LCDGetPixelRaw(x, y)));
    LCDRefresh3();
	return 0;
}

/**
 * \brief   Draws a circle
 * \param   int x1 : X-coordinate of the centre
 * \param   int y1 : Y-coordinate of the centre
 * \param   int size : radius of the circle
 * \param   COLOR col: the color of the circle
 * \param   int fill : fill(1) or no fill(0)
 * \return  int retVal : always 0
 */
int LCDCircle(int x1, int y1, int size, COLOR col, int fill) {
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
    XColor raw = LCDRGBtoXCol(col);
    
    XSetForeground(gLCDHandle->d, gLCDHandle->gc, raw.pixel);
    
    if(fill==1) {
        XFillArc(gLCDHandle->d, gLCDHandle->w, gLCDHandle->gc, x1-(size/2), y1-(size/2), size, size, 0, 360*64);
    }
    else {
        XDrawArc(gLCDHandle->d, gLCDHandle->w, gLCDHandle->gc, x1-(size/2), y1-(size/2), size, size, 0, 360*64);
    }
    
    LCDRefresh3();

}

/**
 * \brief   Inverts the pixels inside of the given circle
 * \param   int x1 : X-coordinate of the centre
 * \param   int y1 : Y-coordinate of the centre
 * \param   int r the radius of the circle
 * \return  int retVal : always 0
 */
int LCDCircleInvert(int x1, int y1, int r){
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
    int x, y, y_r;
    for (x = x1-r; x <= x1+r; x++)
    {
    	y_r = sqrt(r*r-(x1-x)*(x1-x));
        for (y = y1-y_r; y <= y1+y_r; y++)
        {
            LCDPixelInvert(x, y);
        }
    }
    LCDRefresh3();
    return 0;
}

/**
 * \brief   Draw a color line from (x1,y1) to (x2,y2).
 * \param   int x1 : X-coordinate of first pixel
 * \param   int y1 : Y-coordinate of first pixel
 * \param   int x2 : X-coordinate of second pixel
 * \param   int y2 : Y-coordinate of second pixel
 * \param   XColor color :  RGB color value for the pixel
 * \return  int retVal : always 0
 */
int LCDLineRaw(int x1, int y1, int x2, int y2, XColor color)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
	XSetForeground(gLCDHandle->d, gLCDHandle->gc, color.pixel);
	XDrawLine(gLCDHandle->d, gLCDHandle->w, gLCDHandle->gc, x1, y1, x2, y2);
	return 0;
}

/**
 * \brief   A wrapper for LCDLineRaw
 * \param   int x1 : X-coordinate of first pixel
 * \param   int y1 : Y-coordinate of first pixel
 * \param   int x2 : X-coordinate of second pixel
 * \param   int y2 : Y-coordinate of second pixel
 * \param   col :  RGB color value for the pixel
 * \return  int retVal : always 0
 */
int LCDLine(int x1, int y1, int x2, int y2, COLOR col) {
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
    XColor rawCol = LCDRGBtoXCol(col);
    
    LCDLineRaw(x1,y1,x2,y2,rawCol);
    
    LCDRefresh3();
    return 0;
}


/**
 * \brief   Draw a line from (x1,y1) to (x2,y2). The line pixels  will invert the color of existing pixels.
 * \param   int x1 : X-coordinate of first pixel
 * \param   int y1 : Y-coordinate of first pixel
 * \param   int x2 : X-coordinate of second pixel
 * \param   int y2 : Y-coordinate of second pixel
 * \return  int retVal : always 0
 */
int LCDLineInvert(int x1, int y1, int x2, int y2)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
	int x;
	float y;
	for (x = x1; x < x2; x++)
	{
		y = ((float)y1)+((float)(y2-y1))*((float)(x-x1))/((float)(x2-x1));
		LCDPixelInvert(x, (int)y);
	}
    LCDRefresh3();
	return 0;
}

/**
 * \brief   Draw a rectangle with (x1,y1) as top-left coordinate and (x2,y2) as the bottom-right coordinate.
 *          The pixels in the specified area will invert the color of existing pixels.
 * \param   int x1 : X-coordinate of top-left pixel
 * \param   int y1 : Y-coordinate of top-left pixel
 * \param   int x2 : X-coordinate of bottom-right pixel
 * \param   int y2 : Y-coordinate of bottom-right pixel
 * \return  int retVal : always 0
 */
int LCDAreaInvert(int x1, int y1, int x2, int y2)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
	int x, y;
	for (x = x1; x < x2; x++)
	{
		for (y = y1; y < y2; y++)
		{
			LCDPixelInvert(x, y);
		}
	}
	return 0;
}

/**
 * \brief   Draw a textbar for text starting at position (row, column) until (row, column+length).
 *          The textbar will take about 25%-50% of text height & width to draw its frame.
 *          The fill parameter will define how much of the text bar should be 'filled' with color (like a progress bar).
 * \param   int row : Start text cursor position
 * \param   int column : Start text cursor position
 * \param   int length : Text length of the bar
 * \param   int fill : Percentage of textbar to be filled
 * \param   XColor color : Fill color for the textbar
 * \return  RECT rect : RECT structure for the textbar's frame
 */
RECT LCDTextBar(int row, int column, int length, int fill, XColor color)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
	int old_cur_posX, old_cur_posY;
	old_cur_posX = gCurPosX;
	old_cur_posY = gCurPosY;
    
	RECT frame;
	frame.x = gLCDHandle->startCurPosX+column*gLCDHandle->fontWidth;
	frame.y = gLCDHandle->startCurPosY+(row-1.15)*gLCDHandle->fontHeight;
    
	frame.height = 1.5*gLCDHandle->fontHeight;
	frame.width = length*gLCDHandle->fontWidth;
    
	LCDAreaRaw(frame.x, frame.y, frame.x+frame.width*fill/100.0, frame.y+frame.height, color);
    
    
	XColor backupFgColor = gLCDHandle->fgTextColor;
	XColor backupBgColor = gLCDHandle->bgTextColor;
    
	char colorflag = gLCDHandle->colorflag;
    
	LCDSetColorRaw(LCDPixelInvertRaw(color), color, LCD_BGCOL_TRANSPARENT);
    
	LCDSetPrintf(row, column+length-5, "%3d%%", fill);
    
	LCDSetColorRaw(backupFgColor, backupBgColor, colorflag);
	gCurPosX = old_cur_posX;
	gCurPosY = old_cur_posY;
     
	return frame;
}

/**
 * \brief   Indicate if the LCD need to be refreshed
 * \return  int retVal : non-null value indicate that the LCD need to be refreshed
 */
int LCDNeedRefresh(void)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
	if (gLCDEnabled)
	{
		XEvent e;
		XNextEvent(gLCDHandle->d, &e);
		switch (e.type)
		{
			case ButtonPress:
				gMousePosX = e.xbutton.x;
				gMousePosY = e.xbutton.y;
				gMouseButton = e.xbutton.button;
				break;
			case Expose:
				gCurPosX = gLCDHandle->startCurPosX;
				gCurPosY = gLCDHandle->startCurPosY;
				gMouseButton = -1;
				LCDDrawMenu();
				return 1;
				break;
			case ClientMessage:
				LCDRelease(*gLCDHandle);
				exit(0);
				break;
		}
	}
	return 0;
}

/**
 * \brief   Release the LCD
 * \return  int retVal : always 0
 */
int LCDRelease()
{
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
	if (gLCDEnabled)
	{
		XFreeColormap(gLCDHandle->d, gLCDHandle->colormap);
		XDestroyWindow(gLCDHandle->d, gLCDHandle->w);
		/* Close the connexion with the server */
		XCloseDisplay(gLCDHandle->d);
		gLCDEnabled = false;
		free(gLCDHandle);
		gLCDHandle = NULL;
	}
	return 0;
}

/**
 * \brief  Refresh the screen (i.e write display buffers to the framebuffer device). Refreshes the menu
 * \return    int retVal : always 0
 */
int LCDRefresh(void)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
	LCDDrawMenu();
	LCDDrawList();
	while (XPending(gLCDHandle->d))
	{
		LCDNeedRefresh();
	}
	return 0;
}

/**
 * \brief	Refresh the screen (i.e write display buffers to the framebuffer device). Does not refresh the menu
 * \return    int retVal : always 0
 */
int LCDRefresh2(void)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
	LCDDrawList();
	//XEvent e;
	while (XPending(gLCDHandle->d))
	{
		LCDNeedRefresh();
	}
	return 0;
}

/**
 * \brief	Refresh the screen (i.e write display buffers to the framebuffer device). Does not refresh the menu or the listmenu
 * \return    int retVal : always 0
 */
int LCDRefresh3(void)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
	//XEvent e;
	while (XPending(gLCDHandle->d))
	{
		LCDNeedRefresh();
	}
	return 0;
}


/**
 * \brief			Get display information and save to structure pointed by pinfo.
 * Cursor info needs LCDInit() for textsize.
 * \param     FBINFO* pinfo : Pointer to storage for screen & cursor info
 * \return    int retVal\n
 *            0 on success\n
 *            Negative value on failure
 */
int LCDGetFBInfo(FBINFO* pinfo)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
	pinfo->screen.xres = gLCDHandle->width;
	pinfo->screen.yres = gLCDHandle->height;
	pinfo->screen.bpp = 0; //TODO: correct the value
	pinfo->cursor.x = gCurPosX;
	pinfo->cursor.xmax = (gLCDHandle->width-gLCDHandle->startCurPosX)/gLCDHandle->fontWidth-1;
	pinfo->cursor.y = gCurPosY;
	pinfo->cursor.ymax = (gLCDHandle->height-gLCDHandle->startCurPosY)/gLCDHandle->fontHeight-1;
	return 0;
}


/**
 * \brief			Get the number of list items supported by the current display (text) configuration.
 * This includes the item for title bar - thus, different from count variable in LIST_MENU as updated by an LCDList() call.
 * \return    int listCount : Number of list items (including title box)
 */

int LCDListCount(void)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
	return LCD_MAX_LIST_ITEM;
}

/**
 * \brief	  		Set the list index.
 * \param     int index : List index
 * \return    int retVal : List index
 */
int LCDListIndex(int index)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
	gLCDHandle->listMenu->index = index;
	return 0;
}

/**
 * \brief			Scrolls the list display up.
 * Menu index is not altered.
 * If the active menu item goes out of focus, the index becomes negative (no item selected).
 * \return    int retVal : always 0
 */
int LCDListScrollUp(void)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
	if (gLCDHandle->listMenu->scroll == 0)
	{
		return 0;
	}
	if (gLCDHandle->listMenu->start > 0)
	{
		gLCDHandle->listMenu->start--;
	}
	return 0;
}

/**
 * \brief     Scrolls the list display down.
 * Menu index is not altered.
 * If the active menu item goes out of focus, the index becomes negative (no item selected).
 * \return    int retVal : always 0
 */

int LCDListScrollDown(void)
{
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
	if (gLCDHandle->listMenu->scroll == 0)
	{
		return 0;
	}
    
    if (gLCDHandle->listMenu->start < gLCDHandle->listMenu->size - LCD_MAX_LIST_ITEM+2);
	{
		gLCDHandle->listMenu->start++;
	}
	return 0;
}


XImage* LCDCreateImage(Display *dis, int screen, u_char *buf, int width, int height) {
    int depth;
    XImage *img = NULL;
    Visual *vis;
    double rRatio;
    double gRatio;
    double bRatio;
    int outIndex = 0;
    int i;
    int numBufBytes = (3 * (width * height));
    
    depth = DefaultDepth (dis, screen);
    vis = DefaultVisual (dis, screen);
    
    rRatio = vis->red_mask / 255.0;
    gRatio = vis->green_mask / 255.0;
    bRatio = vis->blue_mask / 255.0;
    
    if (depth >= 24) {
        size_t numNewBufBytes = (4 * (width * height));
        u_int32_t *newBuf = malloc (numNewBufBytes);
        
        for (i = 0; i < numBufBytes; ++i) {
            unsigned int r, g, b;
            r = (buf[i] * rRatio);
            ++i;
            g = (buf[i] * gRatio);
            ++i;
            b = (buf[i] * bRatio);
            
            r &= vis->red_mask;
            g &= vis->green_mask;
            b &= vis->blue_mask;
            
            newBuf[outIndex] = r | g | b;
            ++outIndex;
        }
        
        img = XCreateImage (dis,
                            CopyFromParent, depth,
                            ZPixmap, 0,
                            (char *) newBuf,
                            width, height,
                            32, 0
                            );
        
    } else if (depth >= 15) {
        size_t numNewBufBytes = (2 * (width * height));
        u_int16_t *newBuf = malloc (numNewBufBytes);
        
        for (i = 0; i < numBufBytes; ++i) {
            unsigned int r, g, b;
            
            r = (buf[i] * rRatio);
            ++i;
            g = (buf[i] * gRatio);
            ++i;
            b = (buf[i] * bRatio);
            
            r &= vis->red_mask;
            g &= vis->green_mask;
            b &= vis->blue_mask;
            
            newBuf[outIndex] = r | g | b;
            ++outIndex;
        }
        
        img = XCreateImage (dis,
                            CopyFromParent, depth,
                            ZPixmap, 0,
                            (char *) newBuf,
                            width, height,
                            16, 0
                            );
    } else {
        fprintf (stderr, "This program does not support displays with a depth less than 15.");
        return NULL;				
    }
    
    XInitImage (img);
    /*Set the client's byte order, so that XPutImage knows what to do with the data.*/
    /*The default in a new X image is the server's format, which may not be what we want.*/
    
    int byteOrder;
    
    union {
        char c[sizeof(short)];
        short s;
    } order;
    
    order.s = 1;
    if ((1 == order.c[0])) {
        byteOrder =  LSBFirst;
    } else {
        byteOrder =  MSBFirst;
    }
    
    if ((LSBFirst == byteOrder)) {
        img->byte_order = LSBFirst;
    } else {
        img->byte_order = MSBFirst;
    }
    
    /*The bitmap_bit_order doesn't matter with ZPixmap images.*/
    img->bitmap_bit_order = MSBFirst;
    
    return img;
}

/**
 * \brief	Place a RGB color image (24bpp) at (xpos,ypos) position on the LCD screen.
 * \param	int xpos : X-coordinate of top-left image position
 * \param   int ypos : Y-coordinate of top-left image position
 * \param   int xsize : Image width
 * \param   int ysize : Image height
 * \param   BYTE* data : Pointer to image data (24-bit per pixel)
 * \return  int retVal : always 0
 */
int LCDPutImageRGB(int xpos, int ypos, int xsize, int ysize, BYTE* data){
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
    XImage *img;
    int screen = DefaultScreen (gLCDHandle->d);
    
    img = LCDCreateImage(gLCDHandle->d, screen, data, xsize, ysize);
    
    XPutImage(gLCDHandle->d, gLCDHandle->w, gLCDHandle->gc, img, 0, 0, xpos, ypos, xsize, ysize);
    
    return 1;
}

/**
 * \brief	Converts a color image to grey
 * \param   greyimage : the input image
 * \param   newImage:   the output image
 * \return  int retVal : always 0
 */
int LCDConvertGrey(BYTE* greyimage, BYTE* newImage) {
    if(LCD_INITIAL!=1) {
        LCDInit();
    }

    for(int i = 0; i<IM_WIDTH*IM_LENGTH; i++) {
    	newImage[3*i]   = greyimage[i];
	    newImage[3*i+1] = greyimage[i];
	    newImage[3*i+2] = greyimage[i];
    }
    return 0;
}

/**
 * \brief   Specifies the size of subsequent LCDImage functions
 * \param   int type: the size of the image
 * \return  int ret: always 0
 */
int LCDImageSize(int type) {
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
    IM_TYPE = type;
    
    switch (type){
        case QQVGA:
            IM_WIDTH = QQVGA_X;
            IM_LENGTH = QQVGA_Y;
            break;
        case  QVGA:
            IM_WIDTH = QVGA_X;
            IM_LENGTH = QVGA_Y;
            break;
        case  VGA:
            IM_WIDTH = VGA_X;
            IM_LENGTH = VGA_Y;
            break;
        case CAM1MP:
            IM_WIDTH = CAM1MP_X;
            IM_LENGTH = CAM1MP_Y;
        case CAMHD:
            IM_WIDTH = CAMHD_X;
            IM_LENGTH = CAMHD_Y;
        case CAM5MP:
            IM_WIDTH = CAM5MP_X;
            IM_LENGTH = CAM5MP_Y;
        default:
            return 1;
    }
}

/**
 * \brief		Sets the parameters for subsequent image draws
 * \param		int t : the image type
 * \param       int x : x-coordinate of top-left image position
 * \param       int y : y-coordinate of top-left image position
 * \param       int xs : Image width
 * \param       int ys : Image height
 * \return      int retVal : always 0
 */
int LCDImageStart(int x, int y, int xs, int ys) {
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
    IM_TYPE = CUSTOM;
    IM_LEFT = x;
    IM_TOP = y;
    IM_WIDTH = xs;
    IM_LENGTH = ys;
    
    return 0;
}

/**
 * \brief		Draws a colour (RGB) image to the LCD
 * \param       img : A pointer to the BYTE buffer of the image
 * \return      int retVal : always 0
 */
int LCDImage(BYTE *img) {
    if(LCD_INITIAL!=1) {
        LCDInit();
    }

    char* send;
    send = &img[0];
    LCDPutImageRGB(IM_LEFT, IM_TOP, IM_WIDTH, IM_LENGTH, send);
    LCDRefresh3();
}

/**
 * \brief	Converts a color image to grey
 * \param   greyimage : the input image
 * \param   newImage:   the output image
 * \return  int retVal : always 0
 */
int LCDConvertGray(BYTE* greyimage, BYTE* newImage) {
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
    for(int i = 0; i<IM_WIDTH*IM_LENGTH; i++) {
        newImage[3*i]   = greyimage[i];
        newImage[3*i+1] = greyimage[i];
        newImage[3*i+2] = greyimage[i];
    }
    return 0;
}


/**
 * \brief		Draws a grey image to the LCD
 * \param       img : A pointer to the BYTE buffer of the image
 * \return      int retVal : always 0
 */
int LCDImageGray(BYTE *greyImage) {
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
    BYTE newImage[3*IM_WIDTH*IM_LENGTH];
    
    IPSetSize(IM_TYPE);
    
    if (LCDConvertGray(greyImage, newImage) == 1) return 1;
    else LCDPutImageRGB(IM_LEFT, IM_TOP, IM_WIDTH, IM_LENGTH, newImage);
    
    LCDRefresh3();

    return 0;
}

/**
 * \brief		Draws a binary image to the LCD
 * \param       img : A pointer to the BYTE buffer of the image
 * \return      int retVal : always 0
 */
int LCDImageBinary(BYTE *binImage) {
    if(LCD_INITIAL!=1) {
        LCDInit();
    }
    
    for(int i = 0; i<IM_LENGTH*IM_WIDTH; i++) {
        binImage[i] = 255 - binImage[i]*255;
    }
    
    LCDImageGray(binImage);
    
    LCDRefresh3();
    return 0;
}

/**
 * \brief		Get LCD resolution in pixels
 * \param       x : Where to store the width of the LCD
 * \param       y : Where to store the height of the LCD
 * \return      int retVal : always 0
 */
int LCDGetSize(int* x, int* y) {
    char line[100];
    sprintf(line, "%s", OSExecute("fbset -fb /dev/fb1 | grep mode"));

    
    if(strlen(line)==0) {
        printf("Using HDMI...\n");
        *x = 640;
        *y = 480;
        return 0;
    }
    
    char* dimensions;
    dimensions = strtok(line, "\n\r\"");
    dimensions = strtok(NULL, "\n\r\"");
    
    char* heightStr = strtok(dimensions, "x");
    char* widthStr = strtok(NULL,"\n\r ");

    *x = atoi(heightStr);
    *y = atoi(widthStr);
    
    return 0;
}

int LCDGetDimensions(int* pix_x, int* pix_y, int* dim_x, int* dim_y) {
    char line[100];
    sprintf(line, "%s", OSExecute("xdpyinfo | grep 'dimensions:'"));
    
    char* temp;
    
    temp = strtok(line, " \n\r"); //dimensions:
    
    temp = strtok(NULL, " x\n\r"); //width
    *pix_x = atoi(temp);
    temp = strtok(NULL, " x\n\r"); //height
    *pix_y = atoi(temp);
    
    temp = strtok(NULL, " \n\r"); //pixels
    
    temp = strtok(NULL, " (x)\n\r"); //width
    *dim_x = atoi(temp);
    temp = strtok(NULL, " (x)\n\r"); //height
    *dim_y = atoi(temp);
    
    return 0;
}


