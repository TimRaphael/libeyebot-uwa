/**
 * \file    inout.c
 * \brief   Defines functions to interact with the IO board
 * \author  Marcus Pham
 */

#include "inout.h"
#include "eyebot.h"

/**
 * \brief Initialises connection to the IO board
 * \return 0 on success, 1 on failure
 */
int DIGITALInit() {
    if(DIGITALInitialised == 1) {
        return 0;
    }
    
    int temp;
    temp = SERInit(IOBOARD, DEFAULT_BAUD, 0);
    if (temp != 0) {
        return 1;
    }
    
    DIGITALInitialised = 1;
    return 0;
}

/**
 * \brief Sets up the digital input/output pins
 * \param io the pin to setup, direction UP/Down
 * \return 0 on success, 1 on failure
 */
int DIGITALSetup(int io, char direction){
    if(DIGITALInitialised == 0) {
        if(DIGITALInit()==1) {
            return 1;
        }
    }
    
    char cmd[3];
    cmd[0]='c'+128;
    cmd[1]=io;
    cmd[2]=direction;
    
    if(SERCheck(IOBOARD)!=0) {
        fprintf(stderr, "DIGITAL: Timeout!!!\n");
        DIGITALInitialised = 0;
        return 1;
    }
    
    for(int i =0; i<3; i++) {
        SERSendChar(IOBOARD, cmd[i]);
    }
    
    return 0;
}

/**
 * \brief Reads the digital input/output pins
 * \param io the pin to check status
 * \return 0 on success, 1 on failure
 */
int DIGITALRead(int io){
    if(DIGITALInitialised == 0) {
        if(DIGITALInit()==1) {
            return 1;
        }
    }
    
    char cmd[2];
    cmd[0]='i'+128;
    cmd[1]=io;
    
    if(SERCheck(IOBOARD)!=0) {
        fprintf(stderr, "DIGITAL: Timeout!!!\n");
        DIGITALInitialised = 0;
        return -1;
    }
    
    for(int i =0; i<2; i++) {
        SERSendChar(IOBOARD, cmd[i]);
    }
    
    while(SERReceiveChar(IOBOARD)!='>') {
        continue;
    }
    
    char buf[1];
    buf[0] = SERReceiveChar(IOBOARD);
    
    int ret;
    ret = 0|0|0|buf[0];
    return ret;
}

/**
 * \brief Reads all digital pins
 * \return an integer representing the value current status of all pins MSB first
 */
int DIGITALReadAll(void){
    if(DIGITALInitialised == 0) {
        if(DIGITALInit()==1) {
            return 1;
        }
    }
    
    char cmd[1];
    cmd[0]='I'+128;
    
    if(SERCheck(IOBOARD)!=0) {
        fprintf(stderr, "DIGITAL: Timeout!!!\n");
        DIGITALInitialised = 0;
        return -1;
    }
    
    for(int i =0; i<1; i++) {
        SERSendChar(IOBOARD, cmd[i]);
    }
    
    unsigned char buf[2];
    
    while(SERReceiveChar(IOBOARD)!='>') {
        continue;
    }
    
    buf[0]=SERReceiveChar(IOBOARD);
    buf[1]=SERReceiveChar(IOBOARD);
    
    unsigned int ret;
    ret = 0|0|(buf[0] << 8)|buf[1];
    return ret;
}

/**
 * \brief Writes to the digital input/output pins
 * \param io the pin to set, state the state of the pin
 * \return 0 on success, 1 on failure
 */
int DIGITALWrite(int io, int state){
    if(DIGITALInitialised == 0) {
        if(DIGITALInit()==1) {
            return 1;
        }
    }
    
    char cmd[3];
    cmd[0]='o'+128;
    cmd[1]=io;
    cmd[2]=state;
    
    if(SERCheck(IOBOARD)!=0) {
        fprintf(stderr, "DIGITAL: Timeout!!!\n");
        DIGITALInitialised = 0;
        return 1;
    }
    
    for(int i =0; i<3; i++) {
        SERSendChar(IOBOARD, cmd[i]);
    }
    
    return 0;
}

/**
 * \brief Reads the analog channel on the IO board
 * \param the channel to read from
 * \return a hex value representing the analog value
 */
int ANALOGRead(int channel){
    if(DIGITALInitialised == 0) {
        if(DIGITALInit()==1) {
            return 1;
        }
    }
    
    char cmd[2];
    cmd[0]='a'+128;
    cmd[1]=channel;
    
    if(SERCheck(IOBOARD)!=0) {
        fprintf(stderr, "DIGITAL: Timeout!!!\n");
        DIGITALInitialised = 0;
        return 1;
    }
    
    for(int i =0; i<2; i++) {
        SERSendChar(IOBOARD, cmd[i]);
    }
    
    while(SERReceiveChar(IOBOARD)!='>') {
        continue;
    }
    
    char buf[2];
    
    buf[0]=SERReceiveChar(IOBOARD);
    buf[1]=SERReceiveChar(IOBOARD);
    
    unsigned int ret;
    ret = 0|0|(buf[0] << 8)|buf[1];
    return ret;
}

/**
 * \brief Finds the Analog Voltage as read from the IO board
 * \return the voltage*100, on failure returns -1
 */
int ANALOGVoltage(){
    if(DIGITALInitialised == 0) {
        if(DIGITALInit()==1) {
            return -1;
        }
    }
    
    char cmd[1];
    cmd[0]='V'+128;
    
    if(SERCheck(IOBOARD)!=0) {
        fprintf(stderr, "VOLTAGE: Timeout!!!\n");
        DIGITALInitialised = 0;
        return 1;
    }
    
    for(int i =0; i<1; i++) {
        SERSendChar(IOBOARD, cmd[i]);
    }
    
    while(SERReceiveChar(IOBOARD)!='>') {
        continue;
    }
    
    char buf[2];
    
    buf[0]=SERReceiveChar(IOBOARD);
    buf[1]=SERReceiveChar(IOBOARD);
    
    unsigned int ret;
    ret = 0|0|(buf[0] << 8)|buf[1];
    return ret;
}

