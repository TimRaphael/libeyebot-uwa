/**
 *  \file camera.c
 *  \brief Camera routines for the Raspberry Pi 2, modded for the EyeBot.
 *  \author Jeremy Tan & Marcus Pham
 */

#include "eyebot.h"
#include "camera.h"
#include "improc.h"
#include "lcd.h"

static CvCapture *g_capture = NULL;

/**
 *  \brief Initialise the camera.
 *  \param [in] mode The mode to set the camera in
 *  \return Nonzero on success, zero on failure.
 *  \details Establishes the camera functions. This function must be called before any of the other camera functions.
 */
int CAMInit (int mode) {
    if (CAM_INITIAL == 1) {
        return(CAMSetMode(mode));
    }
    
    if (g_capture == NULL) {
        //g_capture = cvCreateCameraCapture(-1);
        g_capture = cvCaptureFromCAM(CV_CAP_ANY);
        if (g_capture == NULL) {
            return CAM_FAILURE;
        }
    }
    CAM_INITIAL = 1;
    return CAMSetMode(mode);
}

/**
 *  \brief Set the camera mode (e.g. format and resolution).
 *  \param [in] mode The camera mode to be set to
 *  \return Nonzero on success, zero on failure.
 *  \details Sets the camera mode. CAMInit must have been called previously.
 */
int CAMSetMode (int mode) {
    
    if(CAM_INITIAL != 1) {
        CAMInit(QVGA);
    }
    
    int width, height, realwidth, realheight;

    CAMMODE = mode;
    IPSetSize(mode);
    LCDImageSize(mode);
    
    // set sizes
    switch (mode){
        case QQVGA:
            CAMWIDTH = QQVGA_X;
            CAMHEIGHT = QQVGA_Y;
            break;
        case  QVGA:
            CAMWIDTH = QVGA_X;
            CAMHEIGHT = QVGA_Y;
            break;
        case  VGA:
            CAMWIDTH = VGA_X;
            CAMHEIGHT = VGA_Y;
            break;
        case CAM1MP:
            CAMWIDTH = CAM1MP_X;
            CAMHEIGHT = CAM1MP_Y;
            break;
        case CAMHD:
            CAMWIDTH = CAMHD_X;
            CAMHEIGHT = CAMHD_Y;
            break;
        case CAM5MP:
            CAMWIDTH = CAM5MP_X;
            CAMHEIGHT = CAM5MP_Y;
            break;
        default:
            return CAM_FAILURE;
    }
    
    CAMSIZE = CAMWIDTH*CAMHEIGHT*3;
    CAMPIXELS = CAMWIDTH*CAMHEIGHT;
    
    cvSetCaptureProperty(g_capture, CV_CAP_PROP_FRAME_WIDTH, CAMWIDTH);
    cvSetCaptureProperty(g_capture, CV_CAP_PROP_FRAME_HEIGHT, CAMHEIGHT);
    realwidth = (int)cvGetCaptureProperty(g_capture, CV_CAP_PROP_FRAME_WIDTH);
    realheight = (int)cvGetCaptureProperty(g_capture, CV_CAP_PROP_FRAME_HEIGHT);

    if (realwidth != CAMWIDTH || realheight != CAMHEIGHT) {
        printf("failure...\n");
        return CAM_FAILURE;
    }
    
    printf("success!!\n");
    return CAM_SUCCESS;
}

/**
 *  \brief Retrieves the current camera mode.
 *  \return The camera mode, or CAM_NONE on error.
 *  \details CAMInit must have been called previously.
 */
int CAMGetMode (void) {
    if(CAM_INITIAL != 1) {
        CAMInit(QQVGA);
    }
    
    return CAMMODE;
}

/**
 *  \brief Retrieve a camera frame into the provided buffer.
 *  \param [in] buf The buffer to copy the camera frame into. The supplied
 *                  buffer must be large enough to acommodate the size of the
 *                  image, as defined by the camera mode.
 *  \return Nonzero on success, zero on failure. On failure, the contents of buf
 *          is undefined.
 *  \details Retrieves the current camera frame and copies it into the
 *           supplied buffer. CAMInit must have been called previously.
 */
int CAMGet (BYTE *buf) {
    if(CAM_INITIAL != 1) {
        CAMInit(QQVGA);
    }
    
    if (g_capture != NULL) {
        IplImage *frame = cvQueryFrame(g_capture);
        if (frame != NULL) {
            cvCvtColor(frame, frame, CV_BGR2RGB);
            memcpy(buf, frame->imageData, frame->imageSize);
            return CAM_SUCCESS;
        }
    }
    return CAM_FAILURE;
}

/**
 *  \brief Stops and releases all camera structures.
 *  \return Nonzero on success, zero on failure.
 *  \details Releases all allocated memory and stops video capture.
 */
int CAMRelease (void) {
    if(CAM_INITIAL != 1) {
        CAMInit(QQVGA);
    }
    
    if (g_capture) {
        cvReleaseCapture(&g_capture);
        g_capture = NULL;
        return CAM_SUCCESS;
    }
    return CAM_FAILURE;
}

/**
 * \brief Grabs a gray image off the camera
 * \param the byte stream to store the image data, data in Gray format
 * \return 0 iff initialised correctly, otherwise 1
 */
int CAMGetGray(BYTE *buf) {
    if(CAM_INITIAL != 1) {
        CAMInit(QQVGA);
    }
    
    BYTE original[CAMSIZE];
    CAMGet(original);
    IPCol2Gray(original, buf);
    
    return 0;
}
