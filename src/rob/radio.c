
#include "radio.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <time.h>
#include <fcntl.h>

// globals
int sockfdListening;

int RADIOInit(void)
{
	struct addrinfo hints, *servinfo, *p;
	int rv;

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET; // set to AF_INET to force IPv4
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_flags = AI_PASSIVE; // use my IP

	if ((rv = getaddrinfo(NULL, SERVERPORT, &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		return -1;
	}

	// loop through all the results and bind to the first we can
	for(p = servinfo; p != NULL; p = p->ai_next) {
		if ((sockfdListening = socket(p->ai_family, p->ai_socktype,
				p->ai_protocol)) == -1) {
			perror("RADIOInit: socket");
			continue;
		}
	
		// Bind socket for listening
		if (bind(sockfdListening, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfdListening);
			perror("RADIOInit: bind");
			continue;
		}

		break;
	}

	if (p == NULL) {
		fprintf(stderr, "RADIOInit: failed to bind socket\n");
		return -1;
	}
	

	freeaddrinfo(servinfo);	
	
	return 0;
	
}


int RADIOSend(int id, BYTE* buf, int size)
{
	int sockfd;
	struct addrinfo hints, *servinfo, *p;
	int status;
	int numbytes;
	char ipstr[INET_ADDRSTRLEN];
	char ip[15];
	strcpy(ip, "192.168.1.");

	// Mapping id to IP address. The last byte of the IP address will be mapped to the ID of the robot
	char buffer_id [10];
	sprintf(buffer_id,"%d\0",(int)id);
	strcat(ip, buffer_id);

	
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_DGRAM;

	if ((status = getaddrinfo(ip, SERVERPORT, &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(status));
		return -1;
	}

	// loop through all the results and make a socket
	for(p = servinfo; p != NULL; p = p->ai_next) {
		void *addr;

		if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
			perror("RADIOSend: socket");
			continue;
		}
		
		// get the pointer to the address itself,
		struct sockaddr_in *ipv4 = (struct sockaddr_in *)p->ai_addr;
		addr = &(ipv4->sin_addr);
		// convert the IP to a string
		inet_ntop(p->ai_family, addr, ipstr, sizeof ipstr);
	
		break;
	}

	if (p == NULL) {
		fprintf(stderr, "RADIOSend: failed to create socket\n");
		return -1;
	}

	// Send bytes
	if ((numbytes = sendto(sockfd, buf, size, 0, p->ai_addr, p->ai_addrlen)) == -1) {
		perror("RADIOSend: sendto");
	}

	freeaddrinfo(servinfo);

//	printf("RADIOSend: sent %d bytes to %s\n", numbytes, ipstr);
	close(sockfd);
	
	return 0;
	
}

int RADIORecv(int* id, BYTE* buf, int* size)
{
	struct sockaddr_storage their_addr;
	socklen_t addr_len;
	char s[INET_ADDRSTRLEN];
	int oldfl;
	
	// Setting the socket back to blocking 
	oldfl = fcntl(sockfdListening, F_GETFL);
	if (oldfl == -1) {
		fprintf(stderr, "RADIORecv: failed to read flags in function fcntl\n");
		return -1;
	}
	if(fcntl(sockfdListening, F_SETFL, oldfl & ~O_NONBLOCK) == -1){
		fprintf(stderr, "RADIORecv: failed to set socket to blocking\n");
		return -1;
	}
	
	// Receive bytes
	addr_len = sizeof their_addr;
	if ((*size = recvfrom(sockfdListening, buf, MAXBUFLEN-1 , 0, (struct sockaddr *)&their_addr, &addr_len)) == -1) {
		perror("RADIORecv: recvfrom");
		return -1;
	}
	
	// Mapping IP address to id
	const char* ip = inet_ntop(their_addr.ss_family, &(((struct sockaddr_in*)((struct sockaddr *)&their_addr))->sin_addr), s, sizeof s);


	unsigned short a, b, c, d;
	sscanf(ip, "%hu.%hu.%hu.%hu", &a, &b, &c, &d);
	
	*id = d;
	
	return 0;
}

int RADIOCheck(int *id)
{
	struct sockaddr_storage their_addr;
	socklen_t addr_len;
	char buf[MAXBUFLEN];
	int numbytes;
	char s[INET_ADDRSTRLEN];
	
	
	// Set the socket to be non blocking
	if(fcntl(sockfdListening, F_SETFL, O_NONBLOCK) == -1){
		fprintf(stderr, "RADIOCheck: failed to set socket to non blocking\n");
		return -1;
	}
	
	addr_len = sizeof their_addr;
	// Check if something is wating in the buffer
	if((numbytes = recvfrom(sockfdListening, buf, MAXBUFLEN-1 , MSG_PEEK, (struct sockaddr *)&their_addr, &addr_len)) == -1) {
		// Uncomment if wanted
		// perror("RADIOCheck: recvfrom");
		return -1;	
	}
	
	// Mapping IP address to id
	const char* ip = inet_ntop(their_addr.ss_family, &(((struct sockaddr_in*)((struct sockaddr *)&their_addr))->sin_addr), s, sizeof s);


	unsigned short a, b, c, d;
	sscanf(ip, "%hu.%hu.%hu.%hu", &a, &b, &c, &d);
	
	*id = d;

	
	return numbytes;	
}



int RADIORelease(void)
{
	if(close(sockfdListening) == -1){
		perror("RADIOTerm: close");
		return -1;
	}
	return 0;
}
