/**
 * \file timer.c
 * \brief Sets up and runs functions at given times
 * \author Rhys Kessey 
 * \author Marcus Pham (cleaned up)
 *
 	\detail These timer functions were written by Rhys Kessey
	The functions deal with two global variables, functions[] and function_timer_scales[]
	Every time a timer is linked to a function, values will be added to these arrays
	Every clock tick (determined by the rate that core_timer is called) these functions will be checked
	if the current clock tick is a multiple of a function scale, then the function will be executed.
	eg.
	functions[] = 				{f1,f2,f3,f4,NULL,NULL,NULL....}
	function_timer_scales[] = 	{1 ,2 ,1 ,4 ,0   ,0   ,0   ....}
	then every clock tick, f1 and f3 will be executed (in that order)
	every 2 clock ticks, f2 will be executed
	every 4 clock ticks, f4 will be executed
 */
#include "timer.h"
#include "eyebot.h"
#include <unistd.h>

void (*_functions[16]) (void) = {NULL};
int _function_timer_scales[16] = {0};
int _timers_active = 0;
int _core_timedev = 1000;
int _clock_ticks = 0;

/**
 * \brief The core timer. Determines whether or not it is time to run one of the functions that have timers attached to them
 * \param sig the signal
 */
void TIMCore_timer(int sig) {
	_clock_ticks += 1;
	//Check through all timers
	int index = 0;
	while (index < 16) {
		//If the function timer scale is saved as zero, ignore it
		if (_function_timer_scales[index]) {
			//If the function's timer scale is a perfect divisor of the current number of clock ticks, run the function
			if ((_clock_ticks % _function_timer_scales[index]) == 0) {
				(*_functions[index])();
			}
		}
		index += 1;
	}
	return;
}

/**
 * \brief Begins an alarm which calls the "core_timer" function every 1ms * rate producing a calling frequency of 1kHz / rate.
 */
void TIMInitialise_core_timer(void) {
	struct itimerval new;
	//Set continuous wait after initial wait
	new.it_interval.tv_usec = _core_timedev;	
	new.it_interval.tv_sec = 0;
	//Set initial wait before calling function
	new.it_value.tv_usec = _core_timedev; 		
	new.it_value.tv_sec = 0;
	//Using the SIGALRM functionality, linking it to the core_timer function
	signal(SIGALRM, TIMCore_timer);
	//Creating the timer, using a real clock	
	setitimer(ITIMER_REAL, &new, NULL);
	return;
}

/**
 * \brief Waits for 1 ms
 * \param n the number of ms to wait for
 * \return 0 always
 */
int OSWait(int n) {
	usleep(n*1000);
	return 0;
}

/**
 * \brief adds a function to a 1kHz/scale timer, ie. executes every 1ms*scale
 * \detail usage: OSAttachTimer(<scale>, &<function>);
 * \param scale the rate to run the function
 * \param fct the function to run, note must return void and have no arguments
 * \return the Timer struct for the timer
 */
TIMER OSAttachTimer(int scale, void (*fct)(void)) {
    if (!_timers_active) {
        TIMInitialise_core_timer();
    }
    
	int index = 0;
	while (index < 16) {
		if (_function_timer_scales[index] == 0) {
			_functions[index] = fct;
			_function_timer_scales[index] = scale;
			_timers_active += 1;
			break;
		}
		index += 1;
	}
	return (TIMER)(index + 1);
}

/**
 * \brief removes the Timer from the list
 * \param handle the timer to remove
 * \return 0 always
 */
int OSDetachTimer(TIMER handle) {
	_functions[(int)(handle - 1)] = NULL;
	_function_timer_scales[(int)(handle - 1)] = 0;
	_timers_active -= 1;
	return 0;
}

/**
 * \brief sets the system time
 * \param hrs the hours
 * \param mins the mins
 * \param secs the secs
 * \return 0 always
 */
int OSSetTime(int hrs,int mins,int secs){
	struct timeval tv;
	gettimeofday(&tv, NULL);
	int offset = (int)tv.tv_sec;
	offset = offset / (24*3600);
	int time_seconds = (offset + hrs*3600 + mins*60 + secs);
	tv.tv_usec = 0;
	tv.tv_sec = (time_t)time_seconds;
	settimeofday(&tv, NULL);
	return 0;
}

/**
 * \brief Get system time (ticks in 1/100 sec)
 * \param hrs the pointer to the hours integer
 * \param mins the pointer to the mins integer
 * \param secs the pointer to the secs integer
 * \param ticks the pointer to the ticks integer
 * \return 0 always
 */
int OSGetTime(int *hrs,int *mins,int *secs,int *ticks){
	struct timeval tv;  
    gettimeofday(&tv, NULL);  
	int time_seconds = (int)tv.tv_sec;
	int time_microseconds = (int)tv.tv_usec;
	time_seconds = time_seconds % (24*3600);
    *hrs = time_seconds / 3600;
	time_seconds -= *hrs*3600;
	*mins = time_seconds / 60;
	*secs = time_seconds - *mins*60;
	*ticks = time_microseconds/1000;
	return 0;
}

/**
 * \brief prints the time on the command line
 * \return 0 always
 */
int OSShowTime(void){
	int hrs;
	int mins;
	int secs;
	int ticks;
	OSGetTime(&hrs, &mins, &secs, &ticks);
	printf("%d : %d : %d  + %d\n", hrs, mins, secs, ticks);
	return 0;
}

/**
 * \brief gives you the count in 1/100 sec since the system start
 * \return the count
 */
int OSGetCount(void){
	return _clock_ticks;
}








