/**
 * \file		system.c
 * \brief   Defines functions for the system
 * \author  Remi KEAT & Marcus Pham
 */

#include "system.h"
#include "serial.h"
#include "eyebot.h"

int OSInitialised = 0;
/**
 * \brief   trims the string by exchanging the first newlin char with the null byte
 * \param   src: the string to be trimmed
 */
void OSTrimline(char *src)
{
    int length = strlen(src);
    int i;
    for (i = 0; i < length; i++)
    {
        if (src[i] == '\n' || src[i] == '\r')
        {
            src[i] = '\0';
            break;
        }
    }
}

void OSStrcpy_n(char* dest, const char* src, size_t n)
{
    int length = strlen(src);
    if (n < length)
    {
        char *temp = (char *)malloc(length+1);
        strcpy(temp,src);
        temp[n] = '\0';
        strcpy(dest, temp);
        free(temp);
    }
    else
    {
        strcpy(dest,src);
    }
}

/**
 * \brief       Executes a system command and obtains the string returned after the command
 * \param       command the command to run
 * \return      the string returned after execution
 */
char* OSExecute(char* command)
{
	FILE* fp;
	char path[1024];
	char output[2056] = "";
	fp = popen(command, "r");
	if (fp == NULL)
	{
		strcat(output, "Failed to run the command");
	}
	else
	{
		while (fgets(path, sizeof(path)-1, fp) != NULL)
		{
			strcat(output, path);
		}
	}
	pclose(fp);
	output[strlen(output)-1] = '\0';
	return output;
}

/**
 * \brief			Returns string containing running RoBIOS version.
 * \return    the OS version
 * \details   Example: "3.1b"
 */
int OSVersion(char* buf)
{
	char* version = VERSION;
    buf = &version[0];
	return 1;
}

/**
 * \brief Gives the version of the board
 * \param version, the string to store the version string
 * \return 0 on success, 1 on failure
 */
int OSVersionIO(char* version) {
    if(OSInitialised == 0) {
        int temp;
        temp = SERInit(IOBOARD, DEFAULT_BAUD, 0);
        if (temp != 0) {
            return 1;
        }
        
    }
    
    char cmd[1];
    cmd[0]='v'+128;
    
    if(SERCheck(IOBOARD)!=0) {
        fprintf(stderr, "BOARD_VER: Timeout!!!\n");
        OSInitialised = 0;
        //memcpy(version, "Not Connected", 14);
        return 1;
    }
    
    for(int i =0; i<1; i++) {
        SERSendChar(IOBOARD, cmd[i]);
    }
    
    while(SERReceiveChar(IOBOARD)!='>') {
        continue;
    }
    
    char buf[4];
    
    SERReceive(IOBOARD, buf);
    
    for(int i=0; i<4; i++) {
        if(buf[i]=='>') {
            buf[i] = '\0';
        }
    }
    
    memcpy(version, buf, 4);
    return 0;
}


/**
 * \brief     Inform the user how fast the processor runs.
 * \return		the actual clockrate of CPU in Hz
 */
int OSMachineSpeed(void)
{
	return MACHINE_SPEED;
}

/**
 * \brief     Inform the user in which environment the program runs.
 * \return	 the Type of used hardware
 * \details   Valid values are: VEHICLE, PLATFORM, WALKER
 */
int OSMachineType(void)
{
	return MACHINE_TYPE;
}

/**
 * \brief     Inform the user with which name the Eyebot is titled
 * \return		the Name of actual Eyebot
 */
int OSMachineName(char* buf)
{
	char* machineName = MACHINE_NAME;
    buf = &machineName[0];
	return 1;
}


/**
 * \brief			Inform the user with which ID the Eyebot is titled
 * \return		the ID of actual Eyebot
 */
int OSMachineID(void)
{
	return MACHINE_ID;
}

/**
 * \brief			Print message and number to display then stop processor (deadend) or wait for key
 * \param     msg a pointer to message
 * \param     number the error number
 * \param     deadend switch to choose deadend or keywait
 * \details   Valid values are:
 * - 0 = no deadend
 * - 1 = deadend
 * \return    int retVal : Always 0
 */
int OSError(char *msg, int number, bool deadend)
{
	//TODO: To complete
	return 0;
}

/**
 * \brief	   Collects infos about the CPU – name, speed, architecture and bogusMips
 * \param     infoCPU a pointer to a structure containing the cpu infos
 * \return    always 0
 */
int OSInfoCPU (INFO_CPU* infoCPU)
{
    strcpy(infoCPU->name, OSExecute("cat /proc/cpuinfo | grep Processor | cut -d ':' -f2 | head -n1"));
    strcpy(infoCPU->arch, OSExecute("cat /proc/cpuinfo | grep Processor | cut -d ':' -f2 | head -n1"));

    strcpy(infoCPU->bogomips, OSExecute("cat /proc/cpuinfo | grep BogoMIPS | cut -d ':' -f2 | head -n1"));
    strcpy(infoCPU->mhz, OSExecute("cat /proc/cpuinfo | grep BogoMIPS | cut -d ':' -f2 | head -n1"));
    
	return 0;
}

/**
 * \brief	  Collects infos about the memory
 * \param     infoMem : pointer to a structure which contains the memory infos
 * \return    always 0
 */
int OSInfoMem (INFO_MEM* infoMem) {
	strcpy(infoMem->procnum, OSExecute("ps aux | wc -l"));
    strcpy(infoMem->total, OSExecute("cat /proc/meminfo | grep MemTotal: | cut -d ':' -f2 | tr -s [:space:] | cut -d ' ' -f2"));
    strcpy(infoMem->free, OSExecute("cat /proc/meminfo | grep MemFree: | cut -d ':' -f2 | tr -s [:space:] | cut -d ' ' -f2"));

	return 0;
}

/**
 * \brief			Collects infos about processes
 * \param 		INFO_PROC infoProc : pointer to a structure which contains the process infos
 * \return    int retVal : always 0
 */

int OSInfoProc (INFO_PROC* infoProc)
{
	strcpy(infoProc->num, OSExecute("ps aux | wc -l"));
	return 0;
}

/**
 * \brief	  Collects system’s miscellaneous infos – uptime, vbatt
 * \param     infoMisc : pointer to a structure which contains the misc infos
 * \return    always 0
 */
int OSInfoMisc (INFO_MISC* infoMisc)
{
	const int vbatt_max_8 = 12 << 8;
	struct sysinfo info;
	sysinfo(&info);
	int len = 100;
	char str[len];
	snprintf(str, len, "%ld", info.uptime);
    
	strcpy(infoMisc->uptime, str);
    
	strcpy(infoMisc->vbatt, "5V");
	//infoMisc->vbatt_8 = vbatt_max_8/2;
	infoMisc->vbatt_8 = vbatt_max_8;
	
	return 0;
}
