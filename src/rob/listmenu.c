/**
 * \file    listmenu.c
 * \brief   Defines all the fucntions for making/editing lists for the Eyebot
 * \author  Marcus Pham & Remi Keat
 */

#include "listmenu.h"
#include "eyebot.h"
#include "system.h"
#include "lcd.h"

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

/**
 * \brief Returns the size of the list
 * \param listmenu the menu to check
 * \return the size of the list
 */
int LMSize(LIST_MENU listmenu) {
	return listmenu.size;
}

/**
 * \brief Toggles the scroll functions
 * \param *listmenu a pointer to the list
 */
void LMToggleScroll(LIST_MENU *listmenu) {
	if (listmenu->scroll==0)
	{
		listmenu->scroll = 1;
	}
	else
	{
		listmenu->scroll = 0;
	}
}

/**
 * \brief checks for the scroll value of the list
 * \param listmenu the listmenu item
 * \return the scroll value
 */
int LMGetScroll(LIST_MENU listmenu)
{
	return listmenu.scroll;
}

/**
 * \brief initialises the list to display on the LCD
 * \param *listmenu a pointer to the menu
 * \param name the name of the list
 */
void LMInit(LIST_MENU *listmenu,char *name)
{
	FBINFO fb;
	LCDGetFBInfo(&fb);
    OSStrcpy_n(listmenu->title,name,LCD_LIST_STRLENGTH);
	listmenu->fgcol = LCD_WHITE;
	listmenu->bgcol = LCD_BLUE;
	listmenu->size = 0;
	listmenu->start = 0;
	listmenu->left = 0;
	listmenu->width = fb.cursor.xmax;
	listmenu->no_empty = 0;
	listmenu->scroll = 0;
	listmenu->index = 0;
}

/**
 * \brief Adds an item (a string) to the list
 * \param *listmenu a pointer to the menu
 * \param name the name of the item to add the list
 * \param *plink a pointer to a user function/data
 */
void LMAddItem(LIST_MENU *listmenu,char *name, void *plink)
{
	if (listmenu->size == 0)
	{
		listmenu->pitems = malloc(sizeof(MENU_ITEM));
	}
	else
	{
		listmenu->pitems = realloc(listmenu->pitems,(LMSize(*listmenu)+1)*sizeof(MENU_ITEM));
	}
	if (!listmenu->pitems)
	{
			printf("ERROR: unable to allocate memory for listmenu\n");
			listmenu->pitems == NULL;
			return;
	}
	listmenu->size+=1;
	OSStrcpy_n(listmenu->pitems[LMSize(*listmenu)-1].label,name,LCD_LIST_STRLENGTH);
	listmenu->pitems[LMSize(*listmenu)-1].fgcol = LCD_RED;
	listmenu->pitems[LMSize(*listmenu)-1].bgcol = LCD_AQUA;
	listmenu->pitems[LMSize(*listmenu)-1].plink = plink;
	if (LMSize(*listmenu) > LCD_MAX_LIST_ITEM)
	{
		listmenu->scroll = 1;
	}
}

/**
 * \brief Frees the items in the list
 * \param *listmenu a pointer to the menu
 */
void LMFreeItems(LIST_MENU *listmenu)
{
	free(listmenu->pitems);
	listmenu->size = 0;
	listmenu->scroll = 0;
}

/**
 * \brief Changes the item's name
 * \param *listmenu a pointer to the menu
 * \param newText the new name of the item to add the list
 * \param index the index of the item in the list
 */
void LMChangeItemText(LIST_MENU *listmenu, char *newText,int index)
{
	if (index < 0 || index >= listmenu->size)
	{
		fprintf(stderr,"WARNING: attempted to change text of item outside bounds\n");
		return;
	}
	else
	{
		OSStrcpy_n(listmenu->pitems[index].label,newText,LCD_LIST_STRLENGTH);
	}
}

/**
 * \brief Returns the index of the menu item
 * \param listmenu the menu
 * \return the index of the item
 */
int LMGetIndex(LIST_MENU listmenu)
{
	return listmenu.index;
}

/**
 * \brief Returns the text for the current menu itme
 * \param listmenu the menu
 * \return the string representing the text
 */
char* LMGetItemText(LIST_MENU listmenu)
{
	int index = listmenu.index;
	return listmenu.pitems[index].label;
}
