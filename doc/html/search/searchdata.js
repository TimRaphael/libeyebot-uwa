var indexSectionsWithContent =
{
  0: "abcdeghiklmnopqrstvwy",
  1: "t",
  2: "et",
  3: "acdeiklmoprsv",
  4: "it",
  5: "bcqtv",
  6: "abcdghiklmnopqrstvwy",
  7: "e"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "defines",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Macros",
  7: "Pages"
};

