var searchData=
[
  ['qqvga',['QQVGA',['../types_8h.html#a805c286cc248885213510b11f736b880',1,'types.h']]],
  ['qqvga_5fpixels',['QQVGA_PIXELS',['../types_8h.html#a287b16c96815254fe161a4e1e735514e',1,'types.h']]],
  ['qqvga_5fsize',['QQVGA_SIZE',['../types_8h.html#a3b8a2a161ce40a9aba07438a2412731b',1,'types.h']]],
  ['qqvga_5fx',['QQVGA_X',['../types_8h.html#a4b56f57420a31e279276f428e1073d42',1,'types.h']]],
  ['qqvga_5fy',['QQVGA_Y',['../types_8h.html#a87d9491556244446c75e9cc4ad64aaa2',1,'types.h']]],
  ['qvga',['QVGA',['../types_8h.html#a0f7ce973ee26e5d14b76aab4e98e2f26',1,'types.h']]],
  ['qvga_5fpixels',['QVGA_PIXELS',['../types_8h.html#acd1d26a221a1fd06aa9922656b1d222b',1,'types.h']]],
  ['qvga_5fsize',['QVGA_SIZE',['../types_8h.html#ab38b3d268f7f619386bd4efebfd43915',1,'types.h']]],
  ['qvga_5fx',['QVGA_X',['../types_8h.html#a9bae597c7c361939d6513fca794ae09f',1,'types.h']]],
  ['qvga_5fy',['QVGA_Y',['../types_8h.html#a80797ec3dbe6fcfc29845ea64a6d138c',1,'types.h']]]
];
